# Pull base image.
FROM node:12-alpine

# Installs latest Chromium (77) package.
RUN apk add --no-cache \
  chromium \
  nss \
  freetype \
  freetype-dev \
  harfbuzz \
  ca-certificates \
  ttf-freefont \
  yarn

# Tell Puppeteer to skip installing Chrome. We'll be using the installed package.
ENV PUPPETEER_SKIP_CHROMIUM_DOWNLOAD true

# Copy files
RUN mkdir -p /var/node_app
WORKDIR /var/node_app
COPY . .

# Install app
RUN \
  cd /var/node_app && \
  npm install --quiet --production