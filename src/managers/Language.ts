import dbPool from '../database/pool';

export interface IGetLanguageResponse {
    language_id: number;
    language_locale_id: number|null;
}

export interface ILanguage {
    id: number;
    name: number;
    iso: string;
    locale: number;
}

class LanguageManager {
    // No cambiar el orden
    private defaultLanguages = ['en', 'es', 'pt', 'fr', 'it', 'de'];
    private LanguagesById = {};
    private Languages = {};

    public retrieve = async () => {
        const [rows] = await dbPool.query('SELECT * FROM language', []);

        for (const lang of rows) {
            this.Languages[lang.iso] = lang.id;
            this.LanguagesById[lang.id] = lang;
        }
    }

    public get = async (iso: string, locale?: string): Promise<IGetLanguageResponse|null> => {
        await this.ensureLanguages();

        if (!this.Languages[iso]) {
            return null;
        }
    
        return {
            language_id: this.Languages[iso],
            language_locale_id: locale && this.Languages[iso + '-' + locale]
                ? this.Languages[iso + '-' + locale]
                : null
        };
    }

    public getById = async (id: number): Promise<ILanguage|null> => {
        await this.ensureLanguages();
        
        return this.LanguagesById[id] || null;
    }

    public getDefaultLanguages = () => this.defaultLanguages;

    public getPathPrefix = async (website, currentLanguage): Promise<string> => {
        if (website.language.multiLanguage) {
            const languageId = currentLanguage.show_locale ? currentLanguage.language_locale_id : currentLanguage.language_id;
            const language = await this.getById(languageId);
            if (language) {
                return `/${language.iso}`;
            }
        }
    
        return '';
    }

    private ensureLanguages = () => {
        if (!Object.keys(this.Languages).length) {
            return this.retrieve();
        }

        return;
    }
}

export default new LanguageManager();