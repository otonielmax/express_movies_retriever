export enum ImageMediaStatus {
    COMPLETED = 1,
    PENDING = 2,
    ERROR = 3
}