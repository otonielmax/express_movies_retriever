export enum ImageMediaType {
    BACKDROP = 1,
    LOGO = 2,
    POSTER = 3,
    PROFILE = 4,
    STILL = 5
}