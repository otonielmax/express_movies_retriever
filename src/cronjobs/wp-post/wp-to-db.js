'use strict';

const logger = require('@smartadtags/logger');
const config = require('config');
const moment = require('moment');
const mysql = require('mysql2/promise');

const healthCheck = require('../../helpers/healthCheck');
const dbService = require('../../services/dbPromises');

logger.config({
    environment: config.LOGGER_ENV,
    service: 'movies-retriever',
    logLevel: config.LOGGER_LOG_LEVEL,
    format: config.LOGGER_FORMAT
});
const transactionId = 'cronjob-wp-post-wp-to-db';

const getPosts = async (mysqlConnection) => {
    const query = `
        SELECT
            ID as id,
            post_title,
            post_content,
            post_modified
        FROM
            d0c_posts
        WHERE
            post_modified >= ? AND
            post_type = ? AND
            post_status = ?
    `;

    const startDate = moment().subtract(24, 'hours').format('YYYY-MM-DD HH:mm');
    const [ rows ] = await mysqlConnection.query(query, [startDate, 'post', 'publish']);

    return rows;
};

const parsePosts = (posts) => {
    if (!posts || !posts.length) {
        return [];
    }

    return posts.map((post) => {
        const splittedTitle = post.post_title.split('|');

        if (!splittedTitle[3]) {
            return null;
        }

        return {
            website_id: splittedTitle[1],
            language_id: splittedTitle[2],
            movie_id: splittedTitle[3],
            content: post.post_content,
            post_modified: post.post_modified
        };
    }).filter((post) => !!post);
};

const getArticle = async (post) => {
    const query = `
        SELECT
            *
        FROM
            article
        WHERE
            website_id = ? AND
            language_id = ? AND
            movie_id = ?
    `;
    const params = [post.website_id, post.language_id, post.movie_id];
    const [ rows ] = await dbService.query(query, params);
    return rows[0];
};

const processPosts = async () => {
    let mysqlConnection;

    try {
        mysqlConnection = await mysql.createConnection({
            host: config.MYSQL_WP_HOST,
            user: config.MYSQL_WP_USER,
            password: config.MYSQL_WP_PASSWORD,
            database: config.MYSQL_WP_DATABASE
        });
    
        logger.info('Retrieving latest posts from WP Database', { transactionId });
    
        const posts = await getPosts(mysqlConnection);
        const parsedPosts = parsePosts(posts);
    
        for (const post of parsedPosts) {
            logger.info('Processing WP Post', { transactionId, post });
            const article = await getArticle(post);
            if (!article) {
                logger.info('There is not article for the movie. Creating it', { transactionId });
    
                const data = {
                    content: post.content,
                    movie_id: post.movie_id,
                    language_id: post.language_id,
                    website_id: post.website_id,
                    created_at: new Date(),
                    updated_at: new Date()
                };
                await dbService.query('INSERT INTO article SET ?', [data]);
    
                logger.info('Article created', { transactionId });
            } else {
                const articleId = article.id;
                const articleDate = article.updated_at || article.created_at;
                const postDate = post.post_modified;
    
                logger.info(`Article "${articleId}" found, comparing dates`, {
                    transactionId,
                    articleId,
                    articleDate,
                    postDate
                });
                
                const isBefore = moment(article.updated_at).isBefore(postDate);
                if (isBefore) {
                    logger.info(`Updating article "${articleId}"`, { transactionId });
                    
                    const data = {
                        content: post.content,
                        updated_at: new Date()
                    };
                    await dbService.query('UPDATE article SET ? WHERE id = ?', [data, articleId]);
    
                    logger.info(`Article "${articleId}" updated`, { transactionId });
                } else {
                    logger.info(`Article "${articleId}" does not need to be updated`, { transactionId });
                }
            }
        }

        await mysqlConnection.end();
    } catch (err) {
        if (mysqlConnection) {
            await mysqlConnection.end();
        }

        throw err;
    }
};

const run = async () => {
    try {
        logger.info('Starting cronjob', { transactionId });
        await processPosts();
        logger.info('Job executed successfully', { transactionId });

        // Enviar healthcheck
        await healthCheck(transactionId, config.HC_WP_POST_WP_TO_DB);
    } catch (err) {
        logger.error('Error executing job', { transactionId, err });
    }
    
    await dbService.endPool();
};

run();