'use strict';

const logger = require('@smartadtags/logger');
const config = require('config');
const mysql = require('mysql2/promise');
const request = require('request-promise');
const WPAPI = require('wpapi');

const getWebsitesConfig = require('../../helpers/getWebsitesConfig');
const healthCheck = require('../../helpers/healthCheck');

logger.config({
    environment: config.LOGGER_ENV,
    service: 'movies-retriever',
    logLevel: config.LOGGER_LOG_LEVEL,
    format: config.LOGGER_FORMAT
});
const transactionId = 'cronjob-wp-post-rss-to-wp';
const websites = getWebsitesConfig().filter((w) => w.wpActive === true);

const wpClient = new WPAPI({
    endpoint: 'https://blog.repelis.biz/wp-json',
    username: config.WP_USERNAME,
    password: config.WP_PASSWORD
});

const processMovies = async () => {
    let mysqlConnection;

    try {
        mysqlConnection = await mysql.createConnection({
            host: config.MYSQL_WP_HOST,
            user: config.MYSQL_WP_USER,
            password: config.MYSQL_WP_PASSWORD,
            database: config.MYSQL_WP_DATABASE
        });
    
        for (const website of websites) {
            const websiteName = website.name;
            
            const reqParams = {
                url: `${website.host}/rss/top`,
                qs: {
                    multi_lang: website.language.multiLanguage ? 1 : 0,
                    format: 'json',
                    limit: 15,
                    ts: +new Date()
                },
                json: true
            };

            logger.info(`[${websiteName}] Retrieving RSS content`, { transactionId, websiteName, reqParams });

            const rssContent = await request(reqParams);
    
            for (const movie of rssContent) {
                const movieTitle = movie.title;
                logger.info(`[${websiteName}] Processing movie`, { transactionId, websiteName, movieTitle });
    
                const query = `
                    SELECT
                        *
                    FROM
                        d0c_posts
                    WHERE
                        post_title LIKE ?
                `;
                const searchParam = `%|${movie.websiteId}|${movie.languageId}|${movie.movieId}`;
    
                const [ rows ] = await mysqlConnection.query(query, [searchParam]);
                const foundRows = rows.length;
    
                if (foundRows === 0) {
                    const postTitle = `${movie.parsedPopularity}-${movie.url}|${movie.websiteId}|${movie.languageId}|${movie.movieId}`;
                    logger.info(`[${websiteName}] No rows found. Creating WP Post`, { transactionId, movieTitle, postTitle });
                    await wpClient.posts().create({
                        title: postTitle,
                        content: 'Por desarrollar',
                        status: 'draft',
                        author: 2,
                        categories: [movie.wordpressCategoryId]
                    });
                } else {
                    logger.info(`[${websiteName}] Movie already exists in WP`, { transactionId, movieTitle });
                }
            }
        }
    
        await mysqlConnection.end();
    } catch (err) {
        if (mysqlConnection) {
            await mysqlConnection.end();
        }

        throw err;
    }
};

const run = async () => {
    try {
        logger.info('Starting cronjob', { transactionId });
        await processMovies();
        logger.info('Job executed successfully', { transactionId });

        // Enviar healthcheck
        await healthCheck(transactionId, config.HC_WP_POST_RSS_TO_WP);
    } catch (err) {
        logger.error('Error executing job', { transactionId, err });
    }
};

run();