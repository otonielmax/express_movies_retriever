'use strict';

const logger = require('@smartadtags/logger');
const async = require('async');
const config = require('config');
const pathToRegexp = require('path-to-regexp');
const squel = require('squel');
const url = require('url');

const getWebsitesConfig = require('../../helpers/getWebsitesConfig');
const healthCheck = require('../../helpers/healthCheck');
const languageHelper = require('../../helpers/language');
const utils = require('../../helpers/utils');
const dbService = require('../../services/db');

logger.config({
    environment: config.LOGGER_ENV,
    service: 'movies-retriever',
    logLevel: config.LOGGER_LOG_LEVEL,
    format: config.LOGGER_FORMAT
});
const transactionId = 'cronjob-hash-regenerator';
let websites = [];

const findHash = (dbConnection, params, callback) => {
    console.log('Iniciando');
    const find = (tryNum) => {
        const hash = initialHash;
        console.log('Hash', hash);

        let maxLength = 100;
        let tryStr = '';
        let finalHash = hash;

        if (tryNum) {
            tryStr = '-' + tryNum;
            maxLength -= tryStr.length;
        }

        if (finalHash.length > maxLength) {
            finalHash = finalHash.substr(0, maxLength);
        }
        if (tryStr.length) {
            finalHash += tryStr;
        }

        const query = squel.select();

        // From
        query.from(params.table);

        // Column
        query.field('COUNT(*)', 'count');

        // Where
        query.where(`${params.column} = ?`, finalHash);


        const preparedQuery = query.toParam();

        dbConnection.query(preparedQuery.text, preparedQuery.values, (err, result) => {
            if (err) {
                return callback(err);
            }

            if (!result || !result[0]) {
                return callback(new Error('Error extraño'));
            }

            if (result[0].count && result[0].count > 0) {
                return find(tryNum + 1);
            }

            return callback(null, finalHash);
        });
    };

    const initialHash = params.hash;

    find(0);
};

const updateErrComplaint = (complaintId, processResult, callback) => {
    const query = `
        UPDATE
            dmca_notice_url
        SET
            ?
        WHERE
            id = ?
    `;
    const data = {
        processed_at: new Date(),
        process_result: processResult,
    };

    dbService.query(query, [data, complaintId], () => {
        return callback();
    });
};

const processComplaint = (complaint, callback) => {
    async.waterfall([
        // Primero decodificar la ruta en el hash correcto
        (cb) => {
            const complaintUrl = url.parse(complaint.url);
            const website = websites.find((w) => w.id === complaint.website_id);

            if (!website) {
                // TODO: Sitio no compatible con ninguno.
                return cb({
                    code: 'could_not_find_website',
                });
            }

            const movieRoute = website.routes.find((r) => r.name === 'movie').values;

            for (const routeVariant in movieRoute) {
                // Obtener ruta
                let routePath = movieRoute[routeVariant];

                if (website.language.multiLanguage) {
                    routePath = '/([a-z]{2}|[a-z]{2}-[A-Z]{2})' + routePath;
                }

                // Parsear ruta
                const keys = [];
                const re = pathToRegexp(routePath, keys);
                const keyIndex = keys.findIndex((k) => k.name === 'hash') + 1;
                const result = re.exec(complaintUrl.pathname);

                if (result && result[keyIndex]) {
                    return cb(null, website, result[keyIndex]);
                }
            }

            return cb({
                code: 'could_not_match_route',
            });
        },
        // Get db connection
        (website, movieHash, cb) => {
            dbService.getConnection((err, dbConnection) => {
                if (err) {
                    return cb({
                        code: 'error_db',
                    }, dbConnection);
                }

                return cb(null, dbConnection, website, movieHash);
            });
        },
        // Transaction
        (dbConnection, website, movieHash, cb) => {
            dbConnection.beginTransaction((err) => {
                if (err) {
                    return cb({
                        code: 'error_transaction',
                    }, dbConnection);
                }

                return cb(null, dbConnection, website, movieHash);
            });
        },
        // Buscar la pelicula basado en el hash
        (dbConnection, website, movieHash, cb) => {
            const query = `
                SELECT
                    mh.movie_id,
                    YEAR(m.release_date) release_year,
                    IFNULL(ml.title, m.title) movie_title,
                    mh.website_id,
                    mh.language_id,
                    mh.language_locale_id,
                    mh.hash,
                    mh.active
                FROM
                    movie_hash mh
                LEFT JOIN
                    movie_language ml ON (ml.movie_id=mh.movie_id AND ml.language_id=mh.language_id AND ml.language_locale_id=mh.language_locale_id)
                JOIN
                    movie m ON m.id=mh.movie_id
                WHERE
                    mh.hash = ? AND
                    mh.active = ? AND
                    (mh.website_id IS NULL OR mh.website_id = ?)
            `;
            // Filtro por activo. Lo que genera es que si ya se regenero ese hash (por eso esta deshabilitado), no haga nada 
            dbConnection.query(query, [movieHash, 1, website.id], (err, foundHash) => {
                if (err) {
                    return cb({
                        code: 'error_searching_movie',
                    }, dbConnection);
                }

                if (!foundHash[0]) {
                    return cb({
                        code: 'movie_hash_not_found',
                    }, dbConnection);
                }

                return cb(null, dbConnection, website, foundHash[0]);
            });
        },
        // Fijarse si hay algun hash activo diferente para el website
        (dbConnection, website, movieHash, cb) => {
            const query = `
                SELECT
                    mh.hash
                FROM
                    movie_hash mh
                WHERE
                    mh.movie_id = ? AND
                    mh.language_id = ? AND
                    mh.language_locale_id = ? AND
                    mh.website_id = ? AND
                    mh.active = ?
            `;

            dbConnection.query(query, [movieHash.movie_id, movieHash.language_id, movieHash.language_locale_id, website.id, 1], (err, hash) => {
                if (err || !hash) {
                    return cb({
                        code: 'error_checking_hash',
                    }, dbConnection);
                }

                // Si existe un hash, y es distinto, quiere decir que ya esta regenerado
                if (hash[0] && hash[0].hash !== movieHash.hash) {
                    return cb({
                        code: 'hash_already_regenerated',
                    }, dbConnection);
                }

                // Si no existe o es el mismo, quiere decir que el hash encontrado previamente es el activo
                return cb(null, dbConnection, website, movieHash);
            });
        },
        // Encontrar un nuevo hash
        (dbConnection, website, movieHash, cb) => {
            const initialHash = utils.normalizeString(movieHash.movie_title);

            if (!initialHash || !initialHash.length) {
                return cb({
                    code: 'movie_hash_empty',
                }, dbConnection);
            }

            const params = {
                table: 'movie_hash',
                column: 'hash',
                hash: initialHash
            };

            findHash(dbConnection, params, (err, newHash) => {
                if (err) {
                    return cb({
                        code: 'error_choosing_new_hash',
                    }, dbConnection);
                }

                return cb(null, dbConnection, website, movieHash, newHash);
            });
        },
        // Obtener todos los antiguos hashes de la pelicula (lo hago antes de insertarlo para despues poder compara con el nuevo estado)
        (dbConnection, website, movieHash, newHash, cb) => {
            const query = `
                SELECT
                    mh.movie_id,
                    mh.website_id,
                    mh.hash,
                    mh.language_id,
                    mh.language_locale_id
                FROM
                    movie_hash mh
                WHERE
                    mh.movie_id = ? AND
                    (mh.website_id IS NULL OR mh.website_id = ?) AND
                    mh.active = 1
            `;

            dbConnection.query(query, [movieHash.movie_id, website.id], (err, oldHashes) => {
                if (err) {
                    return cb({
                        code: 'error_searching_old_hashes',
                    }, dbConnection);
                }

                return cb(null, dbConnection, website, movieHash, newHash, oldHashes);
            });
        },
        // Si el hash original pertenecia especificamente a este website, lo desactivo
        (dbConnection, website, movieHash, newHash, oldHashes, cb) => {
            if (movieHash.website_id !== website.id) {
                return cb(null, dbConnection, website, movieHash, newHash, oldHashes);
            }

            const query = `
                UPDATE
                    movie_hash
                SET
                    active = ?
                WHERE
                    hash = ?
            `;

            dbConnection.query(query, [0, movieHash.hash], (err) => {
                if (err) {
                    return cb({
                        code: 'error_disabling_old_hash',
                    }, dbConnection);
                }

                return cb(null, dbConnection, website, movieHash, newHash, oldHashes);
            });
        },
        // Insertar el nuevo hash
        (dbConnection, website, movieHash, newHash, oldHashes, cb) => {
            const query = `
                INSERT INTO
                    movie_hash
                SET
                    ?
            `;
            const data = {
                movie_id: movieHash.movie_id,
                language_id: movieHash.language_id,
                language_locale_id: movieHash.language_locale_id,
                website_id: website.id,
                hash: newHash,
            };

            dbConnection.query(query, [data], (err) => {
                if (err) {
                    return cb({
                        code: 'error_inserting_new_hash',
                    }, dbConnection);
                }

                return cb(null, dbConnection, website, movieHash, newHash, oldHashes);
            });
        },
        // Obtener TODOS los hashes de la pelicula y controlar si la URL cambio
        (dbConnection, website, movieHash, newHash, oldHashes, cb) => {

            const query = `
                SELECT
                    mh.movie_id,
                    mh.website_id,
                    mh.hash,
                    mh.language_id,
                    mh.language_locale_id
                FROM
                    movie_hash mh
                WHERE
                    mh.movie_id = ? AND
                    (mh.website_id IS NULL OR mh.website_id = ?) AND
                    mh.active = 1
            `;

            dbConnection.query(query, [movieHash.movie_id, website.id], (err, hashes) => {
                if (err) {
                    return cb(err);
                }

                // Proceso los lenguajes de la pagina para encontrar la URL en cada caso.
                // Esto se hace para confirmar que URLS han sido regeneradas.
                let urls = [];
                const movieRoute = website.routes.find((r) => r.name === 'movie').values;
                const defaultLanguage = website.language.available.find((l) => l.default);
                for (const language of website.language.available) {
                    const langId = language.show_locale ? language.language_locale_id : language.language_id;
                    let newBestHash;
                    let oldBestHash;
                    // Si tiene multidioma, usa el lenguaje actual, sino los generos los genera solo con el idioma base
                    if (website.language.multiLanguage) {
                        newBestHash = getBestMovieHash(website, hashes, language, defaultLanguage);
                        oldBestHash = getBestMovieHash(website, oldHashes, language, defaultLanguage);
                    } else {
                        newBestHash = getBestMovieHash(website, hashes, defaultLanguage, defaultLanguage);
                        oldBestHash = getBestMovieHash(website, oldHashes, defaultLanguage, defaultLanguage);
                    }

                    // Si el hash recien creado es el optimo para este idioma, quiere decir que esta URL va a cambiar
                    if (newBestHash.hash === newHash) {
                        const prefix = getLanguagePrefix(website, language);
                        const path = movieRoute[langId] || movieRoute.default;
                        const newLink = website.host + prefix + generateMovieLink(path, movieHash, newBestHash);
                        const oldLink = website.host + prefix + generateMovieLink(path, movieHash, oldBestHash);
                        urls.push({
                            oldLink,
                            newLink,
                        });
                    }
                }

                urls = utils.removeDuplicates(urls, ['oldLink', 'newLink']);

                return cb(null, dbConnection, website, movieHash, urls);
            });
        },
        // Desactivar todas las denuncias que todavia no se han enviado a Google
        (dbConnection, website, movieHash, urls, cb) => {
            if (!urls.length) {
                return cb(null, dbConnection, website, movieHash, urls);
            }

            // Desactivar denuncias del mismo sitio, misma pelicula y que aun no se enviaron
            const query = `
                UPDATE
                    dmca_notice_url_result dnur
                JOIN
                    dmca_notice_url dnu ON dnu.id=dnur.dmca_notice_url_id
                SET 
                    dnur.status = ?
                WHERE
                    dnur.movie_id = ? AND
                    dnu.website_id = ? AND
                    dnur.status = ?
            `;
            dbConnection.query(query, [3, movieHash.movie_id, website.id, 1], (err) => {
                if (err) {
                    return cb({
                        code: 'error_disabling_current_urls',
                    }, dbConnection);    
                }

                return cb(null, dbConnection, website, movieHash, urls);
            });
        },
        // Guardar las nuevas URLS en la DB
        (dbConnection, website, movieHash, urls, cb) => {
            if (!urls.length) {
                return cb(null, dbConnection);
            }

            const now = new Date();
            const data = urls.map((u) => {
                return [
                    complaint.id,
                    movieHash.movie_id,
                    u.oldLink,
                    u.newLink,
                    1,
                    now
                ];
            });

            const query = `
                INSERT INTO
                    dmca_notice_url_result
                    (dmca_notice_url_id, movie_id, old_url, new_url, status, created_at)
                VALUES
                    ?
            `;

            dbConnection.query(query, [data], (err) => {
                if (err) {
                    return cb({
                        code: 'error_saving_new_urls',
                    }, dbConnection);
                }

                return cb(null, dbConnection);
            });
        },
        // Guardar resultado en la db
        (dbConnection, cb) => {
            const query = `
                UPDATE
                    dmca_notice_url
                SET
                    ?
                WHERE
                    id = ?
            `;
            const data = {
                processed_at: new Date(),
                process_result: 'success',
            };

            dbConnection.query(query, [data, complaint.id], (err) => {
                if (err) {
                    return cb({
                        code: 'error_saving_process_result',
                    }, dbConnection);
                }

                return cb(null, dbConnection);
            });
        },
    ], (err, dbConnection) => {
        if (err) {
            if (!dbConnection) {
                return updateErrComplaint(complaint.id, err.code, callback);
            }

            dbConnection.rollback(() => {
                dbConnection.release();
                return updateErrComplaint(complaint.id, err.code, callback);
            });
        } else {
            dbConnection.commit((err) => {
                dbConnection.release();
                if (err) {
                    return updateErrComplaint(complaint.id, 'err_commit', callback);
                }

                return callback();
            });
        }
    });
};

async.waterfall([
    // Procesar websites
    (cb) => {
        websites = getWebsitesConfig();
        return cb(null);
    },
    (cb) => {
        languageHelper.retrieve((err) => {
            if (err) {
                return cb(err);
            }

            return cb(null);
        });
    },
    // Obtener quejas sin procesar
    (cb) => {
        const query = `
            SELECT
                id,
                website_id,
                url,
                created_at
            FROM
                dmca_notice_url
            WHERE
                processed_at IS NULL
        `;

        dbService.query(query, [], (err, complaints) => {
            if (err) {
                return cb(err);
            }
            console.log('Query quejas', complaints);

            return cb(null, complaints);
        });
    },
    // Procesar
    (complaints, cb) => {
        if (!complaints || !complaints.length) {
            logger.info('There is no pending notice', { transactionId });
            return cb();
        }

        async.mapSeries(complaints, processComplaint, (err) => {
            return cb(err);
        });
    },
], (err) => {
    if (err) {
        logger.error('Error executing cronjob', { transactionId, err });
        dbService.endPool();
    } else {
        logger.info('Job executed successfully', { transactionId });
        healthCheck(transactionId, config.HC_HASH_REGENERATOR).then(() => {
            dbService.endPool();
        });
    }
});

const getBestMovieHash = (website, hashes, preferredLanguage, defaultLanguage) => {
    let bestHash;
    // Primero intento agarrar el locale por defecto de la pagina y website correcto
    bestHash = hashes.find((h) => h.language_locale_id === preferredLanguage.language_locale_id && h.website_id === website.id);

    // Si no existe, intento agarrar el locale por defecto de la pagina
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_locale_id === preferredLanguage.language_locale_id && h.website_id === null);
    }

    // Si no existe, intento agarrar el idioma por defecto de la pagina y website correcto
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_id === preferredLanguage.language_id && h.website_id === website.id);
    }

    // Si no existe, intento agarrar el idioma por defecto de la pagina
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_id === preferredLanguage.language_id && h.website_id === null);
    }

    // Si no existe y tiene lenguaje por defecto, intento agarrarlo utilizando el website
    if (defaultLanguage && !bestHash) {
        bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_locale_id && h.website_id === website.id);

        if (!bestHash) {
            bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_id && h.website_id === website.id);
        }
    }

    // Si no existe y tiene lenguaje por defecto, intento agarrarlo
    if (defaultLanguage && !bestHash) {
        bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_locale_id && h.website_id === null);

        if (!bestHash) {
            bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_id && h.website_id === null);
        }
    }

    // Si no existe, agarro ingles + website
    if (!bestHash) {
        const fallbackLangId = languageHelper.getId('en');
        bestHash = hashes.find((h) => h.language_id === fallbackLangId && h.website_id === website.id);
    }

    // Si no existe, agarro ingles
    if (!bestHash) {
        const fallbackLangId = languageHelper.getId('en');
        bestHash = hashes.find((h) => h.language_id === fallbackLangId && h.website_id === null);
    }

    // Si no existe, agarro el primero
    if (!bestHash) {
        bestHash = hashes[0];
    }

    return bestHash;
};

const generateMovieLink = (baseLink, movie, hash) => {
    let url = baseLink;
    url = url.replace(':release_year', movie.release_year).replace(':hash', hash.hash);
    return encodeURI(url);
};

const getLanguagePrefix = (website, currentLanguage) => {
    let prefix = '';

    if (website.language.multiLanguage) {
        let languageId = currentLanguage.show_locale ? currentLanguage.language_locale_id : currentLanguage.language_id;
        const languageIso = languageHelper.getLanguage(languageId).iso;
        prefix = '/' + languageIso;
    }

    return prefix;
};