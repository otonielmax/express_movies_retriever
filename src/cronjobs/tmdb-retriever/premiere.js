'use strict';
const logger = require('@smartadtags/logger');
const async = require('async');
const config = require('config');

const apiService = require('./services/api');
const dbService = require('../../services/db');
const languagesService = require('./services/languages');
const moviesService = require('./services/movie');

logger.config({
    environment: config.LOGGER_ENV,
    service: 'movies-retriever',
    logLevel: config.LOGGER_LOG_LEVEL,
    format: config.LOGGER_FORMAT
});
const transactionId = 'cronjob-tmdb-premiere';

const updatePremiere = (callback) => {
    logger.info('Retrieving "now_playing" movies from TMDB', { transactionId });
    const path = '/movie/now_playing';
    apiService.request(path, {
        method: 'GET',
        json: true,
    }, (err, result, body) => {
        if (err) {
            logger.error('Error obtaining premiere movies', { transactionId, err });
            return callback(err);
        }

        const movies = body.results;

        // Corta si no hay mas peliculas
        if (!movies || !movies.length) {
            logger.info('No hay películas "now_playing"', { transactionId });
            return callback();
        }

        logger.info('Setting "now_playing" flag on DB', { transactionId });
        const movieIds = movies.map((m) => m.id);
        moviesService.setPlaying(movieIds, (err) => {
            return callback(err);
        });
    });
};

async.waterfall([
    (cb) => {
        logger.info('Retrieving languages', { transactionId });
        languagesService.retrieve((err) => {
            return cb(err);
        });
    },
    (cb) => {
        logger.info('Updating premiere movies', { transactionId });
        updatePremiere((err) => {
            return cb(err);
        });
    }
], (err) => {
    if (err) {
        logger.error('Error executing cronjob', { transactionId, err });
    } else {
        logger.info('Job executed successfully', { transactionId });
    }

    dbService.endPool();
});