const async = require('async');
const squel = require('squel');

const Cache = {};

exports.findOrCreate = (dbConnection, params, callback) => {
    let foundOnCache = false;

    const keys = [params.table.toUpperCase()];
    for (let column in params.where) {
        keys.push(params.where[column]);
    }
    const cacheKey = keys.join('_');

    async.waterfall([
        (cb) => {
            if (Cache[cacheKey]) {
                foundOnCache = true;
                return cb(null, Cache[cacheKey]);
            }

            return cb(null, null);
        },
        (entity, cb) => {
            if (entity) {
                return cb(null, entity);
            }

            const query = squel.select();

            // From
            query.from(params.table);

            // Columns
            for (const column of params.columns) {
                query.field(column);
            }

            // Where
            for (const column in params.where) {
                query.where(`${column} = ?`, params.where[column]);
            }

            const preparedQuery = query.toParam();

            dbConnection.query(preparedQuery.text, preparedQuery.values, (err, result) => {
                if (err) {
                    return cb(err);
                }

                return cb(null, result[0]);
            });
        },
        (entity, cb) => {
            if (entity) {
                return cb(null, entity);
            }

            // La crea
            const query = squel.insert();

            // Into
            query.into(params.table);

            for (const key in params.data) {
                query.set(key, params.data[key]);
            }

            const preparedQuery = query.toParam();

            dbConnection.query(preparedQuery.text, preparedQuery.values, (err, result) => {
                if (err) {
                    return cb(err);
                }

                const entity = {
                    ...params.data
                };

                entity.id = result.insertId;

                return cb(null, entity);
            });
        },
        (entity, cb) => {
            if (!foundOnCache) {
                Cache[cacheKey] = entity;
            }

            return cb(null, entity);
        },
    ], (err, entity) => {
        if (err) {
            return callback(new Error(err.message));
        }

        return callback(null, entity);
    });
};

exports.findHash = (dbConnection, params, callback) => {
    const find = (tryNum) => {
        const hash = initialHash;

        let maxLength = 100;
        let tryStr = '';
        let finalHash = hash;

        if (tryNum) {
            tryStr = '-' + tryNum;
            maxLength -= tryStr.length;
        }

        if (finalHash.length > maxLength) {
            finalHash = finalHash.substr(0, maxLength);
        }
        if (tryStr.length) {
            finalHash += tryStr;
        }

        const query = squel.select();

        // From
        query.from(params.table);

        // Column
        query.field('COUNT(*)', 'count');

        // Where
        query.where(`${params.column} = ?`, finalHash);


        const preparedQuery = query.toParam();

        dbConnection.query(preparedQuery.text, preparedQuery.values, (err, result) => {
            if (err) {
                return callback(err);
            }

            if (!result || !result[0]) {
                return callback(new Error('Error extraño'));
            }

            if (result[0].count && result[0].count > 0) {
                return find(tryNum + 1);
            }

            return callback(null, finalHash);
        });
    };

    const initialHash = params.hash;

    find(0);
};

exports.removeAndInsert = (dbConnection, params, callback) => {
    async.waterfall([
        (cb) => {
            const query = squel.delete().from(params.table);

            for (const column in params.where) {
                query.where(`${column} = ?`, params.where[column]);
            }

            const preparedQuery = query.toParam();

            dbConnection.query(preparedQuery.text, preparedQuery.values, (err) => {
                if (err) {
                    return cb(err);
                }

                return cb(null);
            });
        },
        (cb) => {
            const query = `INSERT INTO ${params.table} (${params.columns.join(', ')}) VALUES ? `;
            
            dbConnection.query(query, [params.data], (err) => {
                if (err) {
                    return cb(err);
                }

                return cb(null);
            });
        }
    ], (err) => {
        if (err) {
            return callback(err);
        }

        return callback(null);
    });
};