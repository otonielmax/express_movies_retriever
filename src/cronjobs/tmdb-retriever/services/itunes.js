'use strict';

const request = require('request');

exports.findMovie = (movieTitle, callback) => {
    exports.search(movieTitle, (err, videos) => {
        if (err) {
            return callback(err);
        }

        if (!videos || !videos.length) {
            return callback(null);
        }

        return callback(null, videos[0]);
    });
};

exports.search = (movieTitle, callback) => {
    const searchTerm = encodeURI(movieTitle.replace(/ /g, '+'));
    const apiUrl = `https://itunes.apple.com/search?media=movie&term=${searchTerm}`;

    request({
        method: 'GET',
        url: apiUrl,
        json: true,
    }, (err, result, body) => {
        if (err) {
            return callback(err);
        }

        if (!body || !body.results) {
            return callback(new Error('Invalid response'));
        }

        return callback(null, body.results);
    });
};