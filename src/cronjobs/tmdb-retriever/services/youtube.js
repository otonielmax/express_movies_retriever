'use strict';

const config = require('config');
const request = require('request');

exports.findReview = (movieTitle, callback) => {
    const searchTerm = `${movieTitle} movie review`;
    exports.search(searchTerm, (err, videos) => {
        if (err) {
            return callback(err);
        }

        if (!videos || !videos.length) {
            return callback(null);
        }

        return callback(null, videos[0]);
    });
};


exports.search = (searchTerm, callback) => {
    const term = encodeURIComponent(searchTerm);
    const url = `https://www.googleapis.com/youtube/v3/search?key=${config.YOUTUBE_API_KEY}&part=id,snippet&q=${term}`;

    request.get({
        url,
        json: true,
    }, (err, result, body) => {
        if (err) {
            return callback(err);
        }

        let videos = body.items;

        if (!videos || !videos.length) {
            return callback(null, []);
        }

        videos = videos.map((v) => {
            return {
                key: v.id.videoId,
                title: v.snippet.title,
                description: v.snippet.description,
            };
        });

        return callback(null, videos);
    });
};