'use strict';
const dbService = require('../../../services/db');

const Languages = {};

exports.retrieve = (callback) => {
    const query = 'SELECT * FROM language';

    dbService.query(query, [], (err, languages) => {
        if (err) {
            return callback(err);
        }

        for (let lang of languages) {
            Languages[lang.iso] = lang.id;
        }

        return callback(null);
    });
};

exports.getLanguage = (iso, locale) => {
    if (!Languages[iso]) {
        return null;
    }

    return {
        language_id: Languages[iso],
        language_locale_id: locale ? Languages[iso + '-' + locale] || null : null,
    };
};