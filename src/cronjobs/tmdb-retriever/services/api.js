'use strict';
const config = require('config');
const moment = require('moment');
const request = require('request');

const requests = {};

/**
 * Realiza un request a la API.
 * Controla no pasarse del maximo permitido.
 */
exports.request = (path, params, callback) => {
    const wait = (cb) => {
        if (canPerformRequest()) {
            return cb();
        } else {
            return setTimeout(wait, 100, cb);
        }
    };

    wait(() => {
        requestPerformed();

        // Armar url
        params.url = 'https://api.themoviedb.org/3' + path;

        if (params.url.indexOf('?') === -1) {
            params.url += '?';
        } else {
            params.url += '&';
        }
        params.url += `api_key=${config.TMDB_API_KEY}`;

        request(params, (err, result, body) => {
            if (result && result.statusCode === 429) {
                return module.exports.request(path, params, callback);
            }

            return callback(err, result, body);
        });
    });
};

/**
 * Consulta si es posible realizar el request a la API.
 * Solo permite 4 requests por segundo.
 */
const canPerformRequest = () => {
    const now = moment().unix();

    // Si el segundo no tiene datos, o si la cantidad de requests fue menor a 4.
    return (requests.time !== now || requests.amount < 4);
};

/**
 * Incrementa el contador de requests por segundo a la API.
 */
const requestPerformed = () => {
    const now = moment().unix();

    if (requests.time === now) {
        // Si la hora es la misma, incrementa el contador.
        requests.amount++;
    } else {
        // Si cambio el segundo, empieza de 1.
        requests.time = now;
        requests.amount = 1;
    }
};