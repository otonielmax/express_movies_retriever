'use strict';
const async = require('async');
const config = require('config');
const moment = require('moment');

const constants = require('../config/constants');
const dbService = require('../../../services/db');
const apiService = require('./api');
const youtubeService = require('./youtube');
const itunesService = require('./itunes');
const languagesService = require('./languages');
const entitiesService = require('./entities');
const utils = require('../../../helpers/utils');

const MOVIES_MIN_POPULARITY = Number(config.MOVIES_MIN_POPULARITY);

exports.createOrUpdate = (externalMovie, callback) => {
    const query = `
        SELECT
            *
        FROM
            movie
        WHERE
            external_id = ?
    `;

    dbService.query(query, [externalMovie.id], (err, movie) => {
        if (err || !movie) {
            return callback({
                message: 'ERROR_OBTAINING_MOVIE_DB'
            });
        }

        const [foundMovie] = movie;

        if (foundMovie) {
            return exports.update(externalMovie, callback);
        } else {
            return exports.create(externalMovie, callback);
        }
    });
};

exports.setPlaying = (movieIds, callback) => {
    async.waterfall([
        // Get db connection
        (cb) => {
            dbService.getConnection((err, connection) => {
                if (err) {
                    return cb({
                        message: 'ERROR_CONNECTION',
                        err
                    }, connection);
                }

                return cb(null, connection);
            });
        },
        // Transaction
        (dbConnection, cb) => {
            dbConnection.beginTransaction((err) => {
                if (err) {
                    return cb({
                        message: 'ERROR_TRANSACTION',
                        err
                    }, dbConnection);
                }

                return cb(null, dbConnection);
            });
        },
        // Delete al playing flags
        (dbConnection, cb) => {
            const query = `
                UPDATE
                    movie
                SET
                    now_playing = 0
                WHERE
                    now_playing = 1
            `;
            dbConnection.query(query, [], (err) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                return cb(null, dbConnection);
            });
        },
        // Delete al playing flags
        (dbConnection, cb) => {
            const query = `
                UPDATE
                    movie
                SET
                    now_playing = 1
                WHERE
                    external_id IN (?)
            `;
            dbConnection.query(query, [movieIds], (err) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                return cb(null, dbConnection);
            });
        },
    ], (err, dbConnection) => {
        if (err) {
            if (!dbConnection) {
                return callback(err);
            }

            dbConnection.rollback(() => {
                dbConnection.release();
                return callback(err);
            });
        } else {
            dbConnection.commit((err) => {
                dbConnection.release();
                return callback(err);
            });
        }
    });
};

exports.update = (externalMovie, callback) => {
    console.log(`       UPDATING ${externalMovie.title} (${externalMovie.id})`);

    let movieUpdated = false;
    async.waterfall([
        // Get db connection
        (cb) => {
            dbService.getConnection((err, connection) => {
                if (err) {
                    return cb({
                        message: 'ERROR_CONNECTION',
                        err
                    }, connection);
                }

                return cb(null, connection);
            });
        },
        // Transaction
        (dbConnection, cb) => {
            dbConnection.beginTransaction((err) => {
                if (err) {
                    return cb({
                        message: 'ERROR_TRANSACTION',
                        err
                    }, dbConnection);
                }

                return cb(null, dbConnection);
            });
        },
        // Get movie data
        (dbConnection, cb) => {
            const path = `/movie/${externalMovie.id}?append_to_response=credits,keywords,videos,translations,external_ids,release_dates,alternative_titles`;
            const params = {
                method: 'GET',
                json: true,
            };

            apiService.request(path, params, (err, result, movie) => {
                if (err || !result || result.statusCode !== 200) {
                    return cb({
                        message: 'ERROR_OBTAINING_MOVIE_API',
                        continue: true,
                    }, dbConnection);
                }

                return cb(null, dbConnection, movie);
            });
        },
        // Get movie id
        (dbConnection, movie, cb) => {
            const query = 'SELECT id FROM movie WHERE external_id = ?';

            dbConnection.query(query, [externalMovie.id], (err, result) => {
                if (err) {
                    return cb({
                        message: 'ERROR_GETTING_MOVIE',
                        err
                    }, dbConnection);
                }

                if (!result || !result.length) {
                    return cb({
                        message: 'INVALID_MOVIE_ID'
                    }, dbConnection);
                }

                return cb(null, dbConnection, movie, result[0].id);
            });
        },
        // Insert status if needed
        (dbConnection, movie, movieId, cb) => {
            if (!movie.status || !movie.status.length) {
                movie.movie_status_id = null;

                return cb(null, dbConnection, movie, movieId);
            }

            movie.status = movie.status.toUpperCase().replace(/ /g, '_');

            const params = {
                table: 'movie_status',
                columns: ['id', 'name'],
                where: {
                    name: movie.status,
                },
                data: {
                    name: movie.status,
                },
            };

            entitiesService.findOrCreate(dbConnection, params, (err, entity) => {
                if (err) {
                    return cb({
                        message: 'ERROR_PARSING_STATUS',
                        err
                    }, dbConnection);
                }

                movie.movie_status_id = entity.id;

                return cb(null, dbConnection, movie, movieId);
            });
        },
        // Update hash
        (dbConnection, movie, movieId, cb) => {
            let movieNames = [];
            const defaultLanguages = ['en', 'es', 'pt', 'fr', 'it'];

            if (movie.translations && movie.translations.translations) {
                for (let translation of movie.translations.translations) {
                    if (translation.data.title && translation.data.title.length) {
                        const isDefault = defaultLanguages.includes(translation.iso_639_1);
                        const lang = languagesService.getLanguage(translation.iso_639_1, translation.iso_3166_1);
                        if (isDefault && lang) {
                            let title = utils.normalizeString(translation.data.title);
                            if (title && title.length) {
                                movieNames.push({
                                    title,
                                    original_title: translation.data.title && translation.data.title.length ? translation.data.title : null,
                                    language_id: lang.language_id,
                                    language_locale_id: lang.language_locale_id,
                                });
                            }
                        }
                    }
                }
            }

            movieNames = utils.removeDuplicates(movieNames, ['title']);

            // Si no tengo nombres de pelicula en un lenguaje bueno, corto.
            if (!movieNames.length) {
                return cb(null, dbConnection, movie, movieId);
            }

            updateMovieHashes(movieNames, movieId, dbConnection, (err) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                return cb(null, dbConnection, movie, movieId);
            });
        },
        // Update genres
        (dbConnection, movie, movieId, cb) => {
            if (!movie.genres || !movie.genres.length) {
                return cb(null, dbConnection, movie, movieId);
            }

            // Eliminar repetidos (se dieron casos que estaban repetidos en el arreglo)
            movie.genres = utils.removeDuplicates(movie.genres, ['id']);

            async.mapSeries(movie.genres, (genre, cb) => {
                const params = {
                    table: 'genre',
                    columns: ['id', 'name', 'external_id'],
                    where: {
                        external_id: genre.id
                    },
                    data: {
                        name: genre.name.toUpperCase().replace(/ /g, '_'),
                        external_id: genre.id
                    },
                };

                entitiesService.findOrCreate(dbConnection, params, (err, entity) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PARSING_GENRE',
                            err
                        });
                    }

                    const insert = [
                        movieId,
                        entity.id
                    ];

                    return cb(null, insert);
                });
            }, (err, parsedGenres) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!parsedGenres || !parsedGenres.length) {
                    return cb(null, dbConnection, movie, movieId);
                }

                const params = {
                    table: 'movie_genre',
                    columns: ['movie_id', 'genre_id'],
                    where: {
                        movie_id: movieId
                    },
                    data: parsedGenres,
                };

                entitiesService.removeAndInsert(dbConnection, params, (err) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_UPDATING_GENRE',
                            err
                        }, dbConnection);
                    }

                    return cb(null, dbConnection, movie, movieId);
                });
            });
        },
        // update companies
        (dbConnection, movie, movieId, cb) => {
            if (!movie.production_companies || !movie.production_companies.length) {
                return cb(null, dbConnection, movie, movieId);
            }

            // Eliminar repetidos (se dieron casos que estaban repetidos en el arreglo)
            movie.production_companies = utils.removeDuplicates(movie.production_companies, ['id']);

            async.mapSeries(movie.production_companies, (company, cb) => {
                const params = {
                    table: 'company',
                    columns: ['id', 'name', 'logo_path', 'external_id'],
                    where: {
                        external_id: company.id
                    },
                    data: {
                        name: company.name,
                        logo_path: company.logo_path,
                        external_id: company.id
                    },
                };

                entitiesService.findOrCreate(dbConnection, params, (err, entity) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PARSING_COMPANY',
                            err
                        });
                    }

                    const insert = [
                        movieId,
                        entity.id
                    ];

                    return cb(null, insert);
                });
            }, (err, parsedCompanies) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!parsedCompanies || !parsedCompanies.length) {
                    return cb(null, dbConnection, movie, movieId);
                }

                const params = {
                    table: 'movie_company',
                    columns: ['movie_id', 'company_id'],
                    where: {
                        movie_id: movieId
                    },
                    data: parsedCompanies,
                };

                entitiesService.removeAndInsert(dbConnection, params, (err) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_UPDATING_COMPANY',
                            err
                        }, dbConnection);
                    }

                    return cb(null, dbConnection, movie, movieId);
                });
            });
        },
        // update countries
        (dbConnection, movie, movieId, cb) => {
            if (!movie.production_countries || !movie.production_countries.length) {
                return cb(null, dbConnection, movie, movieId);
            }

            // Eliminar repetidos (se dieron casos que estaban repetidos en el arreglo)
            movie.production_countries = utils.removeDuplicates(movie.production_countries, ['id']);

            async.mapSeries(movie.production_countries, (country, cb) => {
                const params = {
                    table: 'country',
                    columns: ['id', 'name', 'iso_3166'],
                    where: {
                        iso_3166: country.iso_3166_1
                    },
                    data: {
                        name: country.name,
                        iso_3166: country.iso_3166_1,
                    },
                };

                entitiesService.findOrCreate(dbConnection, params, (err, entity) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PARSING_COUNTRY',
                            err
                        });
                    }

                    const insert = [
                        movieId,
                        entity.id
                    ];

                    return cb(null, insert);
                });
            }, (err, parsedCountry) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!parsedCountry || !parsedCountry.length) {
                    return cb(null, dbConnection, movie, movieId);
                }

                const params = {
                    table: 'movie_country',
                    columns: ['movie_id', 'country_id'],
                    where: {
                        movie_id: movieId
                    },
                    data: parsedCountry,
                };

                entitiesService.removeAndInsert(dbConnection, params, (err) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_UPDATING_COUNTRY',
                            err
                        }, dbConnection);
                    }

                    return cb(null, dbConnection, movie, movieId);
                });
            });
        },
        // update performer
        (dbConnection, movie, movieId, cb) => {
            if (!movie.credits || !movie.credits.cast || !movie.credits.cast.length) {
                return cb(null, dbConnection, movie, movieId);
            }

            async.mapSeries(movie.credits.cast, (performer, cb) => {
                createPerson(dbConnection, performer, (err, entity) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PARSING_PERFORMERS',
                            err
                        });
                    }

                    const insert = [
                        movieId,
                        entity.id,
                        performer.character,
                        performer.order
                    ];

                    return cb(null, insert);
                });
            }, (err, parsedPerformers) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!parsedPerformers || !parsedPerformers.length) {
                    return cb(null, dbConnection, movie, movieId);
                }

                const params = {
                    table: 'movie_performer',
                    columns: ['movie_id', 'person_id', '`character`', '`order`'],
                    where: {
                        movie_id: movieId
                    },
                    data: parsedPerformers,
                };

                entitiesService.removeAndInsert(dbConnection, params, (err) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_UPDATING_PERFORMER',
                            err
                        }, dbConnection);
                    }

                    return cb(null, dbConnection, movie, movieId);
                });
            });
        },
        // update crew
        (dbConnection, movie, movieId, cb) => {
            if (!movie.credits || !movie.credits.crew || !movie.credits.crew.length) {
                return cb(null, dbConnection, movie, movieId);
            }

            async.mapSeries(movie.credits.crew, (crew, cb) => {
                createPerson(dbConnection, crew, (err, entity) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PARSING_CREW',
                            err
                        });
                    }

                    const insert = [
                        movieId,
                        entity.id,
                        crew.job,
                        crew.department
                    ];

                    return cb(null, insert);
                });
            }, (err, parsedCrew) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!parsedCrew || !parsedCrew.length) {
                    return cb(null, dbConnection, movie, movieId);
                }

                const params = {
                    table: 'movie_crew',
                    columns: ['movie_id', 'person_id', 'job', 'department'],
                    where: {
                        movie_id: movieId
                    },
                    data: parsedCrew,
                };

                entitiesService.removeAndInsert(dbConnection, params, (err) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_UPDATING_CREW',
                            err,
                        }, dbConnection);
                    }

                    return cb(null, dbConnection, movie, movieId);
                });
            });
        },
        // update keywords
        (dbConnection, movie, movieId, cb) => {
            if (!movie.keywords || !movie.keywords.keywords || !movie.keywords.keywords.length) {
                return cb(null, dbConnection, movie, movieId);
            }

            // Eliminar repetidos (se dieron casos que estaban repetidos en el arreglo)
            movie.keywords.keywords = utils.removeDuplicates(movie.keywords.keywords, ['id']);

            async.mapSeries(movie.keywords.keywords, (keyword, cb) => {
                const params = {
                    table: 'keyword',
                    columns: ['id', 'name', 'external_id'],
                    where: {
                        external_id: keyword.id
                    },
                    data: {
                        name: keyword.name,
                        external_id: keyword.id,
                    },
                };

                entitiesService.findOrCreate(dbConnection, params, (err, entity) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PARSING_KEYWORD',
                            err
                        });
                    }

                    const insert = [
                        movieId,
                        entity.id
                    ];

                    return cb(null, insert);
                });
            }, (err, parsedKeyword) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!parsedKeyword || !parsedKeyword.length) {
                    return cb(null, dbConnection, movie, movieId);
                }

                const params = {
                    table: 'movie_keyword',
                    columns: ['movie_id', 'keyword_id'],
                    where: {
                        movie_id: movieId
                    },
                    data: parsedKeyword,
                };

                entitiesService.removeAndInsert(dbConnection, params, (err) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_UPDATING_KEYWORD',
                            err
                        }, dbConnection);
                    }

                    return cb(null, dbConnection, movie, movieId);
                });
            });
        },
        // update languages and translations
        (dbConnection, movie, movieId, cb) => {
            if (!movie.translations || !movie.translations.translations || !movie.translations.translations.length) {
                return cb(null, dbConnection, movie, movieId);
            }

            // Eliminar repetidos (se dieron casos que estaban repetidos en el arreglo)
            movie.translations.translations = utils.removeDuplicates(movie.translations.translations, ['iso_639_1', 'iso_3166_1']);

            let parsedLanguages = [];

            if (movie.translations && movie.translations.translations) {
                for (let translation of movie.translations.translations) {
                    const lang = languagesService.getLanguage(translation.iso_639_1, translation.iso_3166_1);
                    if (lang) {
                        parsedLanguages.push({
                            movie_id: movieId,
                            language_id: lang.language_id,
                            language_locale_id: lang.language_locale_id,
                            title: translation.data.title && translation.data.title.length ? translation.data.title : null,
                            overview: translation.data.overview && translation.data.overview.length ? translation.data.overview : null,
                            homepage: translation.data.homepage && translation.data.homepage.length ? translation.data.homepage : null,
                            filtered: false
                        });
                    }
                }
            }

            if (!parsedLanguages || !parsedLanguages.length) {
                return cb(null, dbConnection, movie, movieId);
            }

            // Eliminar repetidos otra vez, ya que en idiomas chinos no detecta el locale y queda duplicado
            parsedLanguages = utils.removeDuplicates(parsedLanguages, ['language_id', 'language_locale_id']);

            updateMovieLanguages(parsedLanguages, movieId, dbConnection, (err, shouldUpdate) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (shouldUpdate) {
                    movieUpdated = true;
                }

                return cb(null, dbConnection, movie, movieId);
            });
        },
        // update videos
        (dbConnection, movie, movieId, cb) => {
            if (!movie.videos || !movie.videos.results || !movie.videos.results.length) {
                return cb(null, dbConnection, movie, movieId);
            }

            // Filtrar duplicados
            movie.videos.results = utils.removeDuplicates(movie.videos.results, ['id']);

            const parsedVideos = [];

            for (let video of movie.videos.results) {
                const lang = languagesService.getLanguage(video.iso_639_1, video.iso_3166_1);

                if (lang) {
                    parsedVideos.push([
                        movieId,
                        video.id,
                        lang.language_id,
                        lang.language_locale_id,
                        video.name,
                        video.site,
                        video.key,
                        video.size,
                        video.type
                    ]);
                }
            }

            if (!parsedVideos || !parsedVideos.length) {
                return cb(null, dbConnection, movie, movieId);
            }

            const params = {
                table: 'video',
                columns: ['movie_id', 'external_id', 'language_id', 'language_locale_id', 'name', 'site', '`key`', 'size', 'type'],
                where: {
                    movie_id: movieId
                },
                data: parsedVideos,
            };

            entitiesService.removeAndInsert(dbConnection, params, (err) => {
                if (err) {
                    return cb({
                        message: 'ERROR_UPDATING_VIDEO',
                        err
                    }, dbConnection);
                }

                return cb(null, dbConnection, movie, movieId);
            });
        },
        // update review if neccesary
        (dbConnection, movie, movieId, cb) => {
            const query = `
                SELECT
                    created_at
                FROM
                    movie_review
                WHERE
                    movie_id = ?
            `;

            dbConnection.query(query, [movieId], (err, result) => {
                if (err) {
                    return cb({
                        message: 'ERROR_UPDATING_REVIEW',
                        err
                    }, dbConnection);
                }

                // Solo se actualiza una vez cada 60 dias
                if (result && result[0]) {
                    const createdAt = moment(result[0].created_at);

                    if (createdAt.diff(moment().subtract(2, 'month'), 'days') >= 0) {
                        return cb(null, dbConnection, movie, movieId);
                    }
                }

                insertMovieReview(movieId, movie.title, dbConnection, (err) => {
                    if (err) {
                        return cb({
                            message: err.toString(),
                            err
                        }, dbConnection);
                    }

                    return cb(null, dbConnection, movie, movieId);
                });
            });
        },
        // update movie providers  if neccesary
        (dbConnection, movie, movieId, cb) => {
            // if movie is not yet released, skip this step
            if (moment(movie.release_date).isAfter(moment())) {
                return cb(null, dbConnection, movie, movieId);
            }

            const query = `
                        SELECT
                            created_at
                        FROM
                            movie_provider
                        WHERE
                            movie_id = ?
                    `;

            dbConnection.query(query, [movieId], (err, result) => {
                if (err) {
                    return cb({
                        message: 'ERROR_UPDATING_REVIEW',
                        err
                    }, dbConnection);
                }

                // Solo se actualiza una vez cada 60 dias
                if (result && result[0]) {
                    const createdAt = moment(result[0].created_at);

                    if (createdAt.diff(moment().subtract(2, 'month'), 'days') >= 0) {
                        return cb(null, dbConnection, movie, movieId);
                    }
                }

                insertMovieProvider(movieId, movie.title, dbConnection, (err) => {
                    if (err) {
                        return cb({
                            message: err.toString(),
                            err
                        }, dbConnection);
                    }

                    return cb(null, dbConnection, movie, movieId);
                });
            });
        },
        // Update basic movie data
        (dbConnection, movie, movieId, cb) => {
            const data = {
                external_id: movie.id,
                title: movie.title,
                original_title: movie.original_title,
                overview: movie.overview,
                tagline: movie.tagline && movie.tagline.length ? movie.tagline : null,
                popularity: movie.popularity,
                poster_path: movie.poster_path,
                backdrop_path: movie.backdrop_path,
                release_date: movie.release_date,
                runtime: movie.runtime,
                homepage: movie.homepage,
                budget: movie.budget,
                revenue: movie.revenue,
                vote_average: movie.vote_average,
                vote_count: movie.vote_count,
                adult: movie.adult,
                imdb_id: movie.imdb_id && movie.imdb_id.length ? movie.imdb_id : null,
                movie_status_id: movie.movie_status_id,
            };

            if (movieUpdated) {
                data.updated_at = new Date();
            }

            const query = `
                UPDATE
                    movie
                SET
                    ?
                WHERE
                    id = ?
            `;

            dbConnection.query(query, [data, movieId], (err) => {
                if (err) {
                    return cb({
                        message: 'ERROR_UPDATING_MOVIE',
                        err
                    }, dbConnection);
                }

                return cb(null, dbConnection, movie, movieId);
            });
        },
    ], (err, dbConnection) => {
        if (err) {
            let parsedErr = err.continue ? null : err;
            if (!dbConnection) {
                return callback(parsedErr);
            }

            dbConnection.rollback(() => {
                dbConnection.release();
                return callback(parsedErr);
            });
        } else {
            dbConnection.commit((err) => {
                dbConnection.release();
                return callback(err);
            });
        }
    });
};

exports.create = (externalMovie, callback) => {
    // Filtrar por popularidad
    if (externalMovie.popularity < MOVIES_MIN_POPULARITY) {
        return callback();
    }

    // Filtrar por fecha de publicacion
    const maxDate = moment().add(6, 'months').format('YYYY/MM/DD');
    if (!externalMovie.release_date || externalMovie.release_date >= maxDate) {
        return callback();
    }

    console.log(`       CREATING ${externalMovie.title} (${externalMovie.id})`);
    let movieId;
    async.waterfall([
        // Get db connection
        (cb) => {
            dbService.getConnection((err, connection) => {
                if (err) {
                    return cb({
                        message: 'ERROR_CONNECTION',
                        err
                    });
                }

                return cb(null, connection);
            });
        },
        // Transaction
        (dbConnection, cb) => {
            dbConnection.beginTransaction((err) => {
                if (err) {
                    return cb({
                        message: 'ERROR_TRANSACTION',
                        err
                    }, dbConnection);
                }

                return cb(null, dbConnection);
            });
        },
        // Get movie data
        (dbConnection, cb) => {
            const path = `/movie/${externalMovie.id}?append_to_response=credits,keywords,videos,translations,external_ids,release_dates,alternative_titles`;
            const params = {
                method: 'GET',
                json: true,
            };

            apiService.request(path, params, (err, result, movie) => {
                if (err || !result || result.statusCode !== 200) {
                    // console.log(`Error in request: ${url} - ${err ? err.toString() : 'no error' }`);
                    return cb({
                        message: 'ERROR_OBTAINING_MOVIE_API',
                        continue: true,
                    }, dbConnection);
                }

                return cb(null, dbConnection, movie);
            });

        },
        // Insert status if needed
        (dbConnection, movie, cb) => {
            if (!movie.status || !movie.status.length) {
                movie.movie_status_id = null;

                return cb(null, dbConnection, movie);
            }

            movie.status = movie.status.toUpperCase().replace(/ /g, '_');

            const params = {
                table: 'movie_status',
                columns: ['id', 'name'],
                where: {
                    name: movie.status,
                },
                data: {
                    name: movie.status,
                },
            };

            entitiesService.findOrCreate(dbConnection, params, (err, entity) => {
                if (err) {
                    return cb({
                        message: 'ERROR_PARSING_STATUS',
                        err
                    }, dbConnection);
                }

                movie.movie_status_id = entity.id;

                return cb(null, dbConnection, movie);
            });
        },
        // Insert basic movie data
        (dbConnection, movie, cb) => {
            const now = new Date();
            const data = {
                external_id: movie.id,
                title: movie.title,
                original_title: movie.original_title,
                overview: movie.overview,
                tagline: movie.tagline && movie.tagline.length ? movie.tagline : null,
                popularity: movie.popularity,
                poster_path: movie.poster_path,
                backdrop_path: movie.backdrop_path,
                release_date: movie.release_date,
                runtime: movie.runtime,
                homepage: movie.homepage,
                budget: movie.budget,
                revenue: movie.revenue,
                vote_average: movie.vote_average,
                vote_count: movie.vote_count,
                adult: movie.adult,
                imdb_id: movie.imdb_id && movie.imdb_id.length ? movie.imdb_id : null,
                movie_status_id: movie.movie_status_id,
                created_at: now,
                updated_at: now,
            };

            if (movie.external_ids) {
                if (movie.external_ids.facebook_id) {
                    data.facebook_id = movie.external_ids.facebook_id;
                }
                if (movie.external_ids.twitter_id) {
                    data.twitter_id = movie.external_ids.twitter_id;
                }
                if (movie.external_ids.instagram_id) {
                    data.instagram_id = movie.external_ids.instagram_id;
                }
            }

            const query = 'INSERT INTO movie SET ?';

            dbConnection.query(query, [data], (err, result) => {
                if (err) {
                    return cb({
                        message: 'ERROR_INSERTING_MOVIE',
                        err
                    }, dbConnection);
                }

                movieId = result.insertId;

                return cb(null, dbConnection, movie);
            });
        },
        // Choose hash
        (dbConnection, movie, cb) => {
            let movieNames = [];

            // Idioma principal
            const englishTitle = utils.normalizeString(movie.title);
            if (englishTitle && englishTitle.length) {
                movieNames.push({
                    title: englishTitle,
                    language_id: languagesService.getLanguage('en').language_id,
                    language_locale_id: languagesService.getLanguage('en', 'US').language_locale_id,
                });
            }

            const defaultLanguages = ['en', 'es', 'pt', 'fr', 'it'];

            if (movie.translations && movie.translations.translations) {
                for (let translation of movie.translations.translations) {
                    if (translation.data.title && translation.data.title.length) {
                        const isDefault = defaultLanguages.includes(translation.iso_639_1);
                        const lang = languagesService.getLanguage(translation.iso_639_1, translation.iso_3166_1);
                        if (isDefault && lang) {
                            let title = utils.normalizeString(translation.data.title);
                            if (title && title.length) {
                                movieNames.push({
                                    title,
                                    language_id: lang.language_id,
                                    language_locale_id: lang.language_locale_id,
                                });
                            }
                        }
                    }
                }
            }

            movieNames = utils.removeDuplicates(movieNames, ['title']);

            // Si no tengo nombres de pelicula en un lenguaje bueno, corto.
            if (!movieNames.length) {
                return cb({
                    message: 'NO_LANGUAGES',
                    continue: true,
                }, dbConnection);
            }

            async.mapSeries(movieNames, (movieName, cb) => {
                const params = {
                    table: 'movie_hash',
                    column: 'hash',
                    hash: movieName.title
                };

                entitiesService.findHash(dbConnection, params, (err, hash) => {
                    if (err) {
                        return cb(err);
                    }

                    const result = [movieId, movieName.language_id, movieName.language_locale_id, hash];

                    return cb(null, result);
                });
            }, (err, hashes) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                // Remuevo duplicados a partir del hash (4 posicion del arreglo de valores)
                hashes = utils.removeDuplicates(hashes, [3]);

                const query = 'INSERT INTO movie_hash (movie_id, language_id, language_locale_id, hash) VALUES ?';
                dbConnection.query(query, [hashes], (err) => {
                    if (err) {
                        return cb(err, dbConnection);
                    }

                    return cb(null, dbConnection, movie);
                });
            });
        },
        // Insert genres
        (dbConnection, movie, cb) => {
            if (!movie.genres || !movie.genres.length) {
                return cb(null, dbConnection, movie);
            }

            // Eliminar repetidos (se dieron casos que estaban repetidos en el arreglo)
            movie.genres = utils.removeDuplicates(movie.genres, ['id']);

            async.mapSeries(movie.genres, (genre, cb) => {
                const params = {
                    table: 'genre',
                    columns: ['id', 'name', 'external_id'],
                    where: {
                        external_id: genre.id
                    },
                    data: {
                        name: genre.name.toUpperCase().replace(/ /g, '_'),
                        external_id: genre.id
                    },
                };

                entitiesService.findOrCreate(dbConnection, params, (err, entity) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PARSING_GENRE',
                            err
                        });
                    }

                    const insert = [movieId, entity.id];

                    return cb(null, insert);
                });
            }, (err, parsedGenres) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!parsedGenres || !parsedGenres.length) {
                    return cb(null, dbConnection, movie);
                }

                const query = 'INSERT INTO movie_genre (movie_id, genre_id) VALUES ?';

                dbConnection.query(query, [parsedGenres], (err) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PROCESSING_GENRE',
                            err
                        }, dbConnection);
                    }

                    return cb(null, dbConnection, movie);
                });
            });
        },
        // Insert companies
        (dbConnection, movie, cb) => {
            if (!movie.production_companies || !movie.production_companies.length) {
                return cb(null, dbConnection, movie);
            }

            // Eliminar repetidos (se dieron casos que estaban repetidos en el arreglo)
            movie.production_companies = utils.removeDuplicates(movie.production_companies, ['id']);

            async.mapSeries(movie.production_companies, (company, cb) => {
                const params = {
                    table: 'company',
                    columns: ['id', 'name', 'logo_path', 'external_id'],
                    where: {
                        external_id: company.id
                    },
                    data: {
                        name: company.name,
                        logo_path: company.logo_path,
                        external_id: company.id
                    },
                };

                entitiesService.findOrCreate(dbConnection, params, (err, entity) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PARSING_COMPANY',
                            err
                        });
                    }

                    const insert = [movieId, entity.id];

                    return cb(null, insert);
                });
            }, (err, parsedCompanies) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!parsedCompanies || !parsedCompanies.length) {
                    return cb(null, dbConnection, movie);
                }

                const query = 'INSERT INTO movie_company (movie_id, company_id) VALUES ?';

                dbConnection.query(query, [parsedCompanies], (err) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PROCESSING_COMPANIES',
                            err
                        }, dbConnection);
                    }

                    return cb(null, dbConnection, movie);
                });
            });
        },
        // Insert countries
        (dbConnection, movie, cb) => {
            if (!movie.production_countries || !movie.production_countries.length) {
                return cb(null, dbConnection, movie);
            }

            // Eliminar repetidos (se dieron casos que estaban repetidos en el arreglo)
            movie.production_countries = utils.removeDuplicates(movie.production_countries, ['id']);

            async.mapSeries(movie.production_countries, (country, cb) => {
                const params = {
                    table: 'country',
                    columns: ['id', 'name', 'iso_3166'],
                    where: {
                        iso_3166: country.iso_3166_1
                    },
                    data: {
                        name: country.name,
                        iso_3166: country.iso_3166_1,
                    },
                };

                entitiesService.findOrCreate(dbConnection, params, (err, entity) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PARSING_COUNTRY',
                            err
                        });
                    }

                    const insert = [
                        movieId,
                        entity.id
                    ];

                    return cb(null, insert);
                });
            }, (err, parsedCountry) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!parsedCountry || !parsedCountry.length) {
                    return cb(null, dbConnection, movie);
                }

                const query = 'INSERT INTO movie_country (movie_id, country_id) VALUES ?';

                dbConnection.query(query, [parsedCountry], (err) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PROCESSING_COUNTRY',
                            err
                        }, dbConnection);
                    }

                    return cb(null, dbConnection, movie);
                });
            });
        },
        // Insert performer
        (dbConnection, movie, cb) => {
            if (!movie.credits || !movie.credits.cast || !movie.credits.cast.length) {
                return cb(null, dbConnection, movie);
            }

            async.mapSeries(movie.credits.cast, (performer, cb) => {
                createPerson(dbConnection, performer, (err, entity) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PARSING_PERFORMERS',
                            err
                        });
                    }

                    const insert = [movieId, entity.id, performer.character, performer.order];

                    return cb(null, insert);
                });
            }, (err, parsedPerformers) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!parsedPerformers || !parsedPerformers.length) {
                    return cb(null, dbConnection, movie);
                }

                const query = 'INSERT INTO movie_performer (movie_id, person_id, `character`, `order`) VALUES ?';

                dbConnection.query(query, [parsedPerformers], (err) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PROCESSING_PERFORMERS',
                            err
                        }, dbConnection);
                    }

                    return cb(null, dbConnection, movie);
                });
            });
        },
        // Insert crew
        (dbConnection, movie, cb) => {
            if (!movie.credits || !movie.credits.crew || !movie.credits.crew.length) {
                return cb(null, dbConnection, movie);
            }

            async.mapSeries(movie.credits.crew, (crew, cb) => {
                createPerson(dbConnection, crew, (err, entity) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PARSING_CREW'
                        });
                    }

                    const insert = [
                        movieId,
                        entity.id,
                        crew.job,
                        crew.department
                    ];

                    return cb(null, insert);
                });
            }, (err, parsedCrew) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!parsedCrew || !parsedCrew.length) {
                    return cb(null, dbConnection, movie);
                }

                const query = 'INSERT INTO movie_crew (movie_id, person_id, job, department) VALUES ?';

                dbConnection.query(query, [parsedCrew], (err) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PROCESSING_CREW',
                            err
                        }, dbConnection);
                    }

                    return cb(null, dbConnection, movie);
                });
            });
        },
        // Insert keywords
        (dbConnection, movie, cb) => {
            if (!movie.keywords || !movie.keywords.keywords || !movie.keywords.keywords.length) {
                return cb(null, dbConnection, movie);
            }

            // Eliminar repetidos (se dieron casos que estaban repetidos en el arreglo)
            movie.keywords.keywords = utils.removeDuplicates(movie.keywords.keywords, ['id']);

            async.mapSeries(movie.keywords.keywords, (keyword, cb) => {
                const params = {
                    table: 'keyword',
                    columns: ['id', 'name', 'external_id'],
                    where: {
                        external_id: keyword.id
                    },
                    data: {
                        name: keyword.name,
                        external_id: keyword.id,
                    },
                };

                entitiesService.findOrCreate(dbConnection, params, (err, entity) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PARSING_KEYWORD',
                            err
                        });
                    }

                    const insert = [
                        movieId,
                        entity.id
                    ];

                    return cb(null, insert);
                });
            }, (err, parsedKeyword) => {
                if (err) {
                    return cb(err, dbConnection);
                }

                if (!parsedKeyword || !parsedKeyword.length) {
                    return cb(null, dbConnection, movie);
                }

                const query = 'INSERT INTO movie_keyword (movie_id, keyword_id) VALUES ?';

                dbConnection.query(query, [parsedKeyword], (err) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_PROCESSING_KEYWORD',
                            err
                        }, dbConnection);
                    }

                    return cb(null, dbConnection, movie);
                });
            });
        },
        // Insert languages and translations
        (dbConnection, movie, cb) => {
            if (!movie.translations || !movie.translations.translations || !movie.translations.translations.length) {
                return cb(null, dbConnection, movie);
            }

            // Eliminar repetidos (se dieron casos que estaban repetidos en el arreglo)
            movie.translations.translations = utils.removeDuplicates(movie.translations.translations, ['iso_639_1', 'iso_3166_1']);


            const parsedLanguages = [];

            if (movie.translations && movie.translations.translations) {
                for (let translation of movie.translations.translations) {
                    const lang = languagesService.getLanguage(translation.iso_639_1, translation.iso_3166_1);
                    if (lang) {
                        parsedLanguages.push([
                            movieId,
                            lang.language_id,
                            lang.language_locale_id,
                            translation.data.title && translation.data.title.length ? translation.data.title : null,
                            translation.data.overview && translation.data.overview.length ? translation.data.overview : null,
                            translation.data.homepage && translation.data.homepage.length ? translation.data.homepage : null
                        ]);
                    }
                }
            }

            if (!parsedLanguages || !parsedLanguages.length) {
                return cb(null, dbConnection, movie);
            }

            const query = 'INSERT INTO movie_language (movie_id, language_id, language_locale_id, title, overview, homepage) VALUES ?';

            dbConnection.query(query, [parsedLanguages], (err) => {
                if (err) {
                    return cb({
                        message: 'ERROR_PROCESSING_LANGUAGES',
                        err
                    }, dbConnection);
                }

                return cb(null, dbConnection, movie);
            });
        },
        // Insert videos
        (dbConnection, movie, cb) => {
            if (!movie.videos || !movie.videos.results || !movie.videos.results.length) {
                return cb(null, dbConnection, movie);
            }

            // Filtrar duplicados
            movie.videos.results = utils.removeDuplicates(movie.videos.results, ['id']);

            const parsedVideos = [];

            for (let video of movie.videos.results) {
                const lang = languagesService.getLanguage(video.iso_639_1, video.iso_3166_1);

                if (lang) {
                    parsedVideos.push([
                        movieId,
                        video.id,
                        lang.language_id,
                        lang.language_locale_id,
                        video.name,
                        video.site,
                        video.key,
                        video.size,
                        video.type
                    ]);
                }
            }

            if (!parsedVideos || !parsedVideos.length) {
                return cb(null, dbConnection, movie);
            }

            const query = `
                    INSERT INTO 
                        video
                        (movie_id, external_id, language_id, language_locale_id, name, site, \`key\`, size, type)
                    VALUES ?`;

            dbConnection.query(query, [parsedVideos], (err) => {
                if (err) {
                    return cb({
                        message: 'ERROR_PROCESSING_VIDEOS',
                        err
                    }, dbConnection);
                }

                return cb(null, dbConnection, movie);
            });
        },
        // Insert movie review
        (dbConnection, movie, cb) => {
            insertMovieReview(movieId, movie.title, dbConnection, (err) => {
                if (err) {
                    return cb({
                        message: err.toString(),
                        err
                    }, dbConnection);
                }

                return cb(null, dbConnection, movie);
            });
        },
        // Insert movie provider links
        (dbConnection, movie, cb) => {
            // if movie is not yet released, skip this step
            if (moment(movie.release_date).isAfter(moment())) {
                return cb(null, dbConnection, movie, movieId);
            }

            insertMovieProvider(movieId, movie.title, dbConnection, (err) => {
                if (err) {
                    return cb({
                        message: err.toString(),
                        err
                    }, dbConnection);
                }

                return cb(null, dbConnection, movie);
            });
        },
    ], (err, dbConnection) => {
        if (err) {
            let parsedErr = err.continue ? null : err;
            if (!dbConnection) {
                return callback(parsedErr);
            }

            dbConnection.rollback(() => {
                dbConnection.release();
                return callback(parsedErr);
            });
        } else {
            dbConnection.commit((err) => {
                dbConnection.release();
                return callback(err);
            });
        }
    });
};

const updateMovieLanguages = (parsedLanguages, movieId, dbConnection, callback) => {
    // Variable que se utiliza para saber si es necesario marcar la pelicula como actualizada (ya que cambio alguna traduccion)
    let shouldUpdate = false;

    async.waterfall([
        // Get actual data
        (cb) => {
            const query = `
                SELECT
                    id,
                    movie_id,
                    language_id,
                    language_locale_id,
                    title,
                    overview,
                    homepage
                FROM
                    movie_language
                WHERE
                    movie_id = ?
                ORDER BY
                    id ASC
            `;

            dbConnection.query(query, [movieId], (err, results) => {
                if (err) {
                    return cb({
                        message: 'ERROR_UPDATING_MOVIE'
                    });
                }

                return cb(null, results);
            });
        },
        // Compare current languagues (db) with parsed languages (api)
        (currentMovieLanguage, cb) => {
            const movieLanguageUpdate = [];

            // Recorre los lenguajes actuales para ver si hay que actualizar alguno, insertar nuevos o eliminar viejos
            for (const currentLanguage of currentMovieLanguage) {
                // Busca si el lenguage actual esta en las que trajo la api
                const parsedLanguage = parsedLanguages.find(obj => {
                    return obj.language_id == currentLanguage.language_id && obj.language_locale_id == currentLanguage.language_locale_id;
                });

                if (parsedLanguage) {
                    // Marca el lenguaje parseado como que ya se filtro (despues de recorrer todos los lenguajes actuales, los lenguajes parseados que no tengan filtered en true son los que hay que insertar)
                    parsedLanguage.filtered = true;

                    // Si el titulo o overview del lenguaje que trajo la api es distinto al del lenuaje actual, hay que actualizar la actual con los datos de la nueva
                    if (parsedLanguage.title != currentLanguage.title || parsedLanguage.overview != currentLanguage.overview) {
                        movieLanguageUpdate.push({
                            id: currentLanguage.id,
                            ...parsedLanguage
                        });
                    }
                }
            }

            // Los lenguajes parseados que tienen filtered en false son los lenguajes que trajo la api pero no estan en db
            const movieLanguageInsert = parsedLanguages.filter(obj => obj.filtered === false);

            if (movieLanguageInsert.length || movieLanguageUpdate.length) {
                shouldUpdate = true;
            }

            return cb(null, movieLanguageInsert, movieLanguageUpdate);
        },
        // Insert new languages
        (movieLanguageInsert, movieLanguageUpdate, cb) => {
            if (!movieLanguageInsert.length) {
                return cb(null, movieLanguageUpdate);
            }

            const query = 'INSERT INTO movie_language (movie_id, language_id, language_locale_id, title, overview, homepage) VALUES ?';
            const data = movieLanguageInsert.map(lang => {
                return [
                    movieId,
                    lang.language_id,
                    lang.language_locale_id,
                    lang.title,
                    lang.overview,
                    lang.homepage
                ];
            });

            dbConnection.query(query, [data], (err) => {
                if (err) {
                    return cb({
                        message: 'ERROR_INSERT_LANGUAGES',
                    });
                }

                return cb(null, movieLanguageUpdate);
            });
        },
        // Update languages
        (movieLanguageUpdate, cb) => {
            if (!movieLanguageUpdate.length) {
                return cb(null, movieLanguageUpdate);
            }

            const query = `
                INSERT INTO
                    movie_language (id, movie_id, language_id, language_locale_id, title, overview, homepage)
                VALUES
                    ?
                ON DUPLICATE KEY UPDATE
                    id = VALUES(id),
                    movie_id = VALUES(movie_id),
                    language_id = VALUES(language_id),
                    language_locale_id = VALUES(language_locale_id),
                    title = VALUES(title),
                    overview = VALUES(overview),
                    homepage = VALUES(homepage)
            `;
            const data = movieLanguageUpdate.map(lang => {
                return [
                    lang.id,
                    movieId,
                    lang.language_id,
                    lang.language_locale_id,
                    lang.title,
                    lang.overview,
                    lang.homepage
                ];
            });

            dbConnection.query(query, [data], (err) => {
                if (err) {
                    return cb({
                        message: 'ERROR_UPDATING_LANGUAGES',
                    });
                }

                return cb(null, movieLanguageUpdate);
            });
        },
        // Delete existing spins
        (movieLanguageUpdate, cb) => {
            if (!movieLanguageUpdate.length) {
                return cb(null);
            }

            const query = `
                DELETE FROM
                    movie_language_website
                WHERE
                    movie_language_id IN (?)        
            `;
            const movieLanguageIds = movieLanguageUpdate.map((l) => l.id);

            dbConnection.query(query, [movieLanguageIds], (err) => {
                if (err) {
                    return cb({
                        message: 'ERROR_DELETING_SPINS',
                    });
                }

                return cb(null);
            });
        },
    ], (err) => {
        if (err) {
            return callback(err);
        }

        return callback(null, shouldUpdate);
    });
};

const updateMovieHashes = (movieNames, movieId, dbConnection, callback) => {
    async.waterfall([
        // Get movie languages
        (cb) => {
            const query = `
                SELECT
                    language_id,
                    language_locale_id,
                    title
                FROM
                    movie_language
                WHERE
                    movie_id = ?
            `;

            dbConnection.query(query, [movieId], (err, results) => {
                if (err) {
                    return cb({
                        message: 'ERROR_GETTING_TITLES'
                    });
                }

                return cb(null, results);
            });
        },
        (movieLanguages, cb) => {
            const hashesToCheck = [];
            const hashesToInsert = [];

            for (const movieName of movieNames) {
                const movieLanguage = movieLanguages.find(obj => {
                    return obj.language_id == movieName.language_id && obj.language_locale_id == movieName.language_locale_id;
                });

                // Si no existe no se puede comparar el titulo actual con el nuevo, asique guarda el hash para insertar
                if (!movieLanguage) {
                    hashesToInsert.push({
                        title: movieName.title,
                        language_id: movieName.language_id,
                        language_locale_id: movieName.language_locale_id,
                    });
                    // Si existe y el titulo es distinto lo guarda para comparar el hash
                } else if (movieName.original_title != movieLanguage.title) {
                    hashesToCheck.push({
                        title: movieName.title,
                        language_id: movieName.language_id,
                        language_locale_id: movieName.language_locale_id,
                    });
                }
            }

            return cb(null, hashesToCheck, hashesToInsert);
        },
        // Check if movie hash already exists
        (hashesToCheck, hashesToInsert, cb) => {
            const hashesToDisable = [];

            if (!hashesToCheck || !hashesToCheck.length) {
                return cb(null, hashesToDisable, hashesToInsert);
            }

            async.mapSeries(hashesToCheck, (movieHash, cb) => {
                const query = `
                    SELECT
                        hash
                    FROM
                        movie_hash
                    WHERE
                        movie_id = ? AND
                        language_id = ? AND
                        language_locale_id = ? AND
                        active = 1 AND
                        website_id IS NULL
                `;

                dbConnection.query(query, [movieId, movieHash.language_id, movieHash.language_locale_id], (err, results) => {
                    if (err) {
                        return cb({
                            message: 'ERROR_GETTING_HASH'
                        });
                    }

                    let regex = new RegExp(`^${movieHash.title}(-[0-9]+)?$`);
                    if (results && results.length && !regex.test(results[0].hash)) {
                        hashesToDisable.push(results[0].hash);
                        hashesToInsert.push(movieHash);
                    }

                    return cb(null);
                });
            }, (err) => {
                if (err) {
                    return cb(err);
                }

                return cb(null, hashesToDisable, hashesToInsert);
            });
        },
        // Disable old hashes
        (hashesToDisable, hashesToInsert, cb) => {
            if (!hashesToDisable || !hashesToDisable.length) {
                return cb(null, hashesToInsert);
            }

            const query = `
                UPDATE
                    movie_hash
                SET
                    active = 0
                WHERE
                    hash IN (?)
            `;

            dbConnection.query(query, [hashesToDisable], (err) => {
                if (err) {
                    return cb({
                        message: 'ERROR_DISABLING_HASH'
                    });
                }

                return cb(null, hashesToInsert);
            });
        },
        // Generate new movie hashes
        (newMovieHashes, cb) => {
            if (!newMovieHashes || !newMovieHashes.length) {
                return cb(null, null);
            }

            async.mapSeries(newMovieHashes, (movieName, cb) => {
                const params = {
                    table: 'movie_hash',
                    column: 'hash',
                    hash: movieName.title
                };

                entitiesService.findHash(dbConnection, params, (err, hash) => {
                    if (err) {
                        return cb(err);
                    }

                    const result = [movieId, movieName.language_id, movieName.language_locale_id, hash];

                    return cb(null, result);
                });
            }, (err, hashes) => {
                if (err) {
                    return cb(err);
                }

                // Remuevo duplicados a partir del hash (4 posicion del arreglo de valores)
                hashes = utils.removeDuplicates(hashes, [3]);

                return cb(null, hashes);
            });
        },
        // Insert new hashes
        (hashes, cb) => {
            if (!hashes || !hashes.length) {
                return cb(null);
            }

            const query = 'INSERT INTO movie_hash (movie_id, language_id, language_locale_id, hash) VALUES ?';

            dbConnection.query(query, [hashes], (err) => {
                if (err) {
                    return cb({
                        message: 'ERROR_UPDATING_HASHES'
                    });
                }

                return cb(null);
            });
        }
    ], (err) => {
        return callback(err);
    });
};

const insertMovieReview = (movieId, movieTitle, dbConnection, callback) => {
    async.waterfall([
        (cb) => {
            youtubeService.findReview(movieTitle, (err, review) => {
                return cb(err, review);
            });
        },
        (review, cb) => {
            if (!review || !review.key) {
                return cb(null, review);
            }

            const query = `
                DELETE FROM
                    movie_review
                WHERE
                    movie_id = ?
            `;

            dbConnection.query(query, [movieId], (err) => {
                return cb(err, review);
            });
        },
        (review, cb) => {
            if (!review || !review.key) {
                return cb(null, review);
            }

            const query = `
                INSERT INTO 
                    movie_review
                SET
                    ?
            `;
            const data = {
                movie_id: movieId,
                key: review.key,
                created_at: new Date(),
            };

            dbConnection.query(query, [data], (err) => {
                return cb(err);
            });
        },
    ], (err) => {
        return callback(err);
    });
};

const insertMovieProvider = (movieId, movieTitle, dbConnection, callback) => {
    async.waterfall([
        (cb) => {
            itunesService.findMovie(movieTitle, (err, itunesMovie) => {
                if (err) {
                    // No cortar con error cuando falle itunes, simplemente no hacer nada
                    return cb(null, null);
                }

                return cb(null, itunesMovie);
            });
        },
        (itunesMovie, cb) => {
            if (!itunesMovie || !itunesMovie.trackViewUrl) {
                return cb(null, itunesMovie);
            }

            const query = `
                DELETE FROM
                    movie_provider
                WHERE
                    movie_id = ?
            `;

            dbConnection.query(query, [movieId], (err) => {
                return cb(err, itunesMovie);
            });
        },
        (itunesMovie, cb) => {
            if (!itunesMovie || !itunesMovie.trackViewUrl) {
                return cb(null, itunesMovie);
            }

            const query = `
                INSERT INTO 
                    movie_provider
                SET
                    ?
            `;
            const data = {
                movie_id: movieId,
                provider_id: constants.MOVIE_PROVIDER.ITUNES,
                link: itunesMovie.trackViewUrl,
                created_at: new Date(),
            };

            dbConnection.query(query, [data], (err) => {
                return cb(err);
            });
        },
    ], (err) => {
        return callback(err);
    });
};

const createPerson = (dbConnection, performer, callback) => {
    let initialHash = utils.normalizeString(performer.name);
    if (!initialHash.length) {
        initialHash = 'unnamed';
    }
    const params = {
        table: 'person',
        column: 'hash',
        hash: initialHash,
    };

    entitiesService.findHash(dbConnection, params, (err, hash) => {
        if (err) {
            return callback(err);
        }

        const params = {
            table: 'person',
            columns: ['id', 'name', 'hash', 'profile_path', 'gender_id', 'external_id'],
            where: {
                external_id: performer.id
            },
            data: {
                name: performer.name,
                gender_id: performer.gender,
                profile_path: performer.profile_path,
                external_id: performer.id,
                hash: hash,
            },
        };

        entitiesService.findOrCreate(dbConnection, params, (err, entity) => {
            return callback(err, entity);
        });
    });


};