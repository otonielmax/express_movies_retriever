'use strict';
const logger = require('@smartadtags/logger');
const async = require('async');
const config = require('config');
const minimist = require('minimist');

const apiService = require('./services/api');
const dbService = require('../../services/db');
const languagesService = require('./services/languages');
const moviesService = require('./services/movie');

logger.config({
    environment: config.LOGGER_ENV,
    service: 'movies-retriever',
    logLevel: config.LOGGER_LOG_LEVEL,
    format: config.LOGGER_FORMAT
});
const transactionId = 'cronjob-tmdb-retriever';

const bulkProcess = (minYear, maxYear, callback) => {
    const processYear = (year, callback) => {
        logger.info(`Processing year "${year}"`, { transactionId });

        let totalPages = null;
        const startPage = 1;
        const endPage = 100;

        const processPage = (page) => {
            logger.info(`Processing year "${year}" - Page "${page}"`, { transactionId });
            
            const path = `/discover/movie?year=${year}&page=${page}&sort_by=popularity.desc`;
            let pageTs = +new Date();
            apiService.request(path, {
                method: 'GET',
                json: true,
            }, (err, result, body) => {
                if (err) {
                    logger.error(`Error obtaning page "${page}"`, { transactionId, err });
                    return setTimeout(processPage, 500, page);
                }

                totalPages = body.total_pages;
                const movies = body.results;

                // Corta si no hay mas peliculas
                if (!movies || !movies.length) {
                    return callback();
                }

                // Si la primera pelicula tiene baja popularidad, corta
                if (movies[0].popularity < 3) {
                    return callback();
                }

                logger.info(`Updating page "${page}" movies on DB`, { transactionId });
                async.mapLimit(movies, 1, moviesService.createOrUpdate, (err) => {
                    if (err) {
                        logger.error('Error updating movies', { transactionId });
                        return callback(err);
                    }

                    let pageSpent = +new Date() - pageTs;

                    logger.info(`Processed page ${page}/${totalPages} - ${pageSpent} ms.`, { transactionId });

                    // Si ya recorrio todas las paginas, corta
                    if (page >= totalPages || page >= endPage) {
                        return callback(null);
                    }

                    return processPage(page + 1);
                });
            });
        };

        processPage(startPage);
    };

    const years = [];
    for (let year = maxYear; year >= minYear; year--) {
        years.push(year);
    }

    logger.info('Searching movies on TMDB', { transactionId, years });

    async.mapSeries(years, processYear, (err) => {
        if (callback) {
            return callback(err);
        }
    });
};

async.waterfall([
    (cb) => {
        logger.info('Retrieving languages', { transactionId });
        languagesService.retrieve((err) => {
            return cb(err);
        });
    },
    (cb) => {
        const args = minimist(process.argv.slice(2));

        logger.info('Process args', { transactionId, args });

        const start = Number(args.start);
        const end = Number(args.end);
        logger.info(`Starting bulk process: ${start} - ${end}`, { transactionId });
        bulkProcess(start, end, (err) => {
            return cb(err);
        });
    }
], (err) => {
    if (err) {
        logger.error('Error executing cronjob', { transactionId, err });
    } else {
        logger.info('Job executed successfully', { transactionId });
    }

    dbService.endPool();
});