import * as moment from 'moment';

import { logger } from '../../../logger';
import { IExternalChanges } from '../interfaces/ExternalChanges';

export default (transactionId: string, changes: IExternalChanges, lastDbUpdate: Date, startDate: string): boolean => {
    if (!lastDbUpdate) {
        logger.debug('Item needs to be updated because it does not have update date', { transactionId, lastDbUpdate });
        return true;
    }

    const lastUpdateMoment = moment(lastDbUpdate);
    if (lastUpdateMoment.isBefore(startDate)) {
        logger.debug('Item needs to be updated because last update was more than 2 weeks ago', { transactionId, lastDbUpdate, startDate });
        return true;
    }

    // Grab all dates from changes
    const allChangeDates: Date[] = [];
    if (changes && changes.changes) {
        for (const change of changes.changes) {
            if (change.items && change.items.length) {
                for (const item of change.items) {
                    allChangeDates.push(new Date(item.time));
                }
            }
        }
    }

    if (!allChangeDates.length) {
        logger.debug('There are no recent changes for this item in TMDB', { transactionId, changes });
        return false;
    }

    // Get latest date and check if the items needs to be updated
    const lastApiUpdateMoment = moment(getLatestDate(allChangeDates));
    if (lastUpdateMoment.isBefore(lastApiUpdateMoment)) {
        logger.debug('Item needs to be updated. It has been updated after last DB update.', {
            transactionId,
            lastUpdateMoment: lastUpdateMoment.format(),
            lastApiUpdateMoment: lastApiUpdateMoment.format()
        });
        return true;
    }

    logger.debug('We already have all API changes for this item', {
        transactionId,
        lastUpdateMoment: lastUpdateMoment.format(),
        lastApiUpdateMoment: lastApiUpdateMoment.format()
    });

    return false;
};

const getLatestDate = (dates): Date => {
    return new Date(Math.max.apply(null, dates));
};