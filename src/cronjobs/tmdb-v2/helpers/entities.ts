import * as squel from 'squel';

const CACHE = {};

const findOrCreate = async (connection, params) => {
    const keys = [params.table.toUpperCase()];
    for (const column in params.where) {
        if (params.where[column]) {
            keys.push(params.where[column]);
        }
    }
    const cacheKey = keys.join('_');

    if (CACHE[cacheKey]) {
        return CACHE[cacheKey];
    }

    /**
     * Buscar la entidad en la base de datos
     */
    const selectQuery = squel.select({ autoQuoteFieldNames: true });
    // From
    selectQuery.from(params.table);
    // Columns
    for (const column of params.columns) {
        selectQuery.field(column);
    }
    // Where
    for (const column in params.where) {
        if (params.where[column]) {
            selectQuery.where(`${column} = ?`, params.where[column]);
        }
    }
    const preparedSelectQuery = selectQuery.toParam();
    const [foundRows] = await connection.query(preparedSelectQuery.text, preparedSelectQuery.values);

    if (foundRows && foundRows[0]) {
        return foundRows[0];
    }

    /**
     * La entidad no existe, hay que crearla
     */
    // La crea
    const insertQuery = squel.insert({ autoQuoteFieldNames: true });
    // Into
    insertQuery.into(params.table);
    for (const key in params.data) {
        if (params.data[key]) {
            insertQuery.set(key, params.data[key]);
        }
    }

    const preparedInsertQuery = insertQuery.toParam();
    const [result] = await connection.query(preparedInsertQuery.text, preparedInsertQuery.values);

    const entity = {
        id: result.insertId,
        ...params.data
    };

    return entity;
};

const findHash = async (connection, params) => {
    const find = async (tryNum) => {
        const hash = initialHash;

        let maxLength = 100;
        let tryStr = '';
        let finalHash = hash;

        if (tryNum) {
            tryStr = '-' + tryNum;
            maxLength -= tryStr.length;
        }

        if (finalHash.length > maxLength) {
            finalHash = finalHash.substr(0, maxLength);
        }
        if (tryStr.length) {
            finalHash += tryStr;
        }

        const query = squel.select();
        // From
        query.from(params.table);
        // Column
        query.field('COUNT(*)', 'count');
        // Where
        query.where(`${params.column} = ?`, finalHash);
        // Add dynamic filters
        if (params.where) {
            params.where.map((where) => {
                if (where.value !== 'undefined') {
                    query.where(`${where.column} ${where.operator} ?`, where.value);
                } else {
                    query.where(`${where.column} ${where.operator}`);
                }
            });
        }

        const preparedQuery = query.toParam();

        const [rows] = await connection.query(preparedQuery.text, preparedQuery.values);
        const count = rows[0].count;

        if (count && count > 0) {
            return find(tryNum + 1);
        }

        return finalHash;
    };

    const initialHash = params.hash;
    return find(0);
};

const removeAndInsert = async (connection, params) => {
    // Delete values
    const query = squel.delete().from(params.table);
    for (const column in params.where) {
        if (params.where[column] !== undefined) {
            const value = params.where[column];
            if (value !== null) {
                query.where(`${column} = ?`, params.where[column]);
            } else {
                query.where(`${column} IS NULL`);
            }
        }
    }
    const preparedQuery = query.toParam();

    await connection.query(preparedQuery.text, preparedQuery.values);
    // Insert new values
    const insertQuery = `INSERT INTO ${params.table} (${params.columns.join(', ')}) VALUES ? `;
    await connection.query(insertQuery, [params.data]);
};

export {
    findHash,
    findOrCreate,
    removeAndInsert
};
