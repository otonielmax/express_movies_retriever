import dbPool from '../../../database/pool';
import * as utils from '../../../helpers/utils';
import { logger } from '../../../logger';
import languageManager from '../../../managers/Language';
import * as entitiesHelper from '../helpers/entities';
import { IExternalGenreList } from '../interfaces/ExternalGenre';
import tmbdApi from '../services/TMDB';

interface IGenre {
    externalId: number;
    name: string;
    normalizedName: string;
    movies: boolean;
    series: boolean;
    translations: Array<{
        language_id: number;
        name: string;
        normalizedName: string;
    }>;
}

interface IGenreWithLanguages {
    id: number;
    name: string;
    normalizedName: string;
    translations: Array<{
        language_id: number;
        name: string;
        normalizedName: string;
    }>;
}

class GenreManager {
    public syncAll = async (transactionId: string) => {
        logger.info('Processing all genres', { transactionId });

        // Retrieve all genres in all languages
        const movieGenres = await this.getAllGenres(transactionId, 'movie');
        const serieGenres = await this.getAllGenres(transactionId, 'tv');

        // Parse movie translations
        const parsedGenres: IGenre[] = movieGenres.map((genre) => {
            return {
                externalId: genre.id,
                name: genre.name,
                normalizedName: genre.normalizedName,
                translations: genre.translations,
                movies: true,
                series: false
            };
        });

        // Map serie translations to movie translations
        for (const genre of serieGenres) {
            const found = parsedGenres.find((g) => g.externalId === genre.id);
            if (found) {
                found.series = true;
            } else {
                parsedGenres.push({
                    externalId: genre.id,
                    name: genre.name,
                    normalizedName: genre.normalizedName,
                    translations: genre.translations,
                    movies: false,
                    series: true
                });
            }
        }

        // Process
        for (const genre of parsedGenres) {
            await this.sync(transactionId, genre);
        }
    }

    public findGenreInfo = async (externalGenreId: number) => {
        const [ rows ] = await dbPool.query('SELECT * FROM genre WHERE external_id = ?', [externalGenreId]);
        return rows[0];
    }

    private getAllGenres = async (transactionId: string, type: string) => {
        const languages = await languageManager.getDefaultLanguages();
        const genres = (await this.getExternalGenres(transactionId, type, 'en')).genres as IGenreWithLanguages[];

        const parsedGenres: IGenreWithLanguages[] = genres.map((g) => {
            return {
                ...g,
                translations: []
            };
        });

        // Retrieve all translations per each genre
        for (const language of languages) {
            const langGenres = (await this.getExternalGenres(transactionId, type, language)).genres;
            const lang = await languageManager.get(language);
            if (lang) {
                for (const currGenre of langGenres) {
                    const found = parsedGenres.find((g) => currGenre.id === g.id);
                    if (found) {
                        found.translations.push({
                            language_id: lang.language_id,
                            name: currGenre.name,
                            normalizedName: utils.normalizeString(currGenre.name)
                        });
                    }
                }
            }
        }

        return parsedGenres;
    }

    private getExternalGenres = async (transactionId: string, type: string, language: string): Promise<IExternalGenreList> => {
        const { body } = await tmbdApi.request(transactionId, {
            path: `/genre/${type}/list`,
            qs: {
                language
            }
        });
        return body;
    }

    private sync = async (transactionId: string, genre: IGenre): Promise<void> => {
        const foundGenre = await this.findGenreInfo(genre.externalId);
        if (foundGenre) {
            return this.update(transactionId, foundGenre.id, genre);
        }

        return this.create(transactionId, genre);
    }

    private create = async (transactionId: string, genre: IGenre): Promise<void> => {
        logger.info(`New genre "${genre.externalId}" found`, { transactionId });

        // Insert genre
        const parsedName = utils.normalizeString(genre.name);
        const [insertResult] = await dbPool.query('INSERT INTO genre SET ?', [{
            external_id: genre.externalId,
            name: parsedName,
            movies: genre.movies,
            series: genre.series
        }]);
        const genreId = insertResult.insertId;

        return this.insertTranslations(genreId, genre);
    }

    private insertTranslations = async (genreId: number, genre: IGenre) => {
        const [englishTranslation] = genre.translations;

        for (const translation of genre.translations) {
            // Solo procesar si la traduccion es distinta a la ingles, o si es el idioma ingles
            if (translation.name !== englishTranslation.name || translation.language_id === englishTranslation.language_id) {
                const foundTranslation = await this.findGenreTranslation(genreId, translation.language_id);
                if (!foundTranslation) {
                    // Si no existe, se crea
                    // Find hash
                    const hash = await entitiesHelper.findHash(dbPool, {
                        table: 'genre_language',
                        column: 'hash',
                        hash: utils.normalizeString(translation.name),
                        where: [{
                            column: 'genre_id',
                            operator: '!=',
                            value: genreId
                        }]
                    });
            
                    // Insert genre translation
                    return dbPool.query('INSERT INTO genre_language SET ?', [{
                        genre_id: genreId,
                        hash,
                        name: translation.name,
                        language_id: translation.language_id
                    }]);
                } else {
                    // Si existe, vemos si hay que actualizarlo
                    if (foundTranslation.name !== translation.name) {
                        // Find hash
                        const hash = await entitiesHelper.findHash(dbPool, {
                            table: 'genre_language',
                            column: 'hash',
                            hash: utils.normalizeString(translation.name),
                            where: [{
                                column: 'genre_id',
                                operator: '!=',
                                value: genreId
                            }]
                        });
                        return dbPool.query('UPDATE genre_language SET ? WHERE id = ?', [{
                            genre_id: genreId,
                            hash,
                            name: translation.name,
                            language_id: translation.language_id
                        }, foundTranslation.id]);
                    }
                }
            }
        }
    }

    private update = async (transactionId: string, genreId: number, genre: IGenre): Promise<void> => {
        logger.debug(`Updating external genre "${genre.externalId}" info`, { transactionId });
        await dbPool.query('UPDATE genre SET ? WHERE id = ?', [{
            series: genre.series,
            movies: genre.movies
        }, genreId]);

        return this.insertTranslations(genreId, genre);
    }

    private findGenreTranslation = async (genreId: number, languageId: number) => {
        const query = 'SELECT * FROM genre_language WHERE genre_id = ? AND language_id = ?';
        const [ rows ] = await dbPool.query(query, [genreId, languageId]);
        return rows[0];
    }
}

export default new GenreManager();
