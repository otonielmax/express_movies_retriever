import { ImageMediaType } from '../../../enums/ImageMediaType';
import { normalizeString } from '../../../helpers/utils';
import { findHash } from '../helpers/entities';
import imageMediaManager from './ImageMedia';

export interface IFindOrCreatePersonArgs {
    external_id: number;
    name: string;
    gender_id: number|null;
    profile_path: string|null;
}

class PersonManager {
    public findOrCreate = async (connection, person: IFindOrCreatePersonArgs) => {
        const personFound = await this.find(connection, person);
        if (personFound) {
            return personFound;
        }

        return this.create(connection, person);
    }

    private create = async (connection, person: IFindOrCreatePersonArgs) => {
        const initialHash = normalizeString(person.name);
        const hash = await findHash(connection, {
            table: 'person',
            column: 'hash',
            hash: initialHash || 'unnamed',
        });

        const data: any = {
            name: person.name,
            hash,
            profile_path: person.profile_path,
            gender_id: person.gender_id,
            external_id: person.external_id
        };

        // Procesar imagenes
        if (person.profile_path) {
            const entity = await imageMediaManager.findOrCreate(connection, {
                path: person.profile_path,
                imageMediaTypeId: ImageMediaType.PROFILE
            });

            if (entity) {
                data.profile_image_media_id = entity.id;
            }
        }

        const [result] = await connection.query('INSERT INTO person SET ?', [data]);
        data.id = result.insertId;

        return data;
    }

    private find = async (connection, person: IFindOrCreatePersonArgs) => {
        const query = `
            SELECT
                id, name, hash, profile_path, gender_id, external_id
            FROM
                person
            WHERE
                external_id = ?
        `;
        const [rows] = await connection.query(query, [person.external_id]);
        return rows[0];
    }

}

export default new PersonManager();