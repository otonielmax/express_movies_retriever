import { ImageMediaType } from '../../../enums/ImageMediaType';
import * as entitiesHelper from '../helpers/entities';
import imageMediaManager from './ImageMedia';

export interface IFindOrCreateCompanyArgs {
    external_id: number;
    name: string;
    logo_path: string|null;
}

class CompanyManager {
    public findOrCreate = async (connection, company: IFindOrCreateCompanyArgs) => {
        const result = await entitiesHelper.findOrCreate(connection, {
            table: 'company',
            columns: ['id', 'name', 'logo_path', 'logo_image_media_id', 'external_id'],
            where: {
                external_id: company.external_id
            },
            data: {
                name: company.name,
                logo_path: company.logo_path,
                external_id: company.external_id
            },
        });

        // Solo insertar si la company tiene Logo y no esta en la tabla nueva
        if (company.logo_path && !result.logo_image_media_id) {
            const entity = await imageMediaManager.findOrCreate(connection, {
                path: company.logo_path,
                imageMediaTypeId: ImageMediaType.LOGO
            });

            if (entity) {
                const dataPath = {
                    logo_image_media_id: entity.id
                };
                await connection.query('UPDATE company SET ? WHERE external_id = ?', [dataPath, company.external_id]);
            }
        }

        return result;
    }

}

export default new CompanyManager();