import dbPool from '../../../database/pool';
import { ImageMediaType } from '../../../enums/ImageMediaType';
import * as utils from '../../../helpers/utils';
import { logger } from '../../../logger';
import * as entitiesHelper from '../helpers/entities';
import { IExternalEpisode } from '../interfaces/ExternalEpisode';
import { IExternalVideo } from '../interfaces/ExternalVideo';
import tmbdApi from '../services/TMDB';
import imageMediaManager from './ImageMedia';
import personManager from './Person';
import serieTranslationManager from './SerieTranslation';
import videoMediaManager from './VideoMedia';

export interface ISyncEpisodeArgs {
    serieId: number;
    seasonId: number;
    externalSerieId: number;
    seasonNumber: number;
    episodeNumber: number;
}

class EpisodeManager {
    public sync = async (transactionId: string, args: ISyncEpisodeArgs) => {
        const { serieId, seasonId, externalSerieId, seasonNumber, episodeNumber } = args;
        
        logger.info(`Processing external serie "${externalSerieId}" season "${seasonNumber}" episode "${episodeNumber}"`, {
            transactionId
        });

        // Retrieve from TMDB
        const externalSeason = await this.getExternalEpisode(transactionId, externalSerieId, seasonNumber, episodeNumber);

        let connection;
        try {
            connection = await dbPool.getConnection();
            await connection.beginTransaction();
            // Basic info
            const episodeId = await this.processEpisodeInfo(transactionId, connection, serieId, seasonId, externalSeason);
            // Videos
            await this.processVideos(transactionId, connection, episodeId, externalSeason);
            // Performers
            await this.processPerformers(transactionId, connection, episodeId, externalSeason);
            // Crew
            await this.processCrew(transactionId, connection, episodeId, externalSeason);
            // Translations
            const languageUpdated = await serieTranslationManager.sync(transactionId, connection, {
                episode_id: episodeId
            }, externalSeason.translations);
            if (languageUpdated) {
                await this.setLanguageUpdated(transactionId, connection, episodeId);
            }

            // Commit and close connection
            await connection.commit();
            connection.release();
            connection = null;
        } catch (err) {
            if (connection) {
                await connection.rollback();
                connection.release();
            }
            throw err;
        }
    }

    public findEpisodeInfo = async (connection, externalEpisodeId: number) => {
        const dbConnection = connection || dbPool;
        const [rows] = await dbConnection.query(`
            SELECT
                e.*, im.path AS image_media_still_path
            FROM
                episode e
            LEFT JOIN
                image_media im ON e.still_image_media_id = im.id
            WHERE
                e.external_id = ?`, [externalEpisodeId]
        );
        return rows[0];
    }

    private getExternalEpisode =
        async (transactionId: string, externalSerieId: number, seasonNumber: number, episodeNumber: number): Promise<IExternalEpisode> => {
            const { body } = await tmbdApi.request(transactionId, {
                path: `/tv/${externalSerieId}/season/${seasonNumber}/episode/${episodeNumber}`,
                qs: {
                    append_to_response:
                        'credits,translations,videos'
                }
            });
            return body;
    }

    private processEpisodeInfo = async (
        transactionId: string, connection, serieId: number, seasonId: number, externalEpisode: IExternalEpisode): Promise<number> => {
            logger.debug(`Processing external episode "${externalEpisode.id}" info`, { transactionId });

            const existingEpisode = await this.findEpisodeInfo(connection, externalEpisode.id);

            const data: any = {
                external_id: externalEpisode.id,
                serie_id: serieId,
                season_id: seasonId,
                episode_number: externalEpisode.episode_number,
                title: externalEpisode.name || null,
                overview: externalEpisode.overview || null,
                still_path: externalEpisode.still_path || null,
                air_date: externalEpisode.air_date || null,
                vote_count: externalEpisode.vote_count,
                vote_average: externalEpisode.vote_average,
                updated_at: new Date()
            };

            // Procesar imagenes solo si cambio
            if (externalEpisode.still_path && (!existingEpisode || externalEpisode.still_path !== existingEpisode.image_media_still_path)) {
                const entity = await imageMediaManager.findOrCreate(connection, {
                    path: externalEpisode.still_path,
                    imageMediaTypeId: ImageMediaType.STILL
                });

                if (entity) {
                    data.still_image_media_id = entity.id;
                }
            }

            let episodeId: number;
            if (existingEpisode && existingEpisode.id) {
                await connection.query('UPDATE episode SET ? WHERE id = ?', [data, existingEpisode.id]);
                episodeId = existingEpisode.id;
            } else {
                const [ result ] = await connection.query('INSERT INTO episode SET ?', [data]);
                episodeId = result.insertId;
            }

            return episodeId;
    }

    private processVideos = async (
        transactionId: string, connection, episodeId: number, externalEpisode: IExternalEpisode) => {
            if (!externalEpisode.videos || !externalEpisode.videos.results || !externalEpisode.videos.results.length) {
                return;
            }

            logger.debug(`Processing external episode "${externalEpisode.id}" videos`, { transactionId });

            const videos: IExternalVideo[] = utils.removeDuplicates(externalEpisode.videos.results, ['id']);
            const insertVideos: any = [];
            for (const video of videos) {
                const entity = await videoMediaManager.findOrCreate(connection, video);
                if (entity) {
                    insertVideos.push([
                        episodeId,
                        entity.id
                    ]);
                }
            }

            if (!insertVideos.length) {
                return;
            }

            await entitiesHelper.removeAndInsert(connection, {
                table: 'serie_video',
                columns: ['episode_id', 'video_media_id'],
                where: {
                    episode_id: episodeId
                },
                data: insertVideos
            });
    }

    private processPerformers = async (
        transactionId: string, connection, episodeId: number, externalEpisode: IExternalEpisode) => {
            let cast: any = [];
            if (externalEpisode.credits && externalEpisode.credits.cast && externalEpisode.credits.cast.length) {
                cast = cast.concat(externalEpisode.credits.cast.map((item) => {
                    return {
                        ...item,
                        guest: 0
                    };
                }));
            }
            if (externalEpisode.credits && externalEpisode.credits.guest_stars && externalEpisode.credits.guest_stars.length) {
                cast = cast.concat(externalEpisode.credits.guest_stars.map((item) => {
                    return {
                        ...item,
                        guest: 1
                    };
                }));
            }

            if (!cast.length) {
                return;
            }

            logger.debug(`Processing external episode "${externalEpisode.id}" performers`, { transactionId });

            const insertPerformers: any = [];
            for (const performer of cast) {
                const person = await personManager.findOrCreate(connection, {
                    name: performer.name,
                    gender_id: performer.gender,
                    profile_path: performer.profile_path,
                    external_id: performer.id
                });
                insertPerformers.push([
                    episodeId,
                    person.id,
                    performer.character,
                    performer.order,
                    performer.guest
                ]);
            }

            await entitiesHelper.removeAndInsert(connection, {
                table: 'serie_performer',
                columns: ['episode_id', 'person_id', '`character`', '`order`', 'guest'],
                where: {
                    episode_id: episodeId
                },
                data: insertPerformers
            });
    }

    private processCrew = async (
        transactionId: string, connection, episodeId: number, externalEpisode: IExternalEpisode) => {
            if (!externalEpisode.credits || !externalEpisode.credits.crew || !externalEpisode.credits.crew.length) {
                return;
            }

            logger.debug(`Processing external episode "${externalEpisode.id}" crew`, { transactionId });

            const insertPerformers: any = [];
            for (const crew of externalEpisode.credits.crew) {
                const person = await personManager.findOrCreate(connection, {
                    name: crew.name,
                    gender_id: crew.gender,
                    profile_path: crew.profile_path,
                    external_id: crew.id
                });
                insertPerformers.push([
                    episodeId,
                    person.id,
                    crew.job,
                    crew.department
                ]);
            }

            await entitiesHelper.removeAndInsert(connection, {
                table: 'serie_crew',
                columns: ['episode_id', 'person_id', 'job', 'department'],
                where: {
                    episode_id: episodeId
                },
                data: insertPerformers
            });
    }

    private setLanguageUpdated = async (transactionId: string, connection, episodeId: number) => {
        logger.debug(`Setting episode "${episodeId}" last language update date`, { transactionId });

        const data = {
            language_updated_at: new Date()
        };
        await connection.query('UPDATE episode SET ? WHERE id = ?', [data, episodeId]);
    }
}

export default new EpisodeManager();
