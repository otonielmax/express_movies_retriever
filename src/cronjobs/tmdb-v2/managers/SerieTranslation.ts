import * as squel from 'squel';

import { removeDuplicates } from '../../../helpers/utils';
import { logger } from '../../../logger';
import languageManager from '../../../managers/Language';
import { IExternalSerieTranslation } from '../interfaces/ExternalTranslation';

export interface ISyncSerieTranslationSettings {
    serie_id?: number;
    episode_id?: number|null;
}

interface IParsedTranslations {
    serie_id: number|null;
    episode_id: number|null;
    language_id: number;
    language_locale_id: number|null;
    title: string|null;
    overview: string|null;
    homepage: string|null;
}

class SerieTranslationManager {
    public sync = async (
        transactionId: string, connection,
        settings: ISyncSerieTranslationSettings, translations: IExternalSerieTranslation): Promise<boolean> => {
            if (!translations || !translations.translations || !translations.translations.length) {
                return false;
            }

            logger.debug(`Processing external Serie "${settings.serie_id}" Episode "${settings.episode_id}" translations`, {
                transactionId
            });

            const defaultLanguages = languageManager.getDefaultLanguages();
            const uniqueTranslations = removeDuplicates(translations.translations, ['iso_639_1', 'iso_3166_1']);
            let parsedTranslations: IParsedTranslations[] = [];

            for (const translation of uniqueTranslations) {
                // Filter empty translations
                if (translation.data.name || translation.data.overview) {
                    const shouldInsert = defaultLanguages.includes(translation.iso_639_1);
                    const lang = await languageManager.get(translation.iso_639_1, translation.iso_3166_1);
                    if (shouldInsert && lang) {
                        parsedTranslations.push({
                            serie_id: settings.serie_id || null,
                            episode_id: settings.episode_id || null,
                            language_id: lang.language_id,
                            language_locale_id: lang.language_locale_id,
                            title: translation.data.name || null,
                            overview: translation.data.overview || null,
                            homepage: translation.data.homepage || null
                        });
                    }
                }
            }

            if (!parsedTranslations.length) {
                return false;
            }

            // Eliminar repetidos otra vez, ya que en idiomas chinos no detecta el locale y queda duplicado
            parsedTranslations = removeDuplicates(parsedTranslations, ['language_id', 'language_locale_id']);

            const currentTranslations = await this.getTranslationsFromDb(connection, settings);
            const translationsToUpdate: any = [];
            const translationsToInsert: any = [];

            for (const translation of parsedTranslations) {
                const foundLanguage = currentTranslations.find((obj) => {
                    return obj.language_id === translation.language_id && obj.language_locale_id === translation.language_locale_id;
                });

                if (!foundLanguage) {
                    translationsToInsert.push(translation);
                } else if (translation.title !== foundLanguage.title || translation.overview !== foundLanguage.overview) {
                    translationsToUpdate.push({
                        id: foundLanguage.id,
                        ...translation
                    });
                }
            }

            if (!translationsToInsert.length && !translationsToUpdate.length) {
                return false;
            }

            await this.insertTranslations(transactionId, connection, translationsToInsert);
            await this.updateTranslations(transactionId, connection, translationsToUpdate);

            return true;
    }

    private getTranslationsFromDb = async (connection, settings: ISyncSerieTranslationSettings) => {
        const query = squel.select().from('serie_language');
    
        for (const column in settings) {
            if (settings[column] !== undefined) {
                const value = settings[column];
                if (value !== null) {
                    query.where(`${column} = ?`, value);
                } else {
                    query.where(`${column} IS NULL`);
                }
            }
        }

        const preparedQuery = query.toParam();
        const [ rows ] = await connection.query(preparedQuery.text, preparedQuery.values);

        return rows;
    }

    private insertTranslations = async (transactionId, connection, translationsToInsert) => {
        if (!translationsToInsert.length) {
            return;
        }

        logger.debug(`Creating "${translationsToInsert.length}" translations`, { transactionId });
        
        const query = `
            INSERT INTO
                serie_language
                (serie_id, episode_id, language_id, language_locale_id, title, overview, homepage)
            VALUES ?`;
        const data = translationsToInsert.map((lang) => {
            return [
                lang.serie_id,
                lang.episode_id,
                lang.language_id,
                lang.language_locale_id,
                lang.title,
                lang.overview,
                lang.homepage
            ];
        });

        await connection.query(query, [data]);
    }

    private updateTranslations = async (transactionId, connection, translationsToUpdate) => {
        if (!translationsToUpdate.length) {
            return;
        }

        logger.debug(`Updating "${translationsToUpdate.length}" translations`, { transactionId });

        for (const translation of translationsToUpdate) {
            const dataUpdate = { ...translation };
            delete dataUpdate.id;
            await connection.query('UPDATE serie_language SET ? WHERE id = ?', [dataUpdate, translation.id]);
        }

        // Delete spins
        const translationIds = translationsToUpdate.map((t) => t.id);
        logger.debug('Deleting old spins because translations changed', { transactionId });

        await connection.query('DELETE FROM serie_language_website WHERE serie_language_id IN (?)', [translationIds]);
    }
}

export default new SerieTranslationManager();