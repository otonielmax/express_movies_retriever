import * as moment from 'moment';
import * as squel from 'squel';

import dbPool from '../../../database/pool';
import { ImageMediaType } from '../../../enums/ImageMediaType';
import * as utils from '../../../helpers/utils';
import { logger } from '../../../logger';
import languageManager from '../../../managers/Language';
import { MovieProvider } from '../enums/MovieProvider';
import * as entitiesHelper from '../helpers/entities';
import needsUpdate from '../helpers/needsUpdate';
import { IExternalMovie } from '../interfaces/ExternalMovie';
import { IExternalVideo } from '../interfaces/ExternalVideo';
import itunesService from '../services/ITunes';
import tmbdApi from '../services/TMDB';
import youtubeService from '../services/Youtube';
import companyManager from './Company';
import genreManager from './Genre';
import imageMediaManager from './ImageMedia';
import personManager from './Person';
import videoMediaManager from './VideoMedia';

export interface IExternalMovieBasic {
    externalId: number;
    title: string;
}

interface IParsedTranslations {
    movie_id: number;
    language_id: number;
    language_locale_id: number | null;
    title: string | null;
    overview: string | null;
    homepage: string | null;
}

export interface IMovieBasic {
    id: number;
    poster_path: string;
    backdrop_path: string;
}

class MovieManager {
    public sync = async (jobTransactionId: string, args: IExternalMovieBasic) => {
        const transactionId = `${jobTransactionId}:movie:${args.externalId}`;

        logger.info(`Processing external movie "${args.externalId}"`, { transactionId });

        const startDate = moment().subtract(13, 'days').format('YYYY-MM-DD');
        const endDate = moment().format('YYYY-MM-DD');
        const externalMovie = await this.getExternalMovie(transactionId, args.externalId, startDate, endDate);

        // Search movie and check if it must be updated
        const movieFound = await this.findMovieInfo(externalMovie.id);
        const shouldUpdate = needsUpdate(transactionId, externalMovie.changes, movieFound ? movieFound.updated_at : null, startDate);
        if (!shouldUpdate) {
            return;
        }

        let connection;
        try {
            connection = await dbPool.getConnection();
            await connection.beginTransaction();

            // Movie basic info
            const movieId = await this.processMovieInfo(transactionId, connection, externalMovie, movieFound);
            // Genres
            await this.processMovieGenres(transactionId, connection, movieId, externalMovie);
            // Companies
            await this.processCompanies(transactionId, connection, movieId, externalMovie);
            // Production countries
            await this.processCountries(transactionId, connection, movieId, externalMovie);
            // Performers
            await this.processPerformers(transactionId, connection, movieId, externalMovie);
            // Crew
            await this.processCrew(transactionId, connection, movieId, externalMovie);
            // Keywords
            await this.processKeywords(transactionId, connection, movieId, externalMovie);
            // Videos
            await this.processVideos(transactionId, connection, movieId, externalMovie);
            // Review
            await this.processReview(transactionId, connection, movieId, externalMovie);
            // Providers
            await this.processProviders(transactionId, connection, movieId, externalMovie);
            // Hashes
            await this.processHashes(transactionId, connection, movieId, externalMovie);
            // Translations
            const languageUpdated = await this.processTranslations(transactionId, connection, movieId, externalMovie);
            if (languageUpdated) {
                await this.setLanguageUpdated(transactionId, connection, movieId, externalMovie);
            }

            // Commit and close connection
            await connection.commit();
            connection.release();
            connection = null;
        } catch (err) {
            if (connection) {
                await connection.rollback();
                connection.release();
            }
            throw err;
        }
    }

    public setGalleryOrder = async (transactionId: string, gallery: string, externalIds: [IExternalMovieBasic]) => {
        let connection;
        try {
            connection = await dbPool.getConnection();
            await connection.beginTransaction();

            const columnName = `gallery_${gallery}_position`;

            // Clean current flags
            logger.info(`Cleaning all gallery "${gallery}" orders`, { transactionId });
            const updateAllQuery = squel.update().table('movie').set(columnName, null);
            const preparedUpdateAllQuery = updateAllQuery.toParam();
            await connection.query(preparedUpdateAllQuery.text, preparedUpdateAllQuery.values);

            // Set new flags
            logger.info(`Setting new gallery "${gallery}" orders`, { transactionId });
            for (let i = 0; i < externalIds.length; i++) {
                const position = i + 1;
                const updateOneQuery =
                    squel.update().table('movie').set(columnName, position).where('external_id = ?', externalIds[i].externalId);
                const preparedUpdateOneQuery = updateOneQuery.toParam();

                await connection.query(preparedUpdateOneQuery.text, preparedUpdateOneQuery.values);
            }

            await connection.commit();
        } catch (err) {
            if (connection) {
                await connection.rollback();
                connection.release();
            }
            throw err;
        }
    }

    private getExternalMovie = async (
        transactionId: string, externalId: number, startDate: string, endDate: string): Promise<IExternalMovie> => {
        const { body } = await tmbdApi.request(transactionId, {
            path: `/movie/${externalId}`,
            qs: {
                append_to_response:
                    'credits,keywords,videos,translations,external_ids,changes',
                start_date: startDate,
                end_date: endDate
            }
        });
        return body;
    }

    private processMovieStatus = async (transactionId, connection, externalMovie: IExternalMovie) => {
        const status = externalMovie.status.toUpperCase().replace(/ /g, '_');
        logger.debug(`Processing movie status "${status}"`, { transactionId });
        const entity = await entitiesHelper.findOrCreate(connection, {
            table: 'movie_status',
            columns: ['id', 'name'],
            where: {
                name: status,
            },
            data: {
                name: status,
            },
        });

        return entity.id;
    }

    private processMovieInfo = async (transactionId: string, connection, externalMovie: IExternalMovie, movie: IMovieBasic|null) => {
        const movieStatusId = await this.processMovieStatus(transactionId, connection, externalMovie);

        logger.debug(`Processing external movie "${externalMovie.id}" info`, { transactionId });

        const now = new Date();
        const data: any = {
            external_id: externalMovie.id,
            title: externalMovie.title,
            original_title: externalMovie.original_title || null,
            overview: externalMovie.overview || null,
            tagline: externalMovie.tagline || null,
            popularity: externalMovie.popularity,
            poster_path: externalMovie.poster_path || null,
            backdrop_path: externalMovie.backdrop_path || null,
            release_date: externalMovie.release_date || null,
            runtime: externalMovie.runtime || null,
            homepage: externalMovie.homepage || null,
            budget: externalMovie.budget || null,
            revenue: externalMovie.revenue || null,
            vote_average: externalMovie.vote_average || null,
            vote_count: externalMovie.vote_count || null,
            adult: externalMovie.adult || 0,
            imdb_id: externalMovie.imdb_id || null,
            movie_status_id: movieStatusId,
            created_at: now,
            updated_at: now
        };

        if (externalMovie.external_ids) {
            data.facebook_id = externalMovie.external_ids.facebook_id || null;
            data.twitter_id = externalMovie.external_ids.twitter_id || null;
            data.instagram_id = externalMovie.external_ids.instagram_id || null;
        }

        // Procesar imagenes solo si cambiaron
        if (externalMovie.poster_path && (!movie || externalMovie.poster_path !== movie.poster_path)) {
            const entity = await imageMediaManager.findOrCreate(connection, {
                path: externalMovie.poster_path,
                imageMediaTypeId: ImageMediaType.POSTER
            });

            if (entity) {
                data.poster_image_media_id = entity.id;
            }
        }
        if (externalMovie.backdrop_path && (!movie || externalMovie.backdrop_path !== movie.backdrop_path)) {
            const entity = await imageMediaManager.findOrCreate(connection, {
                path: externalMovie.backdrop_path,
                imageMediaTypeId: ImageMediaType.BACKDROP
            });

            if (entity) {
                data.backdrop_image_media_id = entity.id;
            }
        }

        if (movie && movie.id) {
            await connection.query('UPDATE movie SET ? WHERE id = ?', [data, movie.id]);
            return movie.id;
        }

        const [result] = await connection.query('INSERT INTO movie SET ?', [data]);
      
        return result.insertId;
    }

    private processMovieGenres = async (transactionId: string, connection, movieId: number, externalMovie: IExternalMovie) => {
        if (!externalMovie.genres || !externalMovie.genres.length) {
            return;
        }

        logger.debug(`Processing external movie "${externalMovie.id}" genres`, { transactionId });
        const genres = utils.removeDuplicates(externalMovie.genres, ['id']);
        const insertGenres: any = [];

        for (const genre of genres) {
            const foundGenre = await genreManager.findGenreInfo(genre.id);
            if (foundGenre) {
                insertGenres.push([movieId, foundGenre.id]);
            }
        }

        if (!insertGenres.length) {
            logger.info(`No genres found for external movie "${externalMovie.id}"`, {
                transactionId,
                externalGenres: externalMovie.genres
            });

            return;
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'movie_genre',
            columns: ['movie_id', 'genre_id'],
            where: {
                movie_id: movieId
            },
            data: insertGenres
        });
    }

    private processCompanies = async (transactionId: string, connection, movieId: number, externalMovie: IExternalMovie) => {
        if (!externalMovie.production_companies || !externalMovie.production_companies.length) {
            return;
        }

        logger.debug(`Processing external movie "${externalMovie.id}" companies`, { transactionId });

        const insertCompanies: any = [];
        for (const company of externalMovie.production_companies) {
            const insertedCompany = await companyManager.findOrCreate(connection, {
                name: company.name,
                logo_path: company.logo_path,
                external_id: company.id
            });

            insertCompanies.push([
                movieId,
                insertedCompany.id
            ]);
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'movie_company',
            columns: ['movie_id', 'company_id'],
            where: {
                movie_id: movieId
            },
            data: insertCompanies
        });
    }

    private processCountries = async (transactionId: string, connection, movieId: number, externalMovie: IExternalMovie) => {
        if (!externalMovie.production_countries || !externalMovie.production_countries.length) {
            return;
        }

        logger.debug(`Processing external movie "${externalMovie.id}" production countries`, { transactionId });

        const productionCountries = utils.removeDuplicates(externalMovie.production_countries, ['id']);
        const insertCountries: any = [];

        for (const country of productionCountries) {
            const entity = await entitiesHelper.findOrCreate(connection, {
                table: 'country',
                columns: ['id', 'name', 'iso_3166'],
                where: {
                    iso_3166: country.iso_3166_1
                },
                data: {
                    name: country.title,
                    iso_3166: country.iso_3166_1,
                },
            });

            insertCountries.push([
                movieId,
                entity.id
            ]);
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'movie_country',
            columns: ['movie_id', 'country_id'],
            where: {
                movie_id: movieId
            },
            data: insertCountries
        });
    }

    private processPerformers = async (transactionId: string, connection, movieId: number, externalMovie: IExternalMovie) => {
        if (!externalMovie.credits || !externalMovie.credits.cast || !externalMovie.credits.cast.length) {
            return;
        }

        logger.debug(`Processing external movie "${externalMovie.id}" performers`, { transactionId });

        const insertPerformers: any = [];
        for (const performer of externalMovie.credits.cast) {
            const person = await personManager.findOrCreate(connection, {
                name: performer.name,
                gender_id: performer.gender,
                profile_path: performer.profile_path,
                external_id: performer.id
            });
            insertPerformers.push([
                movieId,
                person.id,
                performer.character,
                performer.order
            ]);
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'movie_performer',
            columns: ['movie_id', 'person_id', '`character`', '`order`'],
            where: {
                movie_id: movieId
            },
            data: insertPerformers
        });
    }

    private processCrew = async (transactionId: string, connection, movieId: number, externalMovie: IExternalMovie) => {
        if (!externalMovie.credits || !externalMovie.credits.crew || !externalMovie.credits.crew.length) {
            return;
        }

        logger.debug(`Processing external movie "${externalMovie.id}" crew`, { transactionId });

        const insertCrew: any = [];
        for (const crew of externalMovie.credits.crew) {
            const person = await personManager.findOrCreate(connection, {
                name: crew.name,
                gender_id: crew.gender,
                profile_path: crew.profile_path,
                external_id: crew.id
            });
            insertCrew.push([
                movieId,
                person.id,
                crew.job,
                crew.department
            ]);
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'movie_crew',
            columns: ['movie_id', 'person_id', 'job', 'department'],
            where: {
                movie_id: movieId
            },
            data: insertCrew
        });
    }

    private processKeywords = async (transactionId: string, connection, movieId: number, externalMovie: IExternalMovie) => {
        if (!externalMovie.keywords || !externalMovie.keywords.results || !externalMovie.keywords.results.length) {
            return;
        }

        logger.debug(`Processing external movie "${externalMovie.id}" keywords`, { transactionId });

        const insertKeywords: any = [];
        for (const keyword of externalMovie.keywords.results) {
            const entity = await entitiesHelper.findOrCreate(connection, {
                table: 'keyword',
                columns: ['id', 'name', 'external_id'],
                where: {
                    external_id: keyword.id,
                },
                data: {
                    name: keyword.name,
                    external_id: keyword.id
                }
            });

            insertKeywords.push([
                movieId,
                entity.id
            ]);
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'movie_keyword',
            columns: ['movie_id', 'keyword_id'],
            where: {
                movie_id: movieId
            },
            data: insertKeywords
        });
    }

    private processVideos = async (transactionId: string, connection, movieId: number, externalMovie: IExternalMovie) => {
        if (!externalMovie.videos || !externalMovie.videos.results || !externalMovie.videos.results.length) {
            return;
        }

        logger.debug(`Processing external movie "${externalMovie.id}" videos`, { transactionId });

        const videos: IExternalVideo[] = utils.removeDuplicates(externalMovie.videos.results, ['id']);
        const insertVideos: any = [];
        for (const video of videos) {
            const entity = await videoMediaManager.findOrCreate(connection, video);
            if (entity) {
                insertVideos.push([
                    movieId,
                    entity.id
                ]);
            }
        }

        if (!insertVideos.length) {
            return;
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'movie_video',
            columns: ['movie_id', 'video_media_id'],
            where: {
                movie_id: movieId
            },
            data: insertVideos
        });
    }

    private processReview = async (transactionId: string, connection, movieId: number, externalMovie: IExternalMovie) => {
        const [rowsFound] = await connection.query('SELECT created_at FROM movie_review WHERE movie_id = ?', [movieId]);

        // Only process reviews after 2 months
        if (rowsFound && rowsFound[0]) {
            const reviewMoment = moment(rowsFound[0].created_at);
            const twoMonthsMoment = moment().subtract(2, 'months');
            if (!reviewMoment.isBefore(twoMonthsMoment)) {
                return;
            }
        }

        logger.debug(`Processing external movie "${externalMovie.id}" youtube review`, { transactionId });

        const videos = await youtubeService.search(transactionId, `${externalMovie.title} movie review`);
        const review = videos[0];

        if (!review || !review.key) {
            return;
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'movie_review',
            columns: ['movie_id', '`key`'],
            where: {
                movie_id: movieId
            },
            data: [
                [movieId, review.key]
            ]
        });
    }

    private processProviders = async (transactionId: string, connection, movieId: number, externalMovie: IExternalMovie) => {
        // If movie is not yet released, skip this step
        if (!externalMovie.release_date || moment().isBefore(externalMovie.release_date)) {
            return;
        }

        const [rowsFound] = await connection.query('SELECT created_at FROM movie_provider WHERE movie_id = ?', [movieId]);

        // Only process reviews after 2 months
        if (rowsFound && rowsFound[0]) {
            const reviewMoment = moment(rowsFound[0].created_at);
            const twoMonthsMoment = moment().subtract(2, 'months');
            if (!reviewMoment.isBefore(twoMonthsMoment)) {
                return;
            }
        }

        logger.debug(`Processing external movie "${externalMovie.id}" providers`, { transactionId });

        const items = await itunesService.search(transactionId, externalMovie.title);
        const itunesMovie = items[0];

        if (!itunesMovie || !itunesMovie.trackViewUrl) {
            return;
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'movie_provider',
            columns: ['movie_id', 'provider_id', 'link'],
            where: {
                movie_id: movieId
            },
            data: [
                [movieId, MovieProvider.ITUNES, itunesMovie.trackViewUrl]
            ]
        });
    }

    private processTranslations = async (transactionId: string, connection, movieId: number, externalMovie: IExternalMovie) => {
        if (!externalMovie.translations || !externalMovie.translations.translations || !externalMovie.translations.translations.length) {
            return false;
        }

        logger.debug(`Processing external Movie "${externalMovie.id}" translations`, { transactionId });

        const defaultLanguages = languageManager.getDefaultLanguages();
        const uniqueTranslations = utils.removeDuplicates(externalMovie.translations.translations, ['iso_639_1', 'iso_3166_1']);
        let parsedTranslations: IParsedTranslations[] = [];

        for (const translation of uniqueTranslations) {
            // Filter empty translations
            if (translation.data.title || translation.data.overview) {
                const shouldInsert = defaultLanguages.includes(translation.iso_639_1);
                const lang = await languageManager.get(translation.iso_639_1, translation.iso_3166_1);
                if (shouldInsert && lang) {
                    parsedTranslations.push({
                        movie_id: movieId,
                        language_id: lang.language_id,
                        language_locale_id: lang.language_locale_id,
                        title: translation.data.title || null,
                        overview: translation.data.overview || null,
                        homepage: translation.data.homepage || null
                    });
                }
            }
        }

        // Eliminar repetidos otra vez, ya que en idiomas chinos no detecta el locale y queda duplicado
        parsedTranslations = utils.removeDuplicates(parsedTranslations, ['language_id', 'language_locale_id']);

        if (!parsedTranslations.length) {
            return false;
        }

        const [currentTranslations] = await connection.query('SELECT * FROM movie_language WHERE movie_id = ?', [movieId]);

        const translationsToUpdate: any = [];
        const translationsToInsert: any = [];

        for (const translation of parsedTranslations) {
            const foundLanguage = currentTranslations.find((obj) => {
                return obj.language_id === translation.language_id && obj.language_locale_id === translation.language_locale_id;
            });

            if (!foundLanguage) {
                translationsToInsert.push(translation);
            } else if (translation.title !== foundLanguage.title || translation.overview !== foundLanguage.overview) {
                translationsToUpdate.push({
                    id: foundLanguage.id,
                    ...translation
                });
            }
        }

        if (!translationsToInsert.length && !translationsToUpdate.length) {
            return false;
        }

        await this.insertTranslations(transactionId, connection, translationsToInsert);
        await this.updateTranslations(transactionId, connection, translationsToUpdate);

        return true;
    }

    private insertTranslations = async (transactionId, connection, translationsToInsert) => {
        if (!translationsToInsert.length) {
            return;
        }

        logger.debug(`Creating "${translationsToInsert.length}" translations`, { transactionId });

        const query = `
            INSERT INTO
                movie_language
                (movie_id, language_id, language_locale_id, title, overview, homepage)
            VALUES ?`;
        const data = translationsToInsert.map((lang) => {
            return [
                lang.movie_id,
                lang.language_id,
                lang.language_locale_id,
                lang.title,
                lang.overview,
                lang.homepage
            ];
        });

        await connection.query(query, [data]);
    }

    private updateTranslations = async (transactionId, connection, translationsToUpdate) => {
        if (!translationsToUpdate.length) {
            return;
        }

        logger.debug(`Updating "${translationsToUpdate.length}" translations`, { transactionId });

        for (const translation of translationsToUpdate) {
            const dataUpdate = { ...translation };
            delete dataUpdate.id;
            await connection.query('UPDATE movie_language SET ? WHERE id = ?', [dataUpdate, translation.id]);
        }

        // Delete spins
        const translationIds = translationsToUpdate.map((t) => t.id);
        logger.debug('Deleting old spins because translations changed', { transactionId });

        await connection.query('DELETE FROM movie_language_website WHERE movie_language_id IN (?)', [translationIds]);
    }

    private processHashes = async (transactionId: string, connection, movieId: number, externalMovie: IExternalMovie) => {
        if (!externalMovie.translations || !externalMovie.translations.translations || !externalMovie.translations.translations.length) {
            return false;
        }

        logger.debug(`Processing external movie "${externalMovie.id}" hashes`, { transactionId });

        // Parse hashes
        const defaultLanguages = languageManager.getDefaultLanguages();
        const parsedNames: any = [];

        // Idioma principal
        const englishTitle = utils.normalizeString(externalMovie.title);
        if (englishTitle) {
            const englishLang = await languageManager.get('en', 'US');
            if (englishLang) {
                parsedNames.push({
                    title: englishTitle,
                    language_id: englishLang.language_id,
                    language_locale_id: englishLang.language_locale_id
                });
            }
        }

        for (const translation of externalMovie.translations.translations) {
            // Filter translations without name
            if (translation.data.title) {
                const isDefault = defaultLanguages.includes(translation.iso_639_1);
                const lang = await languageManager.get(translation.iso_639_1, translation.iso_3166_1);
                // Language must exist and be one of the ones we care about
                if (isDefault && lang) {
                    const title = utils.normalizeString(translation.data.title);
                    if (title) {
                        parsedNames.push({
                            title,
                            original_title: translation.data.title || null,
                            language_id: lang.language_id,
                            language_locale_id: lang.language_locale_id,
                        });
                    }
                }
            }
        }

        const uniqueNames = utils.removeDuplicates(parsedNames, ['title']);

        if (!uniqueNames.length) {
            return false;
        }

        // Search current hashes
        const findQuery = `
            SELECT
                language_id,
                language_locale_id,
                hash
            FROM 
                movie_hash
            WHERE
                movie_id = ?
        `;
        const [rows] = await connection.query(findQuery, [movieId]);

        // Filter only hashes from languages that we dont currently have
        const namesToInsert = uniqueNames.filter((name) => {
            return !rows.find((r) => r.language_id === name.language_id && r.language_locale_id === name.language_locale_id);
        });

        if (!namesToInsert.length) {
            return false;
        }

        logger.debug(`Inserting "${namesToInsert.length}" hashes for external Movie "${externalMovie.id}"`, { transactionId });

        for (const name of namesToInsert) {
            const hash = await entitiesHelper.findHash(connection, {
                table: 'movie_hash',
                column: 'hash',
                hash: name.title
            });

            await connection.query('INSERT INTO movie_hash SET ?', [{
                movie_id: movieId,
                language_id: name.language_id,
                language_locale_id: name.language_locale_id,
                hash
            }]);
        }

        return true;
    }

    private setLanguageUpdated = async (transactionId: string, connection, movieId: number, externalMovie: IExternalMovie) => {
        logger.debug(`Setting external movie "${externalMovie.id}" last language update date`, { transactionId });

        const data = {
            language_updated_at: new Date()
        };
        await connection.query('UPDATE movie SET ? WHERE id = ?', [data, movieId]);
    }

    private findMovieInfo = async (externalMovieId: number) => {
        const [rows] = await dbPool.query(`
            SELECT
                m.*, im_1.path poster_path, im_2.path backdrop_path
            FROM
                movie m
            LEFT JOIN
                image_media im_1 ON m.poster_image_media_id = im_1.id
            LEFT JOIN
                image_media im_2 ON m.backdrop_image_media_id = im_2.id
            WHERE
                external_id = ?`, [externalMovieId]
        );
        return rows[0];
    }
}

export default new MovieManager();