import { v4 } from 'uuid';

import { ImageMediaStatus } from '../../../enums/ImageMediaStatus';
import { ImageMediaType } from '../../../enums/ImageMediaType';
import { IImageMedia } from '../../../interfaces/ImageMedia';
import * as entitiesHelper from '../helpers/entities';

export interface IImageMediaArgs {
    path: string;
    imageMediaTypeId: ImageMediaType;
}

class ImageMediaManager {
    public findOrCreate = async (connection, args: IImageMediaArgs): Promise<IImageMedia|null> => {
        // Desactivar determinado tipo de imagenes
        if ([ImageMediaType.LOGO, ImageMediaType.PROFILE].includes(args.imageMediaTypeId)) {
            return null;
        }

        return entitiesHelper.findOrCreate(connection, {
            table: 'image_media',
            columns: ['id', 'path', 'file_name', 'image_media_type_id', 'image_media_status_id'],
            where: {
                path: args.path
            },
            data: {
                path: args.path,
                file_name: `${v4()}.jpg`,
                image_media_type_id: args.imageMediaTypeId,
                image_media_status_id: ImageMediaStatus.PENDING
            }
        });
    }
}

export default new ImageMediaManager();