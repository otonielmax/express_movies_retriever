import * as moment from 'moment';
import * as squel from 'squel';

import dbPool from '../../../database/pool';
import { ImageMediaType } from '../../../enums/ImageMediaType';
import * as utils from '../../../helpers/utils';
import { logger } from '../../../logger';
import languageManager from '../../../managers/Language';
import * as entitiesHelper from '../helpers/entities';
import needsUpdate from '../helpers/needsUpdate';
import { IExternalSerie } from '../interfaces/ExternalSerie';
import { IExternalVideo } from '../interfaces/ExternalVideo';
import tmbdApi from '../services/TMDB';
import companyManager from './Company';
import episodeManager from './Episode';
import genreManager from './Genre';
import imageMediaManager from './ImageMedia';
import personManager from './Person';
import seasonManager from './Season';
import serieTranslationManager from './SerieTranslation';
import videoMediaManager from './VideoMedia';

export interface IExternalSerieBasic {
    externalId: number;
    name: string;
}

export interface ISerieBasic {
    id: number;
    poster_path: string;
    backdrop_path: string;
}

class SerieManager {
    public sync = async (jobTransactionId: string, args: IExternalSerieBasic) => {
        const transactionId = `${jobTransactionId}:serie:${args.externalId}`;
        
        logger.info(`Processing external serie "${args.externalId}"`, { transactionId });

        const startDate = moment().subtract(13, 'days').format('YYYY-MM-DD');
        const endDate = moment().format('YYYY-MM-DD');
        const externalSerie = await this.getExternalSerie(transactionId, args.externalId, startDate, endDate);

        // Search serie and check if it must be updated
        const serieFound = await this.findSerieInfo(externalSerie.id);
        const shouldUpdate = needsUpdate(transactionId, externalSerie.changes, serieFound ? serieFound.updated_at : null, startDate);
        if (!shouldUpdate) {
            return;
        }
        
        let connection;
        try {
            connection = await dbPool.getConnection();
            await connection.beginTransaction();

            // Serie basic info
            const serieId = await this.processSerieInfo(transactionId, connection, externalSerie, serieFound);
            // Genres
            await this.processSerieGenres(transactionId, connection, serieId, externalSerie);
            // Performers
            await this.processPerformers(transactionId, connection, serieId, externalSerie);
            // Crew
            await this.processCrew(transactionId, connection, serieId, externalSerie);
            // Creators
            await this.processCreators(transactionId, connection, serieId, externalSerie);
            // Production companies
            await this.processCompanies(transactionId, connection, serieId, externalSerie);
            // Networks
            await this.processNetworks(transactionId, connection, serieId, externalSerie);
            // Keywords
            await this.processKeywords(transactionId, connection, serieId, externalSerie);
            // Videos
            await this.processVideos(transactionId, connection, serieId, externalSerie);
            // Hashes
            await this.processHashes(transactionId, connection, serieId, externalSerie);
            // Translations
            const languageUpdated = await serieTranslationManager.sync(transactionId, connection, {
                serie_id: serieId
            }, externalSerie.translations);
            if (languageUpdated) {
                await this.setLanguageUpdated(transactionId, connection, serieId, externalSerie);
            }

            // Commit and close connection
            await connection.commit();
            connection.release();
            connection = null;

            // Process all seasons
            for (const season of externalSerie.seasons) {
                await seasonManager.sync(transactionId, serieId, externalSerie.id, season.season_number);
            }

            // Process next and last episode
            await this.processLastAndNextEpisode(transactionId, serieId, externalSerie);
        } catch (err) {
            if (connection) {
                await connection.rollback();
                connection.release();
            }
            throw err;
        }
    }

    public setGalleryOrder = async (transactionId: string, gallery: string, externalIds: [IExternalSerieBasic]) => {
        let connection;
        try {
            connection = await dbPool.getConnection();
            await connection.beginTransaction();

            const columnName = `gallery_${gallery}_position`;

            // Clean current flags
            logger.info(`Cleaning all gallery "${gallery}" orders`, { transactionId });
            const updateAllQuery = squel.update().table('serie').set(columnName, null);
            const preparedUpdateAllQuery = updateAllQuery.toParam();
            await connection.query(preparedUpdateAllQuery.text, preparedUpdateAllQuery.values);

            // Set new flags
            logger.info(`Setting new gallery "${gallery}" orders`, { transactionId });
            for (let i = 0; i < externalIds.length; i++) {
                const position = i + 1;
                const updateOneQuery =
                    squel.update().table('serie').set(columnName, position).where('external_id = ?', externalIds[i].externalId);
                const preparedUpdateOneQuery = updateOneQuery.toParam();

                await connection.query(preparedUpdateOneQuery.text, preparedUpdateOneQuery.values);
            }

            await connection.commit();
        } catch (err) {
            if (connection) {
                await connection.rollback();
                connection.release();
            }
            throw err;
        }
    }

    private getExternalSerie = async (
        transactionId: string, externalId: number, startDate: string, endDate: string): Promise<IExternalSerie> => {
            const { body } = await tmbdApi.request(transactionId, {
                path: `/tv/${externalId}`,
                qs: {
                    append_to_response:
                        'alternative_titles,credits,external_ids,keywords,translations,videos,changes',
                        start_date: startDate,
                        end_date: endDate
                }
            });
            return body;
    }

    private processSerieStatus = async (transactionId, connection, externalSerie: IExternalSerie) => {
        const status = externalSerie.status.toUpperCase().replace(/ /g, '_');
        logger.debug(`Processing serie status "${status}"`, { transactionId });
        const entity = await entitiesHelper.findOrCreate(connection, {
            table: 'serie_status',
            columns: ['id', 'name'],
            where: {
                name: status,
            },
            data: {
                name: status,
            },
        });

        return entity.id;
    }

    private processSerieType = async (transactionId, connection, externalSerie: IExternalSerie) => {
        const serieType = externalSerie.type.toUpperCase().replace(/ /g, '_');
        logger.debug(`Processing serie type "${serieType}"`, { transactionId });
        const entity = await entitiesHelper.findOrCreate(connection, {
            table: 'serie_type',
            columns: ['id', 'name'],
            where: {
                name: serieType,
            },
            data: {
                name: serieType,
            },
        });

        return entity.id;
    }

    private processSerieInfo = async (transactionId: string, connection, externalSerie: IExternalSerie, serie: ISerieBasic|null) => {
        const serieStatusId = await this.processSerieStatus(transactionId, connection, externalSerie);
        const serieTypeId = await this.processSerieType(transactionId, connection, externalSerie);

        logger.debug(`Processing external serie "${externalSerie.id}" info`, { transactionId });

        const data: any = {
            external_id: externalSerie.id,
            title: externalSerie.name,
            original_title: externalSerie.original_name || null,
            overview: externalSerie.overview || null,
            popularity: externalSerie.popularity,
            poster_path: externalSerie.poster_path || null,
            backdrop_path: externalSerie.backdrop_path || null,
            homepage: externalSerie.homepage || null,
            vote_average: externalSerie.vote_average || null,
            vote_count: externalSerie.vote_count || null,
            first_air_date: externalSerie.first_air_date || null,
            last_air_date: externalSerie.last_air_date || null,
            number_of_episodes: externalSerie.number_of_episodes || 0,
            number_of_seasons: externalSerie.number_of_seasons || 0,
            in_production: externalSerie.in_production ? 1 : 0,
            serie_status_id: serieStatusId,
            serie_type_id: serieTypeId,
            updated_at: new Date()
        };

        if (externalSerie.external_ids) {
            data.imdb_id = externalSerie.external_ids.imdb_id || null;
            data.facebook_id = externalSerie.external_ids.facebook_id || null;
            data.twitter_id = externalSerie.external_ids.twitter_id || null;
            data.instagram_id = externalSerie.external_ids.instagram_id || null;
        }

        // Procesar imagenes solo si cambio algo
        if (externalSerie.poster_path && (!serie || externalSerie.poster_path !== serie.poster_path)) {
            const entity = await imageMediaManager.findOrCreate(connection, {
                path: externalSerie.poster_path,
                imageMediaTypeId: ImageMediaType.POSTER
            });

            if (entity) {
                data.poster_image_media_id = entity.id;
            }
        }
        if (externalSerie.backdrop_path && (!serie || externalSerie.backdrop_path !== serie.backdrop_path)) {
            const entity = await imageMediaManager.findOrCreate(connection, {
                path: externalSerie.backdrop_path,
                imageMediaTypeId: ImageMediaType.BACKDROP
            });

            if (entity) {
                data.backdrop_image_media_id = entity.id;
            }
        }

        if (serie && serie.id) {
            await connection.query('UPDATE serie SET ? WHERE id = ?', [data, serie.id]);
            return serie.id;
        }

        const [ result ] = await connection.query('INSERT INTO serie SET ?', [data]);

        return result.insertId;
    }

    private processSerieGenres = async (transactionId: string, connection, serieId: number, externalSerie: IExternalSerie) => {
        if (!externalSerie.genres || !externalSerie.genres.length) {
             return;
        }

        logger.debug(`Processing external serie "${externalSerie.id}" genres`, { transactionId });
        const genres = utils.removeDuplicates(externalSerie.genres, ['id']);
        const insertGenres: any = [];

        for (const genre of genres) {
            const foundGenre = await genreManager.findGenreInfo(genre.id);
            if (foundGenre) {
                insertGenres.push([serieId, foundGenre.id]);
            }
        }

        if (!insertGenres.length) {
            logger.info(`No genres found for external serie "${externalSerie.id}"`, {
                transactionId,
                externalGenres: externalSerie.genres
            });
            return;
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'serie_genre',
            columns: ['serie_id', 'genre_id'],
            where: {
                serie_id: serieId
            },
            data: insertGenres
        });
    }

    private processPerformers = async (transactionId: string, connection, serieId: number, externalSerie: IExternalSerie) => {
        if (!externalSerie.credits || !externalSerie.credits.cast || !externalSerie.credits.cast.length) {
            return;
        }

        logger.debug(`Processing external serie "${externalSerie.id}" performers`, { transactionId });

        const insertPerformers: any = [];
        for (const performer of externalSerie.credits.cast) {
            const person = await personManager.findOrCreate(connection, {
                name: performer.name,
                gender_id: performer.gender,
                profile_path: performer.profile_path,
                external_id: performer.id
            });
            insertPerformers.push([
                serieId,
                person.id,
                performer.character,
                performer.order
            ]);
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'serie_performer',
            columns: ['serie_id', 'person_id', '`character`', '`order`'],
            where: {
                serie_id: serieId
            },
            data: insertPerformers
        });
    }

    private processCrew = async (transactionId: string, connection, serieId: number, externalSerie: IExternalSerie) => {
        if (!externalSerie.credits || !externalSerie.credits.crew || !externalSerie.credits.crew.length) {
            return;
        }

        logger.debug(`Processing external serie "${externalSerie.id}" crew`, { transactionId });

        const insertPerformers: any = [];
        for (const crew of externalSerie.credits.crew) {
            const person = await personManager.findOrCreate(connection, {
                name: crew.name,
                gender_id: crew.gender,
                profile_path: crew.profile_path,
                external_id: crew.id
            });
            insertPerformers.push([
                serieId,
                person.id,
                crew.job,
                crew.department
            ]);
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'serie_crew',
            columns: ['serie_id', 'person_id', 'job', 'department'],
            where: {
                serie_id: serieId
            },
            data: insertPerformers
        });
    }

    private processCreators = async (transactionId: string, connection, serieId: number, externalSerie: IExternalSerie) => {
        if (!externalSerie.created_by || !externalSerie.created_by.length) {
            return;
        }

        logger.debug(`Processing external serie "${externalSerie.id}" creators`, { transactionId });

        const insertCreators: any = [];
        for (const creator of externalSerie.created_by) {
            const person = await personManager.findOrCreate(connection, {
                name: creator.name,
                gender_id: null,
                profile_path: creator.profile_path,
                external_id: creator.id
            });
            insertCreators.push([
                serieId,
                person.id
            ]);
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'serie_creator',
            columns: ['serie_id', 'person_id'],
            where: {
                serie_id: serieId
            },
            data: insertCreators
        });
    }

    private processCompanies = async (transactionId: string, connection, serieId: number, externalSerie: IExternalSerie) => {
        if (!externalSerie.production_companies || !externalSerie.production_companies.length) {
            return;
        }

        logger.debug(`Processing external serie "${externalSerie.id}" companies`, { transactionId });
        
        const insertCompanies: any = [];
        for (const company of externalSerie.production_companies) {
            const insertedCompany = await companyManager.findOrCreate(connection, {
                name: company.name,
                logo_path: company.logo_path,
                external_id: company.id
            });

            insertCompanies.push([
                serieId,
                insertedCompany.id
            ]);
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'serie_company',
            columns: ['serie_id', 'company_id'],
            where: {
                serie_id: serieId
            },
            data: insertCompanies
        });
    }

    private processNetworks = async (transactionId: string, connection, serieId: number, externalSerie: IExternalSerie) => {
        if (!externalSerie.networks || !externalSerie.networks.length) {
            return;
        }

        logger.debug(`Processing external serie "${externalSerie.id}" networks`, { transactionId });
        
        const insertNetworks: any = [];
        for (const network of externalSerie.networks) {
            // process images
            let logoImageMedia: null|number = null;
            if (network.logo_path) {
                const imageEntity = await imageMediaManager.findOrCreate(connection, {
                    path: network.logo_path,
                    imageMediaTypeId: ImageMediaType.LOGO
                });

                if (imageEntity) {
                    logoImageMedia = imageEntity.id as number;
                }
            }

            const entity = await entitiesHelper.findOrCreate(connection, {
                table: 'network',
                columns: ['id', 'name', 'logo_path', 'external_id'],
                where: {
                    external_id: network.id,
                },
                data: {
                    name: network.name,
                    logo_path: network.logo_path,
                    logo_image_media_id: logoImageMedia,
                    external_id: network.id
                },
            });

            insertNetworks.push([
                serieId,
                entity.id
            ]);
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'serie_network',
            columns: ['serie_id', 'network_id'],
            where: {
                serie_id: serieId
            },
            data: insertNetworks
        });

    }

    private processKeywords = async (transactionId: string, connection, serieId: number, externalSerie: IExternalSerie) => {
        if (!externalSerie.keywords || !externalSerie.keywords.results || !externalSerie.keywords.results.length) {
            return;
        }

        logger.debug(`Processing external serie "${externalSerie.id}" keywords`, { transactionId });
        
        const insertKeywords: any = [];
        for (const keyword of externalSerie.keywords.results) {
            const entity = await entitiesHelper.findOrCreate(connection, {
                table: 'keyword',
                columns: ['id', 'name', 'external_id'],
                where: {
                    external_id: keyword.id,
                },
                data: {
                    name: keyword.name,
                    external_id: keyword.id
                }
            });

            insertKeywords.push([
                serieId,
                entity.id
            ]);
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'serie_keyword',
            columns: ['serie_id', 'keyword_id'],
            where: {
                serie_id: serieId
            },
            data: insertKeywords
        });
    }

    private processVideos = async (transactionId: string, connection, serieId: number, externalSerie: IExternalSerie) => {
        if (!externalSerie.videos || !externalSerie.videos.results || !externalSerie.videos.results.length) {
            return;
        }

        logger.debug(`Processing external serie "${externalSerie.id}" videos`, { transactionId });

        const videos: IExternalVideo[] = utils.removeDuplicates(externalSerie.videos.results, ['id']);
        const insertVideos: any = [];
        for (const video of videos) {
            const entity = await videoMediaManager.findOrCreate(connection, video);
            if (entity) {
                insertVideos.push([
                    serieId,
                    entity.id
                ]);
            }
        }

        if (!insertVideos.length) {
            return;
        }

        await entitiesHelper.removeAndInsert(connection, {
            table: 'serie_video',
            columns: ['serie_id', 'video_media_id'],
            where: {
                serie_id: serieId
            },
            data: insertVideos
        });
    }

    private processHashes = async (transactionId: string, connection, serieId: number, externalSerie: IExternalSerie) => {
        if (!externalSerie.translations || !externalSerie.translations.translations || !externalSerie.translations.translations.length) {
            return false;
        }

        logger.debug(`Processing external Serie "${externalSerie.id}" hashes`, { transactionId });

        // Parse hashes
        const defaultLanguages = languageManager.getDefaultLanguages();
        const parsedNames: any = [];

        // Idioma principal
        const englishTitle = utils.normalizeString(externalSerie.name);
        if (englishTitle) {
            const englishLang = await languageManager.get('en', 'US');
            if (englishLang) {
                parsedNames.push({
                    title: englishTitle,
                    language_id: englishLang.language_id,
                    language_locale_id: englishLang.language_locale_id
                });
            }
        }
        
        for (const translation of externalSerie.translations.translations) {
            // Filter translations without name
            if (translation.data.name) {
                const isDefault = defaultLanguages.includes(translation.iso_639_1);
                const lang = await languageManager.get(translation.iso_639_1, translation.iso_3166_1);
                // Language must exist and be one of the ones we care about
                if (isDefault && lang) {
                    const title = utils.normalizeString(translation.data.name);
                    if (title && title.length) {
                        parsedNames.push({
                            title,
                            original_title: translation.data.name || null,
                            language_id: lang.language_id,
                            language_locale_id: lang.language_locale_id,
                        });
                    }
                }
            }
        }

        if (!parsedNames.length) {
            return false;
        }

        // Search current hashes
        const findQuery = `
            SELECT
                language_id,
                language_locale_id,
                hash
            FROM 
                serie_hash
            WHERE
                serie_id = ?
        `;
        const [ rows ] = await connection.query(findQuery, [serieId]);

        // Filter only hashes from languages that we dont currently have
        const namesToInsert = parsedNames.filter((name) => {
            return !rows.find((r) => r.language_id === name.language_id && r.language_locale_id === name.language_locale_id);
        });

        if (!namesToInsert.length) {
            return false;
        }

        logger.debug(`Inserting "${namesToInsert.length}" hashes for external Serie "${externalSerie.id}"`, { transactionId });

        for (const name of namesToInsert) {
            const hash = await entitiesHelper.findHash(connection, {
                table: 'serie_hash',
                column: 'hash',
                hash: name.title,
                where: [{
                    column: 'serie_id',
                    operator: '!=',
                    value: serieId
                }]
            });

            await connection.query('INSERT INTO serie_hash SET ?', [{
                serie_id: serieId,
                language_id: name.language_id,
                language_locale_id: name.language_locale_id,
                hash
            }]);
        }

        return true;
    }

    private processLastAndNextEpisode = async (transactionId: string, serieId: number, externalSerie: IExternalSerie) => {
        logger.debug(`Processing external serie "${externalSerie.id}" last and next episode`, { transactionId });

        let nextEpisodeId = null;
        let lastEpisodeId = null;

        if (externalSerie.next_episode_to_air) {
            const episodeFound = await episodeManager.findEpisodeInfo(null, externalSerie.next_episode_to_air.id);
            if (episodeFound) {
                nextEpisodeId = episodeFound.id;
            }
        }

        if (externalSerie.last_episode_to_air) {
            const episodeFound = await episodeManager.findEpisodeInfo(null, externalSerie.last_episode_to_air.id);
            if (episodeFound) {
                lastEpisodeId = episodeFound.id;
            }
        }

        await dbPool.query('UPDATE serie SET ? WHERE id=?', [{
            next_episode_id: nextEpisodeId,
            last_episode_id: lastEpisodeId
        }, serieId]);
    }

    private setLanguageUpdated = async (transactionId: string, connection, serieId: number, externalSerie: IExternalSerie) => {
        logger.debug(`Setting external serie "${externalSerie.id}" last language update date`, { transactionId });

        const data = {
            language_updated_at: new Date()
        };
        await connection.query('UPDATE serie SET ? WHERE id = ?', [data, serieId]);
    }

    private findSerieInfo = async (externalSerieId: number) => {
        const [rows] = await dbPool.query(`
            SELECT
                s.*, im_1.path poster_path, im_2.path backdrop_path
            FROM
                serie s
            LEFT JOIN
                image_media im_1 ON s.poster_image_media_id = im_1.id
            LEFT JOIN
                image_media im_2 ON s.backdrop_image_media_id = im_2.id
            WHERE
                external_id = ?`, [externalSerieId]
        );
        return rows[0];
    }
}

export default new SerieManager();
