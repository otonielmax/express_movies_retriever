import languageManager from '../../../managers/Language';
import * as entitiesHelper from '../helpers/entities';
import { IExternalVideo } from '../interfaces/ExternalVideo';

class VideoMediaManager {
    public findOrCreate = async (connection, videoMedia: IExternalVideo) => {
        // Si el lenguaje no existe, no insertamos el video
        const lang = await languageManager.get(videoMedia.iso_639_1, videoMedia.iso_3166_1);
        if (!lang) {
            return null;
        }

        return entitiesHelper.findOrCreate(connection, {
            table: 'video_media',
            columns: ['id', 'name', 'site', 'key', 'size', 'type', 'external_id'],
            where: {
                external_id: videoMedia.id
            },
            data: {
                name: videoMedia.name,
                site: videoMedia.site,
                key: videoMedia.key,
                size: videoMedia.size,
                type: videoMedia.type,
                external_id: videoMedia.id,
                language_id: lang.language_id,
                language_locale_id: lang.language_locale_id || null
            }
        });
    }
}

export default new VideoMediaManager();