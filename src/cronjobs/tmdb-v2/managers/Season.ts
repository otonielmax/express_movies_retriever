import dbPool from '../../../database/pool';
import { ImageMediaType } from '../../../enums/ImageMediaType';
import * as utils from '../../../helpers/utils';
import { logger } from '../../../logger';
import * as entitiesHelper from '../helpers/entities';
import { IExternalSeason } from '../interfaces/ExternalSeason';
import { IExternalVideo } from '../interfaces/ExternalVideo';
import tmbdApi from '../services/TMDB';
import episodeManager from './Episode';
import imageMediaManager from './ImageMedia';
import personManager from './Person';
import videoMediaManager from './VideoMedia';

class SeasonManager {
    public sync = async (transactionId: string, serieId: number, externalSerieId: number, seasonNumber: number) => {
        logger.info(`Processing external serie "${externalSerieId}" season "${seasonNumber}"`, { transactionId });

        // Retrieve from TMDB
        const externalSeason = await this.getExternalSeason(transactionId, externalSerieId, seasonNumber);

        let connection;
        try {
            connection = await dbPool.getConnection();
            await connection.beginTransaction();
            
            // Basic info
            const seasonId = await this.processSeasonInfo(transactionId, connection, serieId, externalSeason);
            // Videos
            await this.processVideos(transactionId, connection, seasonId, externalSeason);
            // Performers
            await this.processPerformers(transactionId, connection, seasonId, externalSeason);
            // Crew
            await this.processCrew(transactionId, connection, seasonId, externalSeason);
    
            // Commit and close connection
            await connection.commit();
            connection.release();
            connection = null;

            // Process all episodes
            for (const episode of externalSeason.episodes) {
                await episodeManager.sync(transactionId, {
                    serieId,
                    seasonId,
                    externalSerieId,
                    seasonNumber,
                    episodeNumber: episode.episode_number
                });
            }
        } catch (err) {
            if (connection) {
                await connection.rollback();
                connection.release();
            }
            throw err;
        }
    }

    private getExternalSeason = async (transactionId: string, externalSerieId: number, seasonNumber: number): Promise<IExternalSeason> => {
        const { body } = await tmbdApi.request(transactionId, {
            path: `/tv/${externalSerieId}/season/${seasonNumber}`,
            qs: {
                append_to_response:
                    'credits,videos'
            }
        });
        return body;
    }

    private processSeasonInfo = async (
        transactionId: string, connection, serieId: number, externalSeason: IExternalSeason): Promise<number> => {
            logger.debug(`Processing external season "${externalSeason.id}" info`, { transactionId });

            const existingSeason = await this.findSeasonInfo(connection, externalSeason.id);

            const data: any = {
                external_id: externalSeason.id,
                serie_id: serieId,
                season_number: externalSeason.season_number,
                title: externalSeason.name || null,
                overview: externalSeason.overview || null,
                poster_path: externalSeason.poster_path || null,
                air_date: externalSeason.air_date || null,
                updated_at: new Date()
            };

            // process images
            if (externalSeason.poster_path && (!existingSeason || externalSeason.poster_path !== existingSeason.image_media_poster_path)) {
                const entity = await imageMediaManager.findOrCreate(connection, {
                    path: externalSeason.poster_path,
                    imageMediaTypeId: ImageMediaType.POSTER
                });

                if (entity) {
                    data.poster_image_media_id = entity.id;
                }
            }

            let seasonId: number;
            if (existingSeason && existingSeason.id) {
                await connection.query('UPDATE season SET ? WHERE id = ?', [data, existingSeason.id]);
                seasonId = existingSeason.id;
            } else {
                const [ result ] = await connection.query('INSERT INTO season SET ?', [data]);
                seasonId = result.insertId;
            }

            if (!existingSeason || existingSeason.title !== data.title || existingSeason.overview !== data.overview) {
                await this.setLanguageUpdated(transactionId, connection, seasonId, externalSeason);
            }

            return seasonId;
    }

    private processVideos = async (
        transactionId: string, connection, seasonId: number, externalSeason: IExternalSeason) => {
            if (!externalSeason.videos || !externalSeason.videos.results || !externalSeason.videos.results.length) {
                return;
            }

            logger.debug(`Processing external season "${externalSeason.id}" videos`, { transactionId });

            const videos: IExternalVideo[] = utils.removeDuplicates(externalSeason.videos.results, ['id']);
            const insertVideos: any = [];
            for (const video of videos) {
                const entity = await videoMediaManager.findOrCreate(connection, video);
                if (entity) {
                    insertVideos.push([
                        seasonId,
                        entity.id
                    ]);
                }
            }

            if (!insertVideos.length) {
                return;
            }

            await entitiesHelper.removeAndInsert(connection, {
                table: 'serie_video',
                columns: ['season_id', 'video_media_id'],
                where: {
                    season_id: seasonId
                },
                data: insertVideos
            });
    }

    private processPerformers = async (
        transactionId: string, connection, seasonId: number, externalSeason: IExternalSeason) => {
            if (!externalSeason.credits || !externalSeason.credits.cast || !externalSeason.credits.cast.length) {
                return;
            }

            logger.debug(`Processing external season "${externalSeason.id}" performers`, { transactionId });

            const insertPerformers: any = [];
            for (const performer of externalSeason.credits.cast) {
                const person = await personManager.findOrCreate(connection, {
                    name: performer.name,
                    gender_id: performer.gender,
                    profile_path: performer.profile_path,
                    external_id: performer.id
                });
                insertPerformers.push([
                    seasonId,
                    person.id,
                    performer.character,
                    performer.order
                ]);
            }

            await entitiesHelper.removeAndInsert(connection, {
                table: 'serie_performer',
                columns: ['season_id', 'person_id', '`character`', '`order`'],
                where: {
                    season_id: seasonId
                },
                data: insertPerformers
            });
    }

    private processCrew = async (
        transactionId: string, connection, seasonId: number, externalSeason: IExternalSeason) => {
            if (!externalSeason.credits || !externalSeason.credits.crew || !externalSeason.credits.crew.length) {
                return;
            }

            logger.debug(`Processing external season "${externalSeason.id}" crew`, { transactionId });

            const insertPerformers: any = [];
            for (const crew of externalSeason.credits.crew) {
                const person = await personManager.findOrCreate(connection, {
                    name: crew.name,
                    gender_id: crew.gender,
                    profile_path: crew.profile_path,
                    external_id: crew.id
                });
                insertPerformers.push([
                    seasonId,
                    person.id,
                    crew.job,
                    crew.department
                ]);
            }

            await entitiesHelper.removeAndInsert(connection, {
                table: 'serie_crew',
                columns: ['season_id', 'person_id', 'job', 'department'],
                where: {
                    season_id: seasonId
                },
                data: insertPerformers
            });
    }

    private setLanguageUpdated = async (transactionId: string, connection, seasonId: number, externalSeason: IExternalSeason) => {
        logger.debug(`Setting external season "${externalSeason.id}" last language update date`, { transactionId });

        const data = {
            language_updated_at: new Date()
        };
        await connection.query('UPDATE season SET ? WHERE id = ?', [data, seasonId]);
    }

    private findSeasonInfo = async (connection, externalSeasonId: number) => {
        const [rows] = await connection.query(`
            SELECT
                s.*, im.path AS image_media_poster_path
            FROM
                season s
            LEFT JOIN
                image_media im ON s.poster_image_media_id = im.id
            WHERE
                s.external_id = ?`, [externalSeasonId]
        );
        return rows[0];
    }
}

export default new SeasonManager();