import { removeDuplicates } from '../../helpers/utils';
import { logger } from '../../logger';
import movieManager from './managers/Movie';
import tmdbAPI from './services/TMDB';

const getPages = (transactionId: string, path: string, maxPages: number) => {
    let items = [];
    const processPage = async (page: number = 1) => {
        const params = {
            path,
            qs: {
                page
            }
        };
        const { body } = await tmdbAPI.request(transactionId, params);
        const { total_pages: totalPages, results } = body;
    
        if (!results || !results.length) {
            logger.info(`There are no items on page "${page}"`, { transactionId });
            return items;
        }

        const parsedResults = results.map((item) => {
            return {
                externalId: item.id,
                title: item.title
            };
        });
        items = items.concat(parsedResults);
    
        if (page >= totalPages || page >= maxPages) {
            logger.info('All pages have been processed', { transactionId });
            return items;
        }

        return processPage(page + 1);
    };

    return processPage();
};

export const syncMovies = async (transactionId: string) => {
    const topRated = await getPages(transactionId, '/movie/top_rated', 20);
    const popular = await getPages(transactionId, '/movie/popular', 20);
    const nowPlaying = await getPages(transactionId, '/movie/now_playing', 10);
    const upcoming = await getPages(transactionId, '/movie/upcoming', 10);

    // Juntar todos los arreglos y remover peliculas duplicadas para solo actualizarlas una vez.
    const allMovies = [].concat(...[topRated, popular, nowPlaying, upcoming]);
    const uniqueMovies = removeDuplicates(allMovies, ['externalId']);

    logger.info(`All lists have been retrieved, processing "${uniqueMovies.length}" movies`, { transactionId });

    for (const movie of uniqueMovies) {
        await movieManager.sync(transactionId, movie);
    }

    logger.info('Setting movie gallery positions', { transactionId });

    await movieManager.setGalleryOrder(transactionId, 'popular', popular);
    await movieManager.setGalleryOrder(transactionId, 'top_rated', topRated);
    await movieManager.setGalleryOrder(transactionId, 'now_playing', nowPlaying);
    await movieManager.setGalleryOrder(transactionId, 'upcoming', upcoming);
};