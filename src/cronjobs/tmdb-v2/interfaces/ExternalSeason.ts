import { IExternalCredit } from './ExternalCredit';
import { IExternalVideo } from './ExternalVideo';

export interface IExternalSeason {
    id: number;
    season_number: number;
    name: string|null;
    overview: string|null;
    poster_path: string|null;
    air_date: string|null;
    episodes: Array<{
        id: number;
        episode_number: number
    }>;
    credits: IExternalCredit;
    videos: {
        results: IExternalVideo[]
    };
}