import { IExternalChanges } from './ExternalChanges';
import { IExternalCredit } from './ExternalCredit';
import { IExternalKeyword } from './ExternalKeyword';
import { IExternalSerieTranslation } from './ExternalTranslation';
import { IExternalVideo } from './ExternalVideo';

export interface IExternalSerie {
    id: number;
    name: string;
    original_name: string;
    original_language: string;
    overview: string;
    backdrop_path: string;
    poster_path: string;
    popularity: number;
    genres: Array<{
        id: number;
        name: string
    }>;
    homepage: string;
    in_production: boolean;
    languages: string[];
    first_air_date: string;
    last_air_date: string;
    translations: IExternalSerieTranslation;
    last_episode_to_air: {
        id: number
    } | null;
    next_episode_to_air: {
        id: number
    } | null;
    networks: Array<{
        id: number;
        name: string;
        logo_path: string;
        origin_country: string
    }>;
    number_of_episodes: number;
    number_of_seasons: number;
    origin_country: string[];
    production_companies: Array<{
        id: number;
        name: string;
        logo_path: null;
        origin_country: string
    }>;
    seasons: Array<{
        id: number;
        season_number: number;
        name: string;
        overview: string;
        poster_path: string;
        episode_count: number;
        air_date: string
    }>;
    status: string;
    type: string;
    vote_average: number;
    vote_count: number;
    credits: IExternalCredit;
    external_ids: {
        imdb_id: string|null;
        freebase_mid: string|null;
        freebase_id: string|null;
        tvdb_id: number|null;
        tvrage_id: number|null;
        facebook_id: string|null;
        instagram_id: string|null;
        twitter_id: string|null
    };
    keywords: IExternalKeyword;
    videos: {
        results: IExternalVideo[]
    };
    created_by: Array<{
        id: number;
        credit_id: string;
        name: string;
        profile_path: string|null
    }>;
    changes: IExternalChanges;
}