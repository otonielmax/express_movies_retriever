import { IExternalCredit } from './ExternalCredit';
import { IExternalSerieTranslation } from './ExternalTranslation';
import { IExternalVideo } from './ExternalVideo';

export interface IExternalEpisode {
    air_date: string|null;
    name: string|null;
    overview: string|null;
    id: number;
    production_code: string|null;
    episode_number: number;
    season_number: number;
    still_path: string|null;
    vote_average: number|null;
    vote_count: number|null;
    credits: IExternalCredit;
    translations: IExternalSerieTranslation;
    videos: {
        results: IExternalVideo[]
    };
}