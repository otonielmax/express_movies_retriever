export interface IExternalCastCredit {
    id: number;
    name: string;
    gender: number;
    profile_path: string;
    character: string;
    credit_id: string;
    order: number;
}

export interface IExternalCrewCredit {
    id: number;
    name: string;
    gender: number;
    profile_path: string;
    job: string;
    department: string;
    credit_id: string;
}

export interface IExternalCredit {
    cast: IExternalCastCredit[];
    crew: IExternalCrewCredit[];
    guest_stars?: IExternalCastCredit[];
}