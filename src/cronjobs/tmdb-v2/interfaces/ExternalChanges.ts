export interface IExternalChanges {
    changes: Array<{
        key: string;
        items: Array<{
            id: string;
            time: string;
        }>;
    }>;
}