export interface IExternalGenreList {
    genres: IExternalGenre[];
}

export interface IExternalGenre {
    id: number;
    name: string;
}