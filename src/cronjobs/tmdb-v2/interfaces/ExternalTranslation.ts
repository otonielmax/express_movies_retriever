export interface IExternalSerieTranslation {
    translations: Array<{
        iso_3166_1: string,
        iso_639_1: string,
        name: string,
        english_name: string,
        data: {
            name: string,
            overview: string,
            homepage: string
        }
    }>;
}

export interface IExternalMovieTranslation {
    translations: Array<{
        iso_3166_1: string,
        iso_639_1: string,
        name: string,
        english_name: string,
        data: {
            title: string,
            overview: string,
            homepage: string
        }
    }>;
}