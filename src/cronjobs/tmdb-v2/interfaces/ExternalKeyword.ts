export interface IExternalKeyword {
    results: Array<{
        id: number,
        name: string
    }>;
}