import { IExternalChanges } from './ExternalChanges';
import { IExternalCredit } from './ExternalCredit';
import { IExternalKeyword } from './ExternalKeyword';
import { IExternalMovieTranslation } from './ExternalTranslation';
import { IExternalVideo } from './ExternalVideo';

export interface IExternalMovie {
    adult: boolean;
    backdrop_path: string | null;
    budget: number | null;
    homepage: string | null;
    id: number;
    imdb_id: string | null;
    original_language: string;
    original_title: string;
    overview: string | null;
    popularity: number;
    poster_path: string | null;
    release_date: string | null;
    revenue: number | null;
    runtime: number | null;
    // spoken_languages
    status: string;
    tagline: string | null;
    title: string;
    vote_average: number | null;
    vote_count: number | null;
    genres: Array<{
        id: number;
        name: string
    }>;
    external_ids: {
        imdb_id: string | null;
        facebook_id: string | null;
        instagram_id: string | null;
        twitter_id: string | null;
    };
    production_companies: Array<{
        id: number;
        name: string;
        logo_path: null;
        origin_country: string
    }>;
    production_countries: Array<{
        name: string;
        iso_3166_1: string;
    }>;
    credits: IExternalCredit;
    keywords: IExternalKeyword;
    videos: {
        results: IExternalVideo[]
    };
    translations: IExternalMovieTranslation;
    changes: IExternalChanges;
}