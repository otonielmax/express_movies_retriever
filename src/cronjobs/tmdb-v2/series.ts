import { removeDuplicates } from '../../helpers/utils';
import { logger } from '../../logger';
import serieManager from './managers/Serie';
import tmdbAPI from './services/TMDB';

const getPages = (transactionId: string, path: string, maxPages: number) => {
    let items = [];
    const processPage = async (page: number = 1) => {
        const params = {
            path,
            qs: {
                page
            }
        };
        const { body } = await tmdbAPI.request(transactionId, params);
        const { total_pages: totalPages, results } = body;
    
        if (!results || !results.length) {
            logger.info(`There are no items on page "${page}"`, { transactionId });
            return items;
        }

        const parsedResults = results.map((item) => {
            return {
                externalId: item.id,
                name: item.name
            };
        });
        items = items.concat(parsedResults);
    
        if (page >= totalPages || page >= maxPages) {
            logger.info('All pages have been processed', { transactionId });
            return items;
        }

        return processPage(page + 1);
    };

    return processPage();
};

export const syncSeries = async (transactionId: string) => {
    const topRated = await getPages(transactionId, '/tv/top_rated', 20);
    const airingToday = await getPages(transactionId, '/tv/airing_today', 10);
    const onTheAir = await getPages(transactionId, '/tv/on_the_air', 10);
    const popular = await getPages(transactionId, '/tv/popular', 20);

    // Juntar todos los arreglos y remover series duplicadas para solo actualizarlos una vez.
    const allSeries = [].concat(...[topRated, airingToday, onTheAir, popular]);
    const uniqueSeries = removeDuplicates(allSeries, ['externalId']);

    logger.info(`All lists have been retrieved, processing "${uniqueSeries.length}" series`, { transactionId });

    for (const serie of uniqueSeries) {
        await serieManager.sync(transactionId, serie);
    }

    logger.info('Setting gallery positions', { transactionId });

    await serieManager.setGalleryOrder(transactionId, 'top_rated', topRated);
    await serieManager.setGalleryOrder(transactionId, 'airing_today', airingToday);
    await serieManager.setGalleryOrder(transactionId, 'on_air', onTheAir);
    await serieManager.setGalleryOrder(transactionId, 'popular', popular);
};