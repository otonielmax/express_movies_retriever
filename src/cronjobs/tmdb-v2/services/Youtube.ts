import * as config from 'config';
import * as request from 'request-promise';

import { logger } from '../../../logger';

class YoutubeService {
    public search = async (transactionId: string, searchTerm: string) => {
        const params = {
            url: 'https://www.googleapis.com/youtube/v3/search',
            method: 'GET',
            qs: {
                key: config.YOUTUBE_API_KEY,
                part: 'id,snippet',
                q: searchTerm
            },
            json: true,
            resolveWithFullResponse: true
        };

        try {
            logger.info('< Youtube - REQ', { transactionId, params });
            const { statusCode, body } = await request(params);
            logger.info(`> Youtube - RES - ${statusCode}`, { transactionId, body });

            const videos = body && body.items ? body.items : [];
            return videos.map((v) => {
                return {
                    key: v.id.videoId,
                    title: v.snippet.title,
                    description: v.snippet.description,
                };
            });
        } catch (err) {
            const statusCode = err.statusCode;
            logger.error('> Youtube - RES - Error', { transactionId, statusCode, err });
            throw err;
        }
    }
}

export default new YoutubeService();