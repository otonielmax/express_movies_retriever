'use strict';

import * as config from 'config';
import * as moment from 'moment';
import * as request from 'request-promise';

import { logger } from '../../../logger';

class TMDBService {
    private requestTime: number;
    private requestAmount: number;

    public request = async (transactionId: string, params: any, retriesLeft: number = 10) => {
        await this.canPerformRequest();

        const customQs = params.qs || {};
        customQs.api_key = config.TMDB_API_KEY;

        const reqParams = {
            method: 'GET',
            url: `https://api.themoviedb.org/3${params.path}`,
            qs: customQs,
            json: true,
            resolveWithFullResponse: true
        };

        this.requestPerformed();

        try {
            logger.info('< TMDB - REQ', { transactionId, reqParams });
            const { body, statusCode } = await request(reqParams);
            logger.info(`> TMDB - RES - ${statusCode}`, { transactionId });
            return {
                body,
                statusCode
            };
        } catch (err) {
            const statusCode = err && err.statusCode ? err.statusCode : null;
            logger.error('> TMDB - RES - Error', {
                transactionId,
                reqParams,
                err
            });

            // Retry failures
            if (retriesLeft > 0 || statusCode === 429) {
                const seconds =  statusCode === 429 ? 1 : 5;
                const nextRetries = statusCode === 429
                    ? retriesLeft 
                    : retriesLeft - 1;

                logger.info(`TMDB - Retrying request in ${seconds} second`, { transactionId });
                // If it was a 429 error, the retry does not count
                await this.sleep(seconds * 1000);
                return this.request(transactionId, params, nextRetries);
            }

            logger.info('> TMDB - No more retries available', { transactionId });
            throw err;
        }
    }

    /**
     * Espera hasta que sea posible realizar el request a la API.
     * Solo permite 4 requests por segundo.
     */
    private canPerformRequest = async (): Promise<void> => {
        const now = moment().unix();

        if (this.requestTime !== now || this.requestAmount < 4) {
            return;
        }

        await this.sleep(100);

        return this.canPerformRequest();
    }

    /**
     * Incrementa el contador de requests por segundo a la API.
     */
    private requestPerformed = () => {
        const now = moment().unix();

        if (this.requestTime === now) {
            // Si la hora es la misma, incrementa el contador.
            this.requestAmount++;
        } else {
            // Si cambio el segundo, empieza de 1.
            this.requestTime = now;
            this.requestAmount = 1;
        }
    }

    private sleep = (t: number): Promise<void> => {
        return new Promise((resolve) => setTimeout(resolve, t));
    }
}

export default new TMDBService();
