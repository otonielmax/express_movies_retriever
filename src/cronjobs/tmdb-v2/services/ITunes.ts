import * as request from 'request-promise';

import { logger } from '../../../logger';

class ITunesService {
    public search = async (transactionId: string, searchTerm: string) => {
        const params = {
            url: 'https://itunes.apple.com/search',
            method: 'GET',
            qs: {
                media: 'movie',
                term: searchTerm.replace(/ /g, '+')
            },
            json: true,
            resolveWithFullResponse: true
        };

        try {
            logger.info('< iTunes - REQ', { transactionId, params });
            const { statusCode, body } = await request(params);
            logger.info(`> iTunes - RES - ${statusCode}`, { transactionId, body });

            return body.results || [];
        } catch (err) {
            const { statusCode, body } = err;
            if (statusCode === 403) {
                logger.warn(`> iTunes - RES - Rate limit error`, { transactionId, statusCode, body });
                await this.sleep(5000);
                return this.search(transactionId, searchTerm);
            }

            logger.error(`> iTunes - RES - Error - ${statusCode}`, { transactionId, statusCode, err });

            return [];
        }
    }

    private sleep = (t: number): Promise<void> => {
        return new Promise((resolve) => setTimeout(resolve, t));
    }
}

export default new ITunesService();