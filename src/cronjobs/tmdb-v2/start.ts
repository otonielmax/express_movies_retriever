import * as config from 'config';

import dbPool from '../../database/pool';
import * as healthCheck from '../../helpers/healthCheck';
import { logger } from '../../logger';
import genreManager from './managers/Genre';
import { syncMovies } from './movies';
import { syncSeries } from './series';

const transactionId = 'cronjob-tmdb-v2';

const run = async () => {
    try {
        logger.info('Starting job TMDB-V2', { transactionId });

        await genreManager.syncAll(transactionId);
        
        logger.info('Processing movies', { transactionId });
        await syncMovies(transactionId);

        logger.info('Processing series', { transactionId });
        await syncSeries(transactionId);

        await healthCheck(transactionId, config.HC_MOVIES_RETRIEVER);

        logger.info('Job executed successfully', { transactionId });
    } catch (err) {
        logger.error('Error executing cronjob', { transactionId, err });
    }

    await dbPool.endPool();
};

run();