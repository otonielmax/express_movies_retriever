'use strict';

const logger = require('@smartadtags/logger');
const async = require('async');
const config = require('config');
const moment = require('moment');
const request = require('request');

const getWebsitesConfig = require('../../helpers/getWebsitesConfig');
const healthCheck = require('../../helpers/healthCheck');
const dbService = require('../../services/dbPromises');
const {
    getLumenNotice,
    insertDmcaNotice,
    insertDmcaPrincipalSender,
    insertDmcaRecipient,
    insertDmcaSender
} = require('./managers/dmca');
const { sendNotice } = require('./managers/bot');
const { getProxyConfig } = require('../../helpers/proxy');


// Configurar logger
logger.config({
    environment: config.LOGGER_ENV,
    service: 'movies-retriever',
    logLevel: config.LOGGER_LOG_LEVEL,
    format: config.LOGGER_FORMAT
});
const transactionId = 'cronjob-lumen-retrieve-notices';
let websites = [];

/**
 * Inserta las notice en la base de datos cuando es necesario.
 * Estas notices no poseen el listado de URLs denunciadas ya que Lumen no lo entrega.
 * Para obtener el listado de URLS, enviamos la denuncia a un bot y posteriormente nos va a llegar el acceso
 * por email, el cual procesara otro job.
 * 
 * Este metodo solo se encarga de insertar parcialmente los notice y enviarlos al bot.
 * 
 * @param {*} notice 
 */
const insertComplaint = async (notice) => {
    const lumenNoticeId = notice.lumen_id;
    logger.info(`Procesar denuncia "${notice.lumen_id}"`, {
        transactionId,
        lumenNoticeId,
        notice
    });

    let dbConnection;
    try {
        // Comprobar que no exista la denuncia
        const foundNotice = await getLumenNotice(null, notice.lumen_id);

        // Obtener conexion e iniciar transaccion
        logger.info('Obtener conexion e iniciar transaccion', {
            transactionId,
            lumenNoticeId
        });
        dbConnection = await dbService.getConnection();
        await dbConnection.beginTransaction();

        let noticeId;
        // Si la denuncia no existe, crea todas las relaciones
        if (!foundNotice) {
            let recipient;
            let sender;
            let principalSender;
            if (notice.recipient_name) {
                // Insertar recipient
                logger.info(`Insertar/obtener recipient "${notice.recipient_name}"`, {
                    transactionId,
                    lumenNoticeId
                });
                recipient = await insertDmcaRecipient(dbConnection, notice.recipient_name);
            }
            if (notice.sender_name) {
                // Insertar sender
                logger.info(`Insertar/obtener sender "${notice.sender_name}"`, {
                    transactionId,
                    lumenNoticeId
                });
                sender = await insertDmcaSender(dbConnection, notice.sender_name);
            }
            if (notice.principal_name) {
                // Insertar principal sender
                logger.info(`Insertar/obtener principal sender "${notice.principal_name}"`, {
                    transactionId,
                    lumenNoticeId
                });
                principalSender = await insertDmcaPrincipalSender(dbConnection, notice.principal_name);
            }

            // Insertar notice
            const insertNotice = {
                lumen_id: notice.lumen_id,
                title: notice.title,
                dmca_sender_id: sender ? sender.id : null,
                dmca_principal_sender_id: principalSender ? principalSender.id : null,
                dmca_recipient_id: recipient ? recipient.id : null,
                tries: 1,
                last_try_at: new Date(),
                created_at: new Date(),
                lumen_sent_at: notice.date_sent,
                lumen_received_at: notice.date_received
            };
            logger.info(`Insertar denuncia "${lumenNoticeId}"`, {
                transactionId,
                lumenNoticeId,
                insertNotice
            });
            noticeId = await insertDmcaNotice(dbConnection, insertNotice);

            // Enviar notice al bot
            await sendNotice(transactionId, lumenNoticeId);
        } else {
            logger.info(`La denuncia "${lumenNoticeId}" ya se encuentra en la DB`, {
                transactionId,
                lumenNoticeId
            });
        }

        logger.info('Commit y liberar conexion', {
            transactionId,
            lumenNoticeId,
            noticeId
        });
        await dbConnection.commit();
        dbConnection.release();

        return true;
    } catch (err) {
        logger.error(`Error al procesar denuncia "${lumenNoticeId}"`, {
            transactionId,
            lumenNoticeId,
            err
        });

        if (dbConnection) {
            logger.info('Rollback y liberar conexion', {
                transactionId,
                lumenNoticeId
            });
            await dbConnection.rollback();
            dbConnection.release();
        }

        throw err;
    }
};

/**
 * Obtiene una determinada pagina de Lumen Database y guarda las notices que sean necesarias.
 * @param {Object} website  - Objeto de configuracion del sitio 
 * @param {Number} pageId   - Numero de pagina a procesar
 */
const processWebsitePage = (website, pageId, callback) => {
    const websiteId = website.id;
    let morePages = false;
    async.waterfall([
        // Buscar en la API de LumenDatabase
        (cb) => {
            const startDate = moment().subtract(Number(config.LUMEN_HOURS_FROM), 'hours').format('x');
            const endDate = moment().subtract(Number(config.LUMEN_HOURS_TO), 'hours').format('x');
            const pageLimit = 25;

            const options = {
                url: 'https://Lumendatabase.org/notices/search',
                qs: {
                    'term': website.host,
                    'term-require-all': 'true',
                    'date_received_facet': `${startDate}..${endDate}`,
                    'per_page': pageLimit,
                    'page': pageId,
                },
                json: true,
                timeout: Number(config.LUMEN_REQUEST_TIMEOUT)
            };

            // Reintentar 5 veces si hay errores
            async.retry(Number(config.LUMEN_REQUEST_RETRY_TIMES), (cb) => {
                logger.info(`Request a LUMENDATABASE.ORG - Pagina "${pageId}"`, {
                    transactionId,
                    pageId,
                    websiteId,
                    options
                });

                // Setear proxy, se hace aca para usar uno distinto en cada request
                options.proxy = getProxyConfig();

                // Realizar el request a traves de proxy ya que la API esta muy limitada.
                request.get(options, (err, result, body) => {
                    if (err) {
                        logger.error(`Request a LUMENDATABASE.ORG - Pagina "${pageId}"`, {
                            transactionId,
                            pageId,
                            websiteId,
                            proxy: options.proxy,
                            err
                        });
                        return cb(err);
                    }

                    const notices = body.notices;

                    if (!notices) {
                        logger.error('Error en el formato de respuesta de Lumen', {
                            transactionId,
                            websiteId,
                            options
                        });
                        return cb(new Error('Error fetching notices'));
                    }

                    return cb(null, notices);
                });
            }, (err, notices) => {
                // Si hay error, corta
                if (err) {
                    return cb(err);
                }

                // Si la cantidad obtenida es igual a la pedida por paginacion, quiere decir que hay mas paginas.
                if (notices.length === pageLimit) {
                    morePages = true;
                }

                logger.info(`Denuncias obtenidas. notices="${notices.length}", morePages="${morePages}"`, {
                    transactionId,
                    websiteId
                });

                // Sino, directamente termina
                return cb(null, notices);
            });
        },
        // Parsear las denuncias
        (notices, cb) => {
            if (!notices.length) {
                return cb(null, notices);
            }

            logger.info(`Parsear denuncias pagina "${pageId}"`, {
                transactionId,
                pageId,
                websiteId
            });

            let parsedNotices = [];
            for (const notice of notices) {
                parsedNotices.push({
                    lumen_id: notice.id,
                    title: notice.title,
                    date_sent: notice.date_sent,
                    date_received: notice.date_received,
                    sender_name: notice.sender_name,
                    principal_name: notice.principal_name,
                    recipient_name: notice.recipient_name
                });
            }

            return cb(null, parsedNotices);
        },
        // Insertar las denuncias en la DB
        (notices, cb) => {
            async.mapSeries(notices, insertComplaint, (err, result) => {
                return cb(err, result);
            });
        }
    ], (err) => {
        if (err) {
            logger.error(`Error al procesar pagina "${pageId}"`, {
                transactionId,
                websiteId,
                err
            });
            return callback(err);
        }

        if (morePages) {
            return processWebsitePage(website, pageId + 1, callback);
        }

        return callback();
    });
};

/**
 * Procesa el sitio, obteniendo todas las notices del mismo y guardandolas.
 * @param {Object} website - Objeto de configuracion del sitio 
 */
const processWebsite = (website, callback) => {
    logger.info(`Procesando website "${website.name}"`, {
        transactionId,
        websiteId: website.id
    });
    return processWebsitePage(website, 1, callback);
};

/**
 * Start job
 */
const runJob = () => {
    logger.info('Iniciando cron job', {
        transactionId
    });

    websites = getWebsitesConfig();

    // Procesar cada sitio
    async.mapSeries(websites, processWebsite, (err) => {
        dbService.endPool();
        if (err) {
            logger.error('Error al ejecutar cron', {
                transactionId,
                err
            });
        } else {
            healthCheck(transactionId, config.HC_LUMEN_RETRIEVE_NOTICES).then(() => {
                logger.info('Cron ejecutado con exito', {
                    transactionId
                });
            });
        }
    });
};
runJob();