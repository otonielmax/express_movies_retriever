'use strict';

const logger = require('@smartadtags/logger');
const config = require('config');
const moment = require('moment');

const healthCheck = require('../../helpers/healthCheck');
const dbService = require('../../services/dbPromises');
const { updateNotice } = require('./managers/dmca');
const { sendNotice } = require('./managers/bot');

// Configurar logger
logger.config({
    environment: config.LOGGER_ENV,
    service: 'movies-retriever',
    logLevel: config.LOGGER_LOG_LEVEL,
    format: config.LOGGER_FORMAT
});
const transactionId = 'cronjob-lumen-resend-notices';

const resendNotice = async (notice) => {
    const lumenNoticeId = notice.lumen_id;
    logger.info(`Reenviando notice "${lumenNoticeId}" al bot`, {
        transactionId,
        notice
    });

    await sendNotice(transactionId, lumenNoticeId);

    // Update notice
    logger.info(`Actualizando notice "${lumenNoticeId}" en la DB`, {
        transactionId
    });

    const updateParams = {
        tries: notice.tries + 1,
        last_try_at: new Date()
    };
    await updateNotice(null, notice.id, updateParams);
};

const run = async () => {
    logger.info('Comenzando job', {
        transactionId
    });

    try {
        const query = `
            SELECT
                id,
                lumen_id,
                tries
            FROM
                dmca_notice
            WHERE
                processed_at IS NULL AND
                tries < ? AND
                last_try_at <= ?
        `;
        const date = moment().subtract(2, 'hours').toDate();
        const [notices] = await dbService.query(query, [3, date]);
    
        logger.info(`Se encontraron "${notices.length}" para reenviar`, {
            transactionId
        });

        for (const notice of notices) {
            await resendNotice(notice);
        }

        // Enviar healthcheck
        await healthCheck(transactionId, config.HC_LUMEN_RESEND_NOTICES);
    } catch (err) {
        logger.error('Error al procesar job', {
            transactionId,
            err
        });
    }

    dbService.endPool();
};

run();