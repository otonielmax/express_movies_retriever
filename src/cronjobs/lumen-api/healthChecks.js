'use strict';
const logger = require('@smartadtags/logger');
const config = require('config');
const moment = require('moment');

const healthCheck = require('../../helpers/healthCheck');
const dbService = require('../../services/dbPromises');

// Configurar logger
logger.config({
    environment: config.LOGGER_ENV,
    service: 'movies-retriever',
    logLevel: config.LOGGER_LOG_LEVEL,
    format: config.LOGGER_FORMAT
});
const transactionId = 'cronjob-lumen-healthchecks';

/**
 * Comprueba que no haya notices sin procesar por mas de dos dias
 */
const checkPendingNotices = async () => {
    const query = `
        SELECT
            id,
            lumen_id,
            created_at
        FROM
            dmca_notice
        WHERE
            created_at <= ? AND processed_at IS NULL
    `;
    const date = moment().subtract(2, 'days').toDate();
    
    logger.info('Comprobando si hay denuncias sin procesar', {
        transactionId
    });
    
    const [notices] = await dbService.query(query, [date]);
    const failure = notices && notices.length;

    if (failure) {
        logger.error(`Se han encontrado "${notices.length}" denuncias sin procesar`, {
            transactionId,
            notices
        });
    } else {
        logger.info('No se han encontrado denuncias sin procesar', {
            transactionId
        });
    }

    // Send HealthCheck
    await healthCheck(transactionId, config.HC_LUMEN_CHECK_PENDING_NOTICES, failure);
};

/**
 * Comprueba que no haya URLS regeneradas que sin enviarse a Google por mas de un día
 */
const checkUnsentUrls = async () => {
    const query = `
        SELECT
            id,
            new_url,
            created_at
        FROM
            dmca_notice_url_result
        WHERE
            created_at <= ? AND status = 1
    `;
    const date = moment().subtract(1, 'days').toDate();

    logger.info('Comprobando si hay URLs sin procesar', {
        transactionId
    });
    
    const [urls] = await dbService.query(query, [date]);
    const failure = urls && urls.length;

    if (failure) {
        logger.error(`Se han encontrado "${urls.length}" URLs sin procesar`, {
            transactionId,
            urls
        });
    } else {
        logger.info('No se han encontrado URLs sin procesar', {
            transactionId
        });
    }

    // Send HealthCheck
    await healthCheck(transactionId, config.HC_LUMEN_CHECK_UNSENT_URLS, failure);
};

const runJob = async () => {
    try {
        await checkPendingNotices();
        await checkUnsentUrls();
    } catch (err) {
        logger.error('Error al procesar healthchecks', {
            transactionId,
            err
        });
    }

    await dbService.endPool();
};

runJob();