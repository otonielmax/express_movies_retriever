const logger = require('@smartadtags/logger');
const cheerio = require('cheerio');
const config = require('config');
const request = require('request-promise');

const { getProxyConfig } = require('../../../helpers/proxy');

/**
 * Realiza un request y reintenta una determinada cantidad de veces antes de tirar error
 * 
 * @param {Object} options  - Configuracion del request 
 * @param {Number} tries    - Cantidad de reintentos pendientes
 */
const requestRetry = async (transactionId, options, tries = 3) => {
    // Assign proxy
    options.proxy = getProxyConfig();

    logger.info(`Performing request to "${options.uri}"`, {
        transactionId,
        pendingTries: tries,
        proxy: options.proxy
    });

    try {
        const result = await request(options);
        return result;
    } catch (err) {
        logger.error('Error performing request', {
            transactionId,
            url: options.uri,
            proxy: options.proxy,
            err
        });

        if (tries && tries > 1) {
            return requestRetry(transactionId, options, tries - 1);
        }

        throw err;
    }
};

/**
 * Crawlea una noticia. 
 * Obtiene el HTML y usa Cheerio (simil jQuery) para obtener un listado de enlaces usando selectores.
 * 
 * @param {String} transactionId    - TransactionID del job
 * @param {Number} noticeId         - ID de denuncia de Lumen 
 * @param {String} accessToken      - Access token para poder acceder a datos sensibles 
 */
const processNotice = async (transactionId, noticeId, accessToken) => {
    const options = {
        uri: `http://lumendatabase.org/notices/${noticeId}?access_token=${accessToken}`,
        headers: {
            'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_14_4) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.157 Safari/537.36'
        },
        timeout: Number(config.LUMEN_REQUEST_TIMEOUT),
        transform: (body) => cheerio.load(body)
    };
    
    // Intentar el request varias veces
    const $ = await requestRetry(transactionId, options, Number(config.LUMEN_REQUEST_RETRY_TIMES));

    // Parsea el DOM e intenta agarrar el listado de URLs con un selector especial
    const elements = $('li.infringing_url').toArray();
    const urls = [];
    for (const el of elements) {
        if (el && el.children && el.children[0]) {
            urls.push(el.children[0].data);
        }
    }

    return urls;
};

module.exports = {
    processNotice
};
