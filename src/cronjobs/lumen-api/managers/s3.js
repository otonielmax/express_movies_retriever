'use strict';

const AWS = require('aws-sdk');
const config = require('config');

AWS.config.update({
    accessKeyId: config.AWS_ACCESS_KEY_ID,
    secretAccessKey: config.AWS_SECRET_ACCESS_KEY
});
const s3 = new AWS.S3({
    params: {
        Bucket: config.AWS_LUMEN_EMAILS_BUCKET
    }
});

const copyObject = (oldKey, newKey) => {
    return s3.copyObject({
        CopySource: `${config.AWS_LUMEN_EMAILS_BUCKET}/${oldKey}`,
        Key: newKey
    }).promise();
};

const deleteObject = (fileKey) => {
    const params = {
        Key: fileKey
    };
    return s3.deleteObject(params).promise();
};

const getObject = (fileKey) => {
    const params = {
        Key: fileKey
    };
    return s3.getObject(params).promise();
};

const listObjects = (params) => {
    return s3.listObjects(params).promise();
};

module.exports = {
    copyObject,
    deleteObject,
    getObject,
    listObjects
};