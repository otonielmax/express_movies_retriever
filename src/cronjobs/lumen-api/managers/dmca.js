'use strict';

const dbService = require('../../../services/dbPromises');

const insertDmcaRecipient = async (dbConnection, name) => {
    // Insertar sender si es necesario
    const query = `
        INSERT INTO
            dmca_recipient
        SET
            ?
        ON DUPLICATE KEY UPDATE
            ?
    `;
    const recipientInsert = {
        name,
        created_at: new Date(),
        last_seen_at: new Date()
    };
    const recipientUpdate = {
        last_seen_at: new Date()
    };
    
    await dbConnection.query(query, [recipientInsert, recipientUpdate]);

    // Obtener el recipiente para retornarlo
    const [recipients] = await dbConnection.query('SELECT * FROM dmca_recipient WHERE name = ?', [name]);

    return recipients[0];
};

const insertDmcaSender = async (dbConnection, name) => {
    // Insertar sender si es necesario
    const query = `
        INSERT INTO
            dmca_sender
        SET
            ?
        ON DUPLICATE KEY UPDATE
            ?
    `;
    const senderInsert = {
        name,
        created_at: new Date(),
        last_seen_at: new Date()
    };
    const senderUpdate = {
        last_seen_at: new Date()
    };
    
    await dbConnection.query(query, [senderInsert, senderUpdate]);

    // Obtener el sender para retornarlo
    const [senders] = await dbConnection.query('SELECT * FROM dmca_sender WHERE name = ?', [name]);

    return senders[0];
};

const insertDmcaPrincipalSender = async (dbConnection, name) => {
    // Insertar sender si es necesario
    const query = `
        INSERT INTO
            dmca_principal_sender
        SET
            ?
        ON DUPLICATE KEY UPDATE
            ?
    `;
    const senderInsert = {
        name,
        created_at: new Date(),
        last_seen_at: new Date()
    };
    const senderUpdate = {
        last_seen_at: new Date()
    };
    
    await dbConnection.query(query, [senderInsert, senderUpdate]);

    // Obtener el sender para retornarlo
    const [senders] = await dbConnection.query('SELECT * FROM dmca_principal_sender WHERE name = ?', [name]);

    return senders[0];
};

const insertDmcaNotice = async (dbConnection, notice) => {
    const query = 'INSERT INTO dmca_notice SET ?';
    const [result] = await dbConnection.query(query, [notice]);

    return result.insertId;
};

const findDmcaNoticeUrl = async (dbConnection, noticeId, url) => {
    const connection = dbConnection || dbService;
    const query = `
        SELECT
            *
        FROM
            dmca_notice_url
        WHERE
            dmca_notice_id = ? AND
            url = ?
    `;

    const [found] = await connection.query(query, [noticeId, url.url]);
    return found[0];
};

const insertDmcaNoticeUrl = async (dbConnection, noticeId, url) => {
    const connection = dbConnection || dbService;

    // Verificar que ya no exista
    const urlFound = await findDmcaNoticeUrl(connection, noticeId, url);
    if (urlFound) {
        return false;
    }

    const query = 'INSERT INTO dmca_notice_url SET ?';
    const data = {
        url: url.url,
        dmca_notice_id: noticeId,
        website_id: url.website_id
    };
    await connection.query(query, [data]);

    return true;
};

const updateNotice = async (dbConnection, noticeId, data) => {
    const connection = dbConnection || dbService;
    const query = `
        UPDATE
            dmca_notice
        SET
            ?
        WHERE
            id = ?
    `;
    await connection.query(query, [data, noticeId]);
};

const getNoticeUrls = async (dbConnection, noticeId) => {
    const query = `
        SELECT
            dnu.id,
            dnu.url
        FROM
            dmca_notice_url dnu
        JOIN
            dmca_notice dn ON dnu.dmca_notice_id=dn.id
        WHERE
            dn.id = ? 
    `;

    const [foundNotices] = await dbConnection.query(query, [noticeId]);

    return foundNotices;
};

const getLumenNotice = async (dbConnection, lumenId) => {
    const connection = dbConnection || dbService;

    const query = 'SELECT * FROM dmca_notice WHERE lumen_id=?';
    const [found] = await connection.query(query, [lumenId]);

    return found[0];
};

module.exports = {
    getLumenNotice,
    getNoticeUrls,
    insertDmcaNotice,
    insertDmcaNoticeUrl,
    insertDmcaPrincipalSender,
    insertDmcaRecipient,
    insertDmcaSender,
    updateNotice
};