'use strict';

const logger = require('@smartadtags/logger');
const request = require('request-promise');

const sendNotice = async (transactionId, noticeId) => {
    logger.info(`Enviando notice "${noticeId}" al bot`, {
        transactionId
    });
    
    // Enviar denuncia al Bot para que las procese
    const requestParams = {
        method: 'POST',
        url: 'http://api.apiworkers.com:2350/executeBot',
        qs: {
            api_key: 'e16fe0ea211554f35e5c0a2bac70cbd3',
            data: JSON.stringify([{
                bot_id: 6,
                parameters: {
                    request_id: noticeId,
                    domain_email: 'users2cash.com'
                }
            }])
        },
        timeout: 15000
    };
    
    try {
        await request(requestParams);
    } catch (err) {
        logger.error(`Error enviando notice "${noticeId}" al bot`, {
            transactionId,
            noticeId,
            requestParams,
            err
        });
        throw err;
    }
};

module.exports = {
    sendNotice
};