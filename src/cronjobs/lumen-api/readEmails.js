'use strict';
const logger = require('@smartadtags/logger');
const { mapLimit } = require('async');
const config = require('config');

const getWebsitesConfig = require('../../helpers/getWebsitesConfig');
const healtCheck = require('../../helpers/healthCheck');
const s3Manager = require('./managers/s3');
const {
    getLumenNotice,
    insertDmcaNoticeUrl,
    updateNotice
} = require('./managers/dmca');
const crawler = require('./managers/crawler');
const dbService = require('../../services/dbPromises');

// Configurar logger
// Configurar logger
logger.config({
    environment: config.LOGGER_ENV,
    service: 'movies-retriever',
    logLevel: config.LOGGER_LOG_LEVEL,
    format: config.LOGGER_FORMAT
});
const transactionId = 'cronjob-lumen-read-emails';
let websites = [];

// Variable global para controlar cuantas URLs se encontraron
let newUrlsFound = 0;

/**
 * Procesa cada correo. Descarga el archivo de S3 y busca la denuncia y su access token en el contenido.
 * En caso de encontrarlo, crawlea el sitio y obtiene el listado de URLs, y las guarda.
 * Al finalizar satisfactoriamente siempre borra el archivo de S3 para que no vuelva a ser procesado.
 * 
 * @param {Object} file     - Objeto de archivo entregado por S3 
 */
const processEmail = async (file) => {
    const fileKey = file.Key;
    const parsedFileKey = file.Key.replace('unread/', '');
    logger.info(`Procesando archivo "${fileKey}"`, {
        transactionId
    });

    // Obtener contenido del archivo de S3 y transformarlo de buffer a texto
    const s3Response = await s3Manager.getObject(fileKey);
    const fileContent = Buffer.from(s3Response.Body).toString('utf8');

    logger.info(`Archivo "${fileKey}" obtenido`, {
        transactionId,
        fileContent
    });

    let success = false;
    // Las URL de lumen son del siguiente estilo:
    // http://lumendatabase.org/notices/18535098?access_token=mvRnKDQm6o3H9yPhyDXrtQ
    // Ejecuto el regex y compruebo si obtuvo todas las variables
    const regex = /http:\/\/lumendatabase.org\/notices\/(.*)\?access_token=(.*)\./g;
    const result = regex.exec(fileContent);
    if (result && result[2]) {
        const lumenNoticeId = result[1];
        const accessToken = result[2];
        const notice = await getLumenNotice(null, lumenNoticeId);

        if (notice && !notice.processed_at) {
            success = true;
            logger.info(`Crawling notice "${lumenNoticeId}" using access token "${accessToken}"`, {
                transactionId,
                lumenNoticeId,
                accessToken
            });

            // Crawlear la notice y extraer las URL del HTML
            const urls = await crawler.processNotice(transactionId, lumenNoticeId, accessToken);

            logger.info(`Notice "${lumenNoticeId} crawleada. Procesando resultados"`, {
                transactionId,
                lumenNoticeId,
                urls
            });

            // De las URLs encontradas, buscar las que pertenezcan a cada sitio
            const parsedUrls = [];
            for (const url of urls) {
                // Busca algun dominio que matchee con la URL de la denuncia
                const website = websites.find((w) => {
                    const match = w.domains.find((domain) => url.indexOf(domain) !== -1);
                    return match ? true : false;
                });
                if (website) {
                    parsedUrls.push({
                        url,
                        website_id: website.id
                    });
                }
            }

            logger.info(`Parseadas URL de notice "${lumenNoticeId}"`, {
                transactionId,
                lumenNoticeId,
                parsedUrls
            });

            if (parsedUrls.length) {
                logger.info(`Insertando URLS de notice "${lumenNoticeId}"`, {
                    transactionId,
                    lumenNoticeId,
                    parsedUrls
                });
                for (const url of parsedUrls) {
                    const success = await insertDmcaNoticeUrl(null, notice.id, url);
                    if (success) {
                        logger.info(`URL "${url.url}" insertada`, {
                            transactionId,
                            lumenNoticeId,
                            url
                        });
                        newUrlsFound++;
                    }
                }
            } else {
                logger.error(`La notice "${lumenNoticeId}" no tenia URLs de ningun sitio. Posible error de parseo`, {
                    transactionId,
                    parsedUrls,
                    urls
                });
            }

            logger.info(`Marcando notice "${lumenNoticeId}" como procesada`, {
                transactionId,
                lumenNoticeId,
                noticeId: notice.id
            });

            const updateData = {
                access_token: accessToken,
                processed_at: new Date()
            };
            await updateNotice(null, notice.id, updateData);
        } else if (notice && notice.processed_at) {
            logger.info(`Notice "${lumenNoticeId}" ya ha sido procesada anteriormente. No es necesario volver a hacerlo`, {
                transactionId,
                notice
            });
        }
    } else {
        logger.info('Email invalido, no se puede parsear ningun contenido', {
            transactionId,
            fileContent
        });
    }

    const newKey = `${success ? 'success' : 'fail'}/${parsedFileKey}`;
    logger.info(`Renombrando el archivo "${fileKey}" de S3`, {
        transactionId,
        oldKey: fileKey,
        newKey
    });

    await s3Manager.copyObject(fileKey, newKey);
    await s3Manager.deleteObject(fileKey);
};

/**
 * Ejecuta el job.
 * Lee el listado de archivos del bucket de S3 y los procesa.
 */
const runJob = async () => {
    websites = getWebsitesConfig();

    try {
        logger.info('Obteniendo listado de archivos de S3', {
            transactionId
        });

        const params = {
            Prefix: 'unread/'
        };
        const fileList = await s3Manager.listObjects(params);

        if (fileList && fileList.Contents && fileList.Contents.length) {
            const files = fileList.Contents;
            logger.info(`Encontrados "${files.length}" archivos en S3`, {
                transactionId,
                files
            });

            // Correr de a 5 emails en paralelo
            await mapLimit(files, 5, processEmail);

            if (newUrlsFound > 0) {
                logger.info(`Se encontraron e insertaron "${newUrlsFound}" nuevas URLs`, { transactionId });
            }
        } else {
            logger.info('No se encontro ningun archivo en S3', {
                transactionId,
                fileList
            });
        }

        // Enviar healthcheck
        await healtCheck(transactionId, config.HC_LUMEN_READ_EMAILS);
    } catch (err) {
        logger.error('Error ejecutando job', {
            transactionId,
            err
        });
    }

    await dbService.endPool();
};

runJob();