'use strict';

const logger = require('@smartadtags/logger');
const async = require('async');
const config = require('config');

const dbService = require('../../services/db');
const getWebsitesConfig = require('../../helpers/getWebsitesConfig');
const healthCheck = require('../../helpers/healthCheck');
const languageHelper = require('../../helpers/language');
const spinners = require('./spinners');

logger.config({
    environment: config.LOGGER_ENV,
    service: 'movies-retriever',
    logLevel: config.LOGGER_LOG_LEVEL,
    format: config.LOGGER_FORMAT
});
const transactionId = 'cronjob-spinner';

const processMovies = (website, language, callback) => {
    const defaultLanguage = website.language.available.find((l) => l.default === true);
    let amountSpinned = 0;

    const processPage = (page) => {
        logger.info(`[${website.name}] Procesando pagina ${page}`, { transactionId });

        async.waterfall([
            // Obtener las pelis por ID para optimizar
            (cb) => {
                const limit = 100;
                const offset = (page - 1) * limit;
                const query = `
                    SELECT
                        id
                    FROM
                        movie m
                    WHERE
                        m.popularity >= ?
                    ORDER BY
                        m.popularity DESC
                    LIMIT
                        ?
                    OFFSET
                        ?
                `;

                const minPopularity = Number(config.MOVIES_MIN_POPULARITY) * 3;
                dbService.query(query, [minPopularity, limit, offset], (err, movies) => {
                    if (err) {
                        return cb(err);
                    }

                    if (!movies || !movies.length) {
                        // Tiene que cortar
                        logger.info(`[${website.name}] No hay mas películas`, { transactionId });
                        return cb({
                            finished: true,
                        });
                    }

                    const movieIds = movies.map((m) => m.id);

                    return cb(null, movieIds);
                });
            },
            (movieIds, cb) => {
                const query = `
                    SELECT
                        m.id movie_id,
                        IF(ml_1.overview IS NOT NULL, ml_1.id, IF(ml_2.overview IS NOT NULL, ml_2.id, IF(ml_3.overview IS NOT NULL, ml_3.id, ml_4.id))) movie_language_id
                    FROM
                        movie m
                    -- Languages
                    LEFT JOIN
                        movie_language ml_1 ON (m.id=ml_1.movie_id AND ml_1.language_locale_id=?)
                    LEFT JOIN
                        movie_language_website mlw_1 ON (ml_1.id=mlw_1.movie_language_id AND mlw_1.website_id=?)
                    LEFT JOIN
                        movie_language ml_2 ON (m.id=ml_2.movie_id AND ml_2.language_id=?)
                    LEFT JOIN
                        movie_language_website mlw_2 ON (ml_2.id=mlw_2.movie_language_id AND mlw_2.website_id=?)
                    LEFT JOIN
                        movie_language ml_3 ON (m.id=ml_3.movie_id AND ml_3.language_id=?)
                    LEFT JOIN
                        movie_language_website mlw_3 ON (ml_3.id=mlw_3.movie_language_id AND mlw_3.website_id=?)
                    LEFT JOIN
                        movie_language ml_4 ON (m.id=ml_4.movie_id AND ml_4.language_id=?)
                    LEFT JOIN
                        movie_language_website mlw_4 ON (ml_4.id=mlw_4.movie_language_id AND mlw_4.website_id=?)
                    WHERE
                        m.id IN (?)
                    GROUP BY
                        m.id
                    ORDER BY
                        m.popularity DESC
                `;

                const params = [
                    language.language_locale_id,
                    website.id,
                    language.language_id,
                    website.id,
                    defaultLanguage.language_id,
                    website.id,
                    102,
                    website.id,
                    movieIds
                ];

                dbService.query(query, params, (err, movies) => {
                    if (err) {
                        return cb(err);
                    }

                    return cb(null, movies);
                });
            },
            // Filtrar solo las que haga falta spinear
            (movies, cb) => {
                if (!movies.length) {
                    return cb(null, []);
                }

                const movieLanguageIds = movies.map((m) => m.movie_language_id);

                const query = `
                    SELECT
                        ml.movie_id,
                        m.title,
                        ml.id movie_language_id,
                        ml.language_id,
                        IFNULL(ml.overview, m.overview) overview
                    FROM
                        movie_language ml
                    JOIN
                        movie m ON m.id=ml.movie_id
                    LEFT JOIN
                        movie_language_website mlw ON (ml.id=mlw.movie_language_id AND mlw.website_id=?)
                    WHERE
                        ml.id IN (?) AND
                        mlw.overview IS NULL
                    ORDER BY
                        m.popularity DESC
                `;

                dbService.query(query, [website.id, movieLanguageIds], (err, missingMovies) => {
                    if (err) {
                        return cb(err);
                    }

                    return cb(null, missingMovies);
                });
            },
            (movies, cb) => {
                amountSpinned += movies.length;

                async.mapLimit(movies, 4, (movie, cb) => {
                    processMovie(website, movie, (err) => {
                        return cb(err);
                    });
                }, (err) => {
                    if (err) {
                        return cb(err);
                    }

                    // Cortar un maximo de 50 peliculas por sitio, por ejecucion.
                    if (amountSpinned >= 50) {
                        logger.info(`[${website.name}] Ya se spinearon mas de 50 peliculas`, { transactionId });
                        return cb({
                            finished: true,
                        });
                    }

                    return cb();
                });
            },
        ], (err) => {
            if (err) {
                // Ya no hay mas peliculas
                if (err.finished) {
                    return callback(null);
                }

                // Corta con error
                return callback(err);
            }

            // Procesa mas peliculas
            processPage(page + 1);
        });
    };
    processPage(1);
};

const processMovie = (website, movie, callback) => {
    async.waterfall([
        (cb) => {
            const languageIso = languageHelper.getLanguage(movie.language_id).iso;
            const spinner = spinners['spinbot'];
            logger.info(`[${website.name}] Processing movie "${movie.title}" - ${languageIso}`, { transactionId });
            spinner.spin(transactionId, movie.overview, languageIso, (err, spinnedOverview) => {
                if (err) {
                    // Si no trae el spin, que no lo guarde pero que no corte.
                    logger.error(`[${website.name}] Error generando spin de sinopsis`, { transactionId, err, movieId: movie.movie_id, languageIso });
                    return cb(null, null);
                }

                return cb(null, spinnedOverview);
            });
        },
        (overview, cb) => {
            if (!overview) {
                return cb(null, overview);
            }

            const data = {
                overview: overview.trim(),
                movie_language_id: movie.movie_language_id,
                website_id: website.id,
            };

            const query = `
                INSERT INTO movie_language_website SET ?
            `;

            dbService.query(query, [data], (err) => {
                if (err) {
                    return cb(err);
                }

                return cb(null);
            });
        }
    ], (err) => {
        return callback(err);
    });
};

const processWebsite = (website, callback) => {
    async.mapSeries(website.language.available, (language, cb) => {
        processMovies(website, language, (err) => {
            return cb(err);
        });
    }, (err) => {
        return callback(err);
    });
};

async.waterfall([
    (cb) => {
        // Procesar websites
        const websites = getWebsitesConfig();
        return cb(null, websites);
    },
    // Get languages
    (websites, cb) => {
        languageHelper.retrieve((err) => {
            if (err) {
                return cb(err);
            }

            return cb(null, websites);
        });
    },
    (websites, cb) => {
        async.mapSeries(websites, (website, cb) => {
            processWebsite(website, (err) => {
                return cb(err);
            });
        }, (err) => {
            return cb(err);
        });
    },
], (err) => {
    if (err) {
        logger.error('Error executing cronjob', { transactionId, err });
        dbService.endPool();
    } else {
        logger.info('Job executed successfully', { transactionId });
        healthCheck(transactionId, config.HC_SPINNER).then(() => {
            dbService.endPool();
        });
    }

});