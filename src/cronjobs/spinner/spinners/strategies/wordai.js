'use strict';

const logger = require('@smartadtags/logger');
const request = require('request');

const Languages = {
    en: 'en',
    es: 'es',
    it: 'it',
    fr: 'fr'
};

exports.spin = (transactionId, text, language, callback) => {
    const actualLanguage = Languages[language];
    if (!actualLanguage) {
        return callback(new Error('Invalid language'));
    }

    logger.info('< WordAI - REQ', { transactionId, options });
    const options = {
        url: 'http://wordai.com/users/turing-api.php',
        form: {
            email: 'marc@valiranetwork.com',
            pass: 'Ayelen1997!',
            s: text,
            quality: 'Unique',
            lang: actualLanguage,
            sentence: 'on',
            output: 'json',
            returnspin: 'true'
        },
        json: true
    };


    request.post(options, (err, response, body) => {
        if (err) {
            logger.error('> WordAI - RES - Error', { transactionId, err });
            return callback(err);
        }

        if (!response || response.statusCode !== 200) {
            const statusCode = response ? response.statusCode : 'unknown';
            logger.error(`> WordAI - RES - invalid statusCode "${statusCode}"`, { transactionId, body });
            return callback(new Error('Invalid response'));
        }

        if (!body || !body.text || !body.text.length) {
            logger.error('> WordAI - RES - Empty body', { transactionId });
            return callback(new Error('Invalid spin'));
        }

        logger.info('< WordAI - RES - Success', { transactionId, body, statusCode: response.statusCode });

        return callback(null, body.text);
    });
};