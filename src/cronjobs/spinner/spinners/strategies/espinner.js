'use strict';

const request = require('request');

exports.spin = (transactionId, text, callback) => {
    const options = {
        url: 'http://espinner.net/app/api/spinner',
        body: {
            email: 'marc@valiranetwork.com',
            apikey: '3780aac5b54137d1d1fbe1a998291a9ea02cea57',
            delete_frases: '1',
            content: text,
        },
        json: true,
    };

    request.post(options, (err, response, body) => {
        if (err) {
            return callback(err);
        }

        if (!response || response.statusCode !== 200) {
            return callback(new Error('Invalid response'));
        }

        if (!body || !body.spin_unique || !body.spin_unique.length) {
            return callback(new Error('Invalid spin'));
        }

        return callback(null, body.spin_unique);
    });
};