'use strict';

const logger = require('@smartadtags/logger');
const request = require('request');

const { getProxyConfig } = require('../../../../helpers/proxy');

const Languages = {
    es: 'sp',
    en: 'en',
    fr: 'fr'
};

exports.spin = (transactionId, text, language, callback) => {
    const actualLanguage = Languages[language];

    if (!actualLanguage) {
        return callback(new Error('Invalid language'));
    }

    const options = {
        url: 'https://spinbot.info/php/process.php',
        form: {
            data: text,
            lang: actualLanguage,
        },
        proxy: getProxyConfig(),
        timeout: 30000
    };

    logger.info('< SPINBOT - REQ', { transactionId, options });
    request.post(options, (err, response, body) => {
        if (err) {
            logger.error('> SPINBOT - RES - Error', { transactionId, err });
            return callback(err);
        }

        if (!response || response.statusCode !== 200) {
            const statusCode = response ? response.statusCode : 'unknown';
            logger.error(`> SPINBOT - RES - invalid statusCode "${statusCode}"`, { transactionId, body });
            return callback(new Error('Invalid response'));
        }

        if (!body || !body.length) {
            logger.error('> SPINBOT - RES - Empty body', { transactionId });
            return callback(new Error('Invalid spin'));
        }

        logger.info('< SPINBOT - RES - Success', { transactionId, body, statusCode: response.statusCode });

        return callback(null, body);
    });
};