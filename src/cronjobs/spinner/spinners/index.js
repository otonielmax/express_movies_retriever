'use strict';

module.exports = {
    espinner: require('./strategies/espinner'),
    wordai: require('./strategies/wordai'),
    spinbot: require('./strategies/spinbot'),
};