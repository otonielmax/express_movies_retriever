
import dbPool from '../../../database/pool';
import { logger } from '../../../logger';
import { IMovie, ISerie } from '../interfaces';

interface IArticleEvent {
    data: IMovie | ISerie;
    website: any;
}

export class ArticleManager {
    private transactionId: string;

    constructor (transactionId: string) {
        this.transactionId = transactionId;
    }

    public async save (event: IArticleEvent): Promise<void> {
        const { lang, article } = event.data;
        const data = {
            website_id: event.website.id,
            language_id: lang.id,
            content: article?.text,
            created_at: new Date()
        };

        // @todo Contemplar series
        if ('movieId' in event.data) {
            // tslint:disable-next-line: no-string-literal
            data['movie_id'] = event.data.movieId;
        }

        logger.silly('Inserting new article', {
            transactionId: this.transactionId,
            data
        });

        try {
            await dbPool.query('INSERT INTO article SET ?', [data]);
        } catch (err) {
            logger.error('Error inserting new article', {
                transactionId: this.transactionId,
                data,
                err
            });

            throw err;
        }
    }
}