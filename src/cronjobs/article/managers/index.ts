export * from './ArticleManager';
export * from './CrawlerManager';
export * from './LanguageManager';
export * from './MovieManager';
export * from './SerieManager';