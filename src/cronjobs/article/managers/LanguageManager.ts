import * as squel from 'squel';
import dbPool from '../../../database/pool';
import { logger } from '../../../logger';
import { ILanguage } from '../interfaces';

export class LanguageManager {
    private transactionId: string;

    constructor (transactionId: string) {
        this.transactionId = transactionId;
    }

    public async getAllByIso (iso: string[]): Promise<ILanguage[]> {
        const query = squel.select()
            .field('id')
            .field('name')
            .field('iso')
            .from('language')
            .where('iso IN ?', iso);
        
        logger.silly('Searching languages', {
            transactionId: this.transactionId,
            query: query.toString()
        });

        const preparedQuery = query.toParam();
        const [ rows ] = await dbPool.query(preparedQuery.text, preparedQuery.values);

        if (!rows || !rows.length) {
            return [];
        }

        return rows;
    }
}