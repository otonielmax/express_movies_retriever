import * as squel from 'squel';
import dbPool from '../../../database/pool';
import { logger } from '../../../logger';
import { ISerie } from '../interfaces';

export class SerieManager {
    private transactionId: string;

    constructor (transactionId: string) {
        this.transactionId = transactionId;
    }

    public async getAll (): Promise<ISerie[]> {
        const query = squel.select()
            .field('id')
            .field('title')
            .from('serie')
            .order('gallery_popular_position')
            .where(
                squel.expr()
                    .or('gallery_popular_position IS NOT NULL')
                    .or('gallery_top_rated_position IS NOT NULL')
                    .or('gallery_on_air_position IS NOT NULL')
                    .or('gallery_airing_today_position IS NOT NULL')
                    .or('gallery_latest_position IS NOT NULL')
            )
            .limit(2);
        
        logger.silly('Searching series', {
            transactionId: this.transactionId,
            query: query.toString()
        });

        const preparedQuery = query.toParam();
        const [ rows ] = await dbPool.query(preparedQuery.text, preparedQuery.values);

        if (!rows || !rows.length) {
            return [];
        }

        return rows;
    }
}