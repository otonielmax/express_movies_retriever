
import * as config from 'config';
import { EventEmitter } from 'events';
import * as puppeteer from 'puppeteer';
import { logger } from '../../../logger';
import { ArticleForgeLengths, Events } from '../enums';
import { IMovie, ISerie } from '../interfaces';

export class CrawlerManager extends EventEmitter {
    private browser: any;
    private transactionId: string;

    constructor (transactionId: string) {
        super();
        this.transactionId = transactionId;
    }

    /**
     * Starts puppeteer instance and wait for records to be added
     */
    public async launch () {
        logger.info('Launching puppeteer', {
            transactionId: this.transactionId
        });

        try {
            this.browser = await puppeteer.launch({
                headless: config.ENVIRONMENT !== 'local' || config.PUPPETEER_HEADLESS === 'true' ? true : false,
                executablePath: config.ENVIRONMENT !== 'local' ? '/usr/bin/chromium-browser' : undefined,
                args: config.ENVIRONMENT !== 'local' ? ['--no-sandbox'] : undefined
            });
        } catch (err) {
            logger.error('Error starting crawler', {
                transactionId: this.transactionId,
                err
            });

            throw err;
        }
    }

    /**
     * Waits to puppeteer be idle and then closes it
     */
    public async close (): Promise<void> {
        logger.info('Closing puppeteer', {
            transactionId: this.transactionId
        });

        if (this.browser) {
            await this.browser.close();
        }
    }

    /**
     * Processes a given job to create a new article
     * @param jobData - Data contains puppeteer page instance, url, etc 
     */
    public async processJob (website: any, data: IMovie | ISerie): Promise<void> {
        logger.silly(`Processing new record: [${data.lang.name}] ${data.title}`, {
            transactionId: this.transactionId,
            data
        });

        try {
            const page = await this.browser.newPage();

            // Initial page config
            await page.setViewport({ width: 1366, height: 768 });

            await page.goto(`${config.ARTICLE_FORGE_URL}/new_article`, { waitUntil: 'load' });

            if (page.url() === `${config.ARTICLE_FORGE_URL}/users/sign_in`) {
                logger.debug(`Performing "log in" for [${data.lang.name}] ${data.title}`, {
                    transactionId: this.transactionId,
                    data
                });

                await this.login(page);
            }

            logger.info(`Creating article [${data.lang.name}] ${data.title}`, {
                transactionId: this.transactionId,
                data
            });

            await this.createArticle(website, page, data);
            await page.close();
        } catch (err) {
            logger.error('Error executing processing job', {
                transactionId: this.transactionId,
                data,
                err
            });

            throw err;
        }
    }

    /**
     * Performs user login the service
     * @param page - Puppeteer page instance
     */
    private async login (page): Promise<void> {
        // Is needed wait that inputs were rendered
        await page.waitForSelector('#user_email');
        await page.waitForSelector('#user_password');

        // Completes form and submits it
        await page.type('#user_email', config.ARTICLE_FORGE_USERNAME, { delay: 100 });
        await page.type('#user_password', config.ARTICLE_FORGE_PASSWORD , { delay: 175 });

        await page.waitFor(1200);

        await page.keyboard.press('Enter');
        
        // Login process doesn't finish while next page is not loaded
        await page.waitForNavigation({ waitUntil: 'load' });
    }

    /**
     * Creates new article of the given record
     * @param website - Website config 
     * @param page - Puppeteer page instance
     * @param data - Record data
     */
    private async createArticle (website: any, page, data: IMovie | ISerie): Promise<IMovie | ISerie | null> {
        let foundId: number|null = null;

        // Flag to don't keep waiting for the article infinitely
        let waitedTime: number = 0;

        // I know, this is fucking shit. I'll die to do this.. sorry dude
        const inspectRequest = async (response) => {
            // "get_batch_progress" is the response that contains minimal info about the article  
            if (response.url().includes('get_batch_progress')) {
                const payload: any = await response.json();
                
                if (payload && foundId && payload[foundId]) {
                    if (payload[foundId].status === 201) {
                        logger.debug(`Article id "${foundId}" done for [${data.lang.name}] ${data.title}`, {
                            transactionId: this.transactionId,
                            payload
                        });

                        data.article = {
                            id: foundId
                        };
                    } else {
                        logger.silly(`Article id "${foundId}" in progress "${payload[foundId].progress}"`, {
                            transactionId: this.transactionId,
                            payload
                        });
                    }
                }
            }

            // "check_ongoing" is the response that contains data about old articles
            if (response.url().includes('check_ongoing')) {
                const payload = await response.json();

                if (payload && payload.additional_info) {
                    const ids = Object.keys(payload.additional_info).map((i) => Number(i));
                    foundId = Math.max(...ids);

                    logger.debug(`Article id selected for [${data.lang.name}] ${data.title} is ${foundId}`, {
                        transactionId: this.transactionId,
                        payload
                    });
                }
            }
        };

        const keywords: string[] = [];
        const articleLength: string = Object.keys(ArticleForgeLengths)[config.ARTICLE_LENGTH] ? 
            config.ARTICLE_LENGTH : ArticleForgeLengths.short;

        // @todo Contemplar varios idiomas
        if ('serieId' in data) {
            keywords.push('Serie');
        } else {
            keywords.push('Movie');
        }

        // Each website config can set custom keywords for each language
        // @todo normalize keywords
        if (website.articles && website.articles.keywords) {
            const customKeywords: string[] = website.articles.keywords[data.lang.id] || website.articles.keywords.default || [];
            keywords.push(...customKeywords);

            logger.debug(`Adding custom keywords for [${data.lang.name}] ${data.title}`, { 
                transactionId: this.transactionId,
                keywords
            });
        }

        // Is needed wait that inputs were rendered
        await page.waitForSelector('#keyword');
        await page.waitForSelector('#select_length');
        await page.waitForSelector('#language');

        // completes the form
        await page.type('#keyword', this.normalizeTitle(data.title), { delay: 120 });
        await page.select('#select_length', articleLength);
        await page.select('#language', data.lang.iso);
        await page.type('#sub_keywords', keywords.join('\n'), { delay: 160 });

        await page.waitFor(1200);

        await page.click('#create_article_button');

        await page.on('response', inspectRequest);

        // As long the API doesn't send us an article id, we wait
        while (!data.article || !data.article.id) {
            logger.silly(`Waiting 500ms for API to send us article of [${data.lang.name}] ${data.title}`, { 
                transactionId: this.transactionId,
                waitedTime
            });

            waitedTime = waitedTime + Number(config.ARTICLE_WAIT_INTERVAL);
            await page.waitFor(Number(config.ARTICLE_WAIT_INTERVAL));
        }

        // If we exceed the maximum waiting time, we can assume that an error has occurred and the generation was aborted
        if (waitedTime >= Number(config.ARTICLE_MAX_WAIT_TIME)) {
            logger.warn(`Maximum waiting time exceeded for [${data.lang.name}] ${data.title}`, { 
                transactionId: this.transactionId,
                waitedTime,
                data
            });

            return null;
        }

        // Once we have the id, we expect the article to be completed
        await page.waitForSelector(`#hyperlink_${data.article.id}`);
        await page.click(`#hyperlink_${data.article.id}`);
        await page.waitForNavigation({ waitUntil: 'load' });
        
        page.removeListener('request', inspectRequest);

        // Gets article created and save it to the data object
        const element = await page.$('#spintax-area-pre');
        const text: string = await(await element.getProperty('textContent')).jsonValue();

        data.article.text = text;

        logger.info(`Article [${data.lang.name}] ${data.title} completed `, {
            transactionId: this.transactionId,
            data
        });

        this.emit(Events.articleCreated, {
            data,
            website
        });

        return data;
    }

    private normalizeTitle (title: string): string {
        let normalizedTitle = title;
        const whiteList: any = {
            a: /[àáâäãåā]/gi,
            ae: /[æ]/gi,
            c: /[çćč]/gi,
            e: /[èéêëēėę]/gi,
            i: /[ïïíīįî]/gi,
            l: /[ł]/gi,
            n: /[ñń]/gi,
            o: /[ôöòóøōõ]/gi,
            oe: /[œ]/gi,
            s: /[śš]/gi,
            ss: /[ß]/gi,
            u: /[ûüùúū]/gi,
            y: /[ÿ]/gi,
            z: /[žźż]/
        };

        Object.keys(whiteList).forEach((key) => {
            const regex: RegExp = whiteList[key];
            normalizedTitle = normalizedTitle.replace(regex, key);
        });

        return normalizedTitle;
    }
}