import * as squel from 'squel';
import dbPool from '../../../database/pool';
import { logger } from '../../../logger';
import { ILanguage, IMovie } from '../interfaces';

export class MovieManager {
    private transactionId: string;
    private movies: IMovie[] = [];
    private website: any = {};
    private routes: any[] = [{
        name: 'home',
        gallery: 'gallery_popular_position'
    }, {
        name: 'movies_upcoming',
        gallery: 'gallery_upcoming_position',
    }, {
        name: 'movies_top_rated',
        gallery: 'gallery_top_rated_position'
    }, {
        name: 'movies_now_playing',
        gallery: 'gallery_now_playing_position'
    }];

    constructor (website: any, transactionId: string) {
        this.website = website;
        this.transactionId = transactionId;
    }

    /**
     * Gets all pending movies for specific website to create articles
     * @param website - Website config
     * @param languages - Available languages to use
     */
    public async getAll (languages: ILanguage[]): Promise<IMovie[]> {
        // From available languages, we only use configured website languages
        const websiteLangs = languages.filter((language) => {
            return this.website.language.available.find((l) => l.language_id === language.id);
        });

        await this.getPage(1, websiteLangs);

        // We only keep the websites that have a gallery set up on the site
        this.movies = this.movies.filter((movie) => {
            for (const route of this.routes) {
                const galleryRoute = this.website.routes.find((r) => r.name === route.name);
                const itemPosition = movie.galleries ? movie.galleries[route.gallery] : null;

                return (galleryRoute && itemPosition);
            }

            return false;
        });

        return this.movies;
    }

    /**
     * Gets movies for specific page
     * @param pageNumber - Page to get
     * @param languages - Languages to filter
     */
    private async getPage (pageNumber: number, languages: ILanguage[]): Promise<any> {
        logger.info(`Obtaining movies id for page "${pageNumber}"`, {
            transactionId: this.transactionId
        });

        // To get movies, for performance purposes, first we need the ids
        const ids: number[] = await this.getIds(pageNumber);

        // If no ids found, mean already we got all movies
        if (!ids || !ids.length) {
            return this.movies;
        }

        // Now we can get pending movies and save it
        this.movies = this.movies.concat(await this.getMovies(ids, languages));

        return this.getPage(pageNumber + 1, languages);
    }

    /**
     * Get movie ids for specific page
     * @param pageNumber - Page to get
     */
    private async getIds (pageNumber: number): Promise<number[]> {
        const limit = 250;
        const offset = limit * (pageNumber - 1);
        const query = squel.select()
            .field('m.id', 'id')
            .from('movie', 'm')
            .order('m.gallery_popular_position')
            // We only want movies that have at least one gallery
            .where(
                squel.expr()
                    .or('m.gallery_popular_position IS NOT NULL')
                    .or('m.gallery_now_playing_position IS NOT NULL')
                    .or('m.gallery_top_rated_position IS NOT NULL')
                    .or('m.gallery_upcoming_position IS NOT NULL')
            )
            .offset(offset)
            .limit(limit);

        logger.silly('Searching movie ids', {
            transactionId: this.transactionId,
            query: query.toString()
        });
        
        const preparedQuery = query.toParam();
        const [ rows ] = await dbPool.query(preparedQuery.text, preparedQuery.values);

        if (!rows || !rows.length) {
            return [];
        }

        return rows.map((row) => row.id);
    }

    /**
     * Gets pending movies. And creates an array for each movie and language
     * @param ids - Movie ids to seach for specific page to use
     * @param languages - Languages to use
     */
    private async getMovies (ids: number[], languages: ILanguage[]): Promise<IMovie[]> {
        const movies: IMovie[] = [];
        const query = squel.select()
            .field('ml.title')
            .field('ml.movie_id', 'movieId')
            .field('ml.language_id', 'languageId')
            .from('movie_language', 'ml')
            .left_join('movie', 'm', 'm.id = ml.movie_id')
            // We only keep movies that doesn't has articles created
            .left_join('article', 'a', 
                squel.expr()
                    .and('m.id = a.movie_id')
                    .and('a.website_id = ?', this.website.id)
            )
            .where(
                squel.expr()
                    .and('a.movie_id IS NULL')
                    .and('a.language_id IS NULL')
            )
            .where('ml.movie_id IN ?', ids)
            .where('ml.language_id IN ?', languages.map((lang) => lang.id))
            .group('ml.movie_id')
            .group('ml.language_id');

        for (const route of this.routes) {
            query.field(`m.${route.gallery}`, route.gallery);
        }

        logger.silly('Searching pending movies', {
            transactionId: this.transactionId,
            query: query.toString()
        });

        const preparedQuery = query.toParam();
        const [ rows ] = await dbPool.query(preparedQuery.text, preparedQuery.values);
        
        // For each movie, we find his lang data and push it to out movies object
        for (const row of rows) {
            const { title, movieId, languageId, ...galleries } = row;
            const lang = languages.find((l) => l.id === row.languageId);

            if (row.title && lang) {
                movies.push({
                    movieId,
                    title,
                    lang,
                    galleries
                });
            }
        }

        return movies;
    }
}