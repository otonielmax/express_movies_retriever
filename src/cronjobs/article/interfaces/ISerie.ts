import { IArticle } from './IArticle';
import { ILanguage } from './ILanguage';
export interface ISerie {
    serieId: number;
    title: string;
    lang: ILanguage;
    article?: IArticle;
}