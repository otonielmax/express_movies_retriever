export interface ILanguage {
    id: number;
    name: string;
    iso: string;
}