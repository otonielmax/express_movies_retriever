import { IArticle } from './IArticle';
import { ILanguage } from './ILanguage';

export interface IMovie {
    movieId: number;
    title: string;
    lang: ILanguage;
    galleries?: string[];
    article?: IArticle;
}