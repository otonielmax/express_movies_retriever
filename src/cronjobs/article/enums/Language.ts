export enum ArticleForgeLanguages {
    english = 'en',
    dutch = 'nl',
    french = 'fr',
    german = 'de',
    italian = 'it',
    portuguese = 'pt',
    spanish = 'es',
}