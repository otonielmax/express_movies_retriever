export enum ArticleForgeLengths {
    short = 'Short',
    medium = 'Medium',
    long = 'Long',
}