import dbPool from '../../database/pool';
import * as getWebsitesConfig from '../../helpers/getWebsitesConfig';
import { logger } from '../../logger';
import { ArticleForgeLanguages, Events } from './enums';
import { ILanguage, IMovie, ISerie } from './interfaces';
import { 
    ArticleManager,
    CrawlerManager,
    LanguageManager,
    MovieManager,
    // SerieManager,
} from './managers';

const transactionId = 'cronjob-articles';

const run = async () => {
    logger.info('Starting job articles', { transactionId });

    try {
        const articleManager: ArticleManager = new ArticleManager(transactionId);
        const crawler: CrawlerManager = new CrawlerManager(transactionId);
        const languageManager: LanguageManager = new LanguageManager(transactionId);

        // Gets languages and websites config
        const languages: ILanguage[] = await languageManager.getAllByIso(Object.values(ArticleForgeLanguages));
        const websites = getWebsitesConfig();

        crawler.on(Events.articleCreated, articleManager.save);

        for (const website of websites) {
            if (!website.articles || !website.articles.enabled) {
                logger.debug(`Skiping website ${website.name} because articles config is not enabled`, { transactionId });
                continue;
            }

            // Creates new managers for each website
            const movieManager: MovieManager = new MovieManager(website, transactionId);
            // const serieManager: SerieManager = new SerieManager(transactionId);

            // Gets all movies and series
            const movies: IMovie[] = await movieManager.getAll(languages);
            const series: ISerie[] = []; // await serieManager.getAll(languages);

            // Creates one array for each movie, serie and language
            const items: Array<IMovie | ISerie> = [
                ...movies,
                ...series
            ];

            // Creates puppeteer instance
            await crawler.launch();

            logger.debug(`${items.length} pending items to be processed for website ${website.name}`, { transactionId });

            for (const item of items) {
                await crawler.processJob(website, item);
            }

            await crawler.close();
        }

        crawler.removeAllListeners(Events.articleCreated);
    } catch (err) {
        logger.error('Error executing cronjob', {
            transactionId,
            err
        });
    }

    await dbPool.endPool();
};

run();