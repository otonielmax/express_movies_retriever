'use strict';

import * as config from 'config';

import dbPool from '../../database/pool';
import * as healthCheck from '../../helpers/healthCheck';
import { IImageMedia } from '../../interfaces/ImageMedia';
import { logger } from '../../logger';
import { getAllImages } from './helpers/get_image';
import { processImages } from './helpers/store_image';

const transactionId = 'cronjob-tmdb-image-download';

// Start Job
const run = async () => {
    try {
        logger.info('Starting cronjob', { transactionId });

        const images: IImageMedia[] = await getAllImages();

        if (images.length) {
            logger.info(`Processing "${images.length}" images`, { transactionId });
            const results = await processImages(images, transactionId);

            if (results.error) {
                logger.error('Some images failed to be processed', { transactionId, results });
            } else {
                logger.info('All images have been processed successfully', { transactionId });
            }
        } else {
            logger.info('There are no images to process', { transactionId });
        }

        await healthCheck(transactionId, config.HC_TMDB_IMAGE_DOWNLOAD);

        logger.info('Job executed successfully', { transactionId });
    } catch (err) {
        logger.error('Error executing cronjob', { transactionId, err });
    }

    await dbPool.endPool();
};

run();