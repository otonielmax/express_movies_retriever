import dbPool from '../../../database/pool';
import { ImageMediaStatus } from '../../../enums/ImageMediaStatus';

/**
 * Obtiene el listado de todas las imagenes de la base de datos
 */
export const getAllImages = async () => {
    const query = `
        SELECT
            im.id, 
            im.path, 
            im.file_name,
            im.image_media_type_id
        FROM
            image_media im
        WHERE
            im.image_media_status_id = ?
    `;

    const [ rows ] = await dbPool.query(query, [ImageMediaStatus.PENDING]);

    return rows;
};

/**
 * Actualiza el status de la imagen
 */
export const updateImageStatus = async (item: any, statusId: ImageMediaStatus) => {
    const query = `
        UPDATE
            image_media im
        SET 
            im.image_media_status_id = ?
        WHERE
            im.id = ?
    `;

    return dbPool.query(query, [statusId, item.id]);
};