'use strict';

import * as AWS from 'aws-sdk';
import * as config from 'config';
import * as request from 'request-promise';
import * as sharp from 'sharp';

import { ImageMediaStatus } from '../../../enums/ImageMediaStatus';
import { ImageMediaType } from '../../../enums/ImageMediaType';
import { IImageMedia } from '../../../interfaces/ImageMedia';
import { logger } from '../../../logger';
import { updateImageStatus } from '../helpers/get_image';

AWS.config.update({
    accessKeyId: config.DO_ACCESS_KEY_ID,
    secretAccessKey: config.DO_SECRET_ACCESS_KEY,
});

const s3 = new AWS.S3({
    endpoint: 'nyc3.digitaloceanspaces.com'
});

const listImagesMediaSize = [
    {
        imageMediaType: ImageMediaType.BACKDROP,
        sizes: [
            300,
            780,
            1280
        ]
    },
    {
        imageMediaType: ImageMediaType.LOGO,
        sizes: [
            92,
            150
        ]
    },
    {
        imageMediaType: ImageMediaType.POSTER,
        sizes: [
            92,
            185,
            342,
            780
        ]
    },
    {
        imageMediaType: ImageMediaType.PROFILE,
        sizes: [
            45,
            92
        ]
    },
    {
        imageMediaType: ImageMediaType.STILL,
        sizes: [
            92,
            185,
            300
        ]
    },
];

export interface IProcessImagesResult {
    total: number;
    success: number;
    error: number;
}

// Proceso para subir imagenes a AWS
export const processImages = async (images: IImageMedia[], transactionId: string): Promise<IProcessImagesResult> => {    
    // Recorremos el listado de imagenes a enviar
    const results = {
        total: 0,
        success: 0,
        error: 0
    };

    for (const image of images) {
        results.total++;
        logger.info(`Processing image "${image.path}"`, { transactionId, image });

        try {
            const bufferImage = await downloadImage(image, transactionId);

            // Obtenemos el indice del tipo de imagen a trabar
            const imageMediaConfig = listImagesMediaSize.find((im) => im.imageMediaType === image.image_media_type_id);
            const sizes = imageMediaConfig?.sizes || [];
            // Recorremos todos los tamaños necesarios de cada imagen
            for (const mediaSize of sizes) {           
                const path = `tmdb-images/${mediaSize}/${image.file_name}`;
                const fileExists = await checkIfFileExists(path);

                logger.debug(`Processing image "${image.path}" size "${mediaSize}"`, { transactionId });

                // Si la imagen no existe, ajustamos la imagen con sharp y la subimos
                if (!fileExists) {
                    // Aplicamos el modulo sharp para modificar las dimensiones
                    const newBufferImage = await customizateSharp(bufferImage, mediaSize);
                    await createFileInAWS(path, newBufferImage, transactionId);
                } else {
                    logger.debug(`Size already "${mediaSize}" exists in Bucket, skipping`, { transactionId });
                }
            }
    
            // Marcar imagen como success
            await updateImageStatus(image, ImageMediaStatus.COMPLETED);
            results.total++;

            logger.info(`Image ${image.path} processed successfully`, { transactionId });
        } catch (err) {
            logger.error(`Error processing file "${image.path}"`, { transactionId, err, image });
            // Marcar imagen como erronea
            await updateImageStatus(image, ImageMediaStatus.COMPLETED);
            results.error++;
        }
    }

    return results;
};

// Subida del buffer al espacio de AWS
const createFileInAWS = async (key: string, value: Buffer, transactionId: string): Promise<any> => {
    const options = {
        Bucket: config.DO_BUCKET,
        Key: key,
        Body: value
    };

    try {
        return s3.putObject(options).promise();    
    } catch (err) {
        logger.error(`Error subiendo buffer al bucket `, {
            transactionId,             
            options,
            err
        });
        throw err;
    }
};

// Verificar si el archivo existe en AWS
const checkIfFileExists = async (key: string): Promise<boolean> => {
    const options = {
        Bucket: config.DO_BUCKET,
        Key: key
    };

    try {
        await s3.headObject(options).promise();
        // Si no falla, es porque el objeto existe
        return true;
    } catch (err) {
        if (err && err.code === 'NotFound') {                            
            return false;
        }

        throw err;
    }    
};

// Modificar propiedades de la imagen y devolver buffer de la misma
const customizateSharp = async (buffer: Buffer, size?: number): Promise<any> => {
    const sharpPromise = sharp(buffer)
        .jpeg({ quality: 60 });

    if (size) {
        sharpPromise.resize(size);
    }

    return sharpPromise.toBuffer();
};

// Descargar imagen en formato original
const downloadImage = async (item: IImageMedia, transactionId: string): Promise<any> => {
    const requestParams = {
        method: 'GET',
        url: `https://image.tmdb.org/t/p/original${item.path}`,
        timeout: 15000,
        simple: true,
        encoding: null
    };

    try {    
        const buffer = request(requestParams);

        // Si la imagen es PNG, convertirla a JPG
        if (item.path.includes('.png')) {                                                   
            const newBuffer = customizateSharp(buffer);                    
            return newBuffer;              
        }

        return buffer;
    } catch (err) {
        logger.error(`Error descargando imagen`, {
            transactionId,            
            requestParams,
            item,
            err
        });

        throw err;
    }
};