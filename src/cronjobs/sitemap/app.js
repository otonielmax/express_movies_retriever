const logger = require('@smartadtags/logger');
const async = require('async');
const config = require('config');
const moment = require('moment');
const xml = require('xml');

logger.config({
    environment: config.LOGGER_ENV,
    service: 'movies-retriever',
    logLevel: config.LOGGER_LOG_LEVEL,
    format: config.LOGGER_FORMAT
});
const transactionId = 'cronjob-sitemap';

const getWebsitesConfig = require('../../helpers/getWebsitesConfig');
const healthCheck = require('../../helpers/healthCheck');
const languageHelper = require('../../helpers/language');
const dbService = require('../../services/db');
const redisService = require('../../services/redis');

const processPage = (page, callback) => {
    const limit = 1000;
    const offset = limit * (page - 1);

    async.waterfall([
        (cb) => {
            const query = `
                SELECT
                    id
                FROM
                    movie
                WHERE
                    popularity >= ?
                ORDER BY
                    popularity DESC
                LIMIT
                    ?
                OFFSET
                    ?
            `;

            const minPopularity = config.MOVIES_MIN_POPULARITY;
            dbService.query(query, [minPopularity, limit, offset], (err, movies) => {
                if (err) {
                    return cb(err);
                }

                const movieIds = movies.map((movie) => movie.id);

                return cb(null, movieIds);
            });
        },
        (movieIds, cb) => {
            if (!movieIds || !movieIds.length) {
                return cb(null, []);
            }

            const query = `
                SELECT
                    mh.movie_id,
                    m.created_at,
                    m.updated_at,
                    YEAR(m.release_date) release_year,
                    mh.website_id,
                    mh.hash,
                    mh.language_id,
                    mh.language_locale_id
                FROM
                    movie_hash mh
                JOIN
                    movie m ON mh.movie_id=m.id
                WHERE
                    mh.movie_id IN (?) AND
                    mh.active = 1
                ORDER BY
                    m.popularity DESC
            `;

            dbService.query(query, [movieIds], (err, result) => {
                if (err) {
                    return cb(err);
                }

                // Agrupar las peliculas
                const moviesObj = {};
                const movies = [];
                for (let movie of result) {
                    if (!moviesObj[movie.movie_id]) {

                        moviesObj[movie.movie_id] = {
                            movie_id: movie.movie_id,
                            release_year: movie.release_year,
                            updated_at: movie.updated_at,
                            hashes: [],
                        };
                    }
                    moviesObj[movie.movie_id].hashes.push({
                        hash: movie.hash,
                        website_id: movie.website_id,
                        language_id: movie.language_id,
                        language_locale_id: movie.language_locale_id,
                    });
                }

                // Convertir a arreglo
                for (let id in moviesObj) {
                    movies.push(moviesObj[id]);
                }

                return callback(null, movies);
            });
        },
    ], (err, movies) => {
        return callback(err, movies);
    });
};

const generateMoviesXml = (website, movies) => {
    let sitemaps = [];
    let currentSitemap = 0;
    let currentSitemapCount = 0;
    const movieRoute = website.routes.find((r) => r.name === 'movie').values;
    const defaultLanguage = website.language.available.find((l) => l.default);
    const totalMovies = movies.length;

    for (const movieIndex in movies) {
        const movie = movies[movieIndex];
        const movieLangs = [];
        const moviePriority = getMoviePriority(Number(movieIndex), totalMovies);

        // Preparo todos los idiomas
        for (const language of website.language.available) {
            const langId = language.show_locale ? language.language_locale_id : language.language_id;
            let bestHash;
            // Si tiene multidioma, usa el lenguaje actual, sino los generos los genera solo con el idioma base
            if (website.language.multiLanguage) {
                bestHash = getBestMovieHash(website, movie.hashes, language, defaultLanguage);
            } else {
                bestHash = getBestMovieHash(website, movie.hashes, defaultLanguage, defaultLanguage);
            }
            const prefix = getLanguagePrefix(website, language);
            const path = movieRoute[langId] || movieRoute.default;
            const link = website.host + prefix + generateMovieLink(path, movie, bestHash);

            movieLangs.push({
                href: link,
                hreflang: languageHelper.getLanguage(langId).iso,
            });
        }

        for (const alternate of movieLangs) {
            let alternates = [{
                loc: alternate.href,
            }, {
                lastmod: moment(movie.updated_at).format(),
            }, {
                changefreq: 'monthly'
            }, {
                priority: moviePriority
            }];

            for (const alternate2 of movieLangs) {
                alternates.push({
                    'xhtml:link': [{
                        _attr: {
                            rel: 'alternate',
                            hreflang: alternate2.hreflang,
                            href: alternate2.href,
                        },
                    }]
                });
            }

            currentSitemapCount++;

            if (currentSitemapCount > 5000) {
                currentSitemap++;
                currentSitemapCount = 1;
            }

            if (!sitemaps[currentSitemap]) {
                sitemaps[currentSitemap] = [];
            }

            sitemaps[currentSitemap].push({
                url: alternates,
            });
        }
    }

    // Agregar cabeceras
    for (let sitemap of sitemaps) {
        sitemap.unshift({
            _attr: {
                'xmlns': 'http://www.sitemaps.org/schemas/sitemap/0.9',
                'xmlns:xhtml': 'http://www.w3.org/1999/xhtml',
            }
        });
    }

    // Pasar a XML a todos los sitemaps
    let result = [];
    for (const sitemap of sitemaps) {
        result.push(xml({
            urlset: sitemap,
        }, {
            declaration: true,
        }));
    }

    return result;
};

const moviesSitemap = (website, movies, callback) => {
    let allMovies;
    if (website.id === 3) {
        allMovies = movies.slice(0, 1000);
    } else {
        allMovies = movies;
    }

    const sitemaps = generateMoviesXml(website, allMovies);

    return callback(null, sitemaps);
};

const commonSitemap = (website, callback) => {
    const now = moment().format();
    const urlSet = [{
        _attr: {
            'xmlns': 'http://www.sitemaps.org/schemas/sitemap/0.9',
            'xmlns:xhtml': 'http://www.w3.org/1999/xhtml',
        }
    }];

    // ------------------------------------
    // HOME, CARTELERA Y ESTRENOS
    // ------------------------------------
    let availableRoutes = [];

    // Si el sitio tiene ruta de home, la procesa
    let route = website.routes.find((r) => r.name === 'home');
    if (route) {
        availableRoutes.push(route);
    }

    // Si el sitio tiene ruta de cartelera, la procesa
    route = website.routes.find((r) => r.name === 'billboard');
    if (route) {
        availableRoutes.push(route);
    }

    // Si el sitio tiene ruta de estrenos, la procesa
    route = website.routes.find((r) => r.name === 'premiere');
    if (route) {
        availableRoutes.push(route);
    }

    let routes = website.routes.filter((r) => r.name === 'home_alt');
    if (routes && routes.length) {
        availableRoutes = availableRoutes.concat(routes);
    }

    // Si el sitio tiene home alternativos, los procesa

    // Realizo lo mismo para cada una de las rutas mencionadas arriba
    for (const route of availableRoutes) {
        const currentRoute = route.values;
        const currentLangs = [];
        // Preparo todos los idiomas
        for (const language of website.language.available) {
            const langId = language.show_locale ? language.language_locale_id : language.language_id;
            const prefix = getLanguagePrefix(website, language);
            const path = currentRoute[langId] || currentRoute.default;
            const link = website.host + prefix + path;
            currentLangs.push({
                href: link,
                hreflang: languageHelper.getLanguage(langId).iso,
            });
        }

        // Agrego todos los idiomas al XML
        for (const alternate of currentLangs) {
            let url = [{
                loc: alternate.href,
            }, {
                lastmod: now,
            }, {
                changefreq: 'hourly'
            }, {
                priority: 1.0
            }];

            for (const alternate2 of currentLangs) {
                url.push({
                    'xhtml:link': [{
                        _attr: {
                            rel: 'alternate',
                            hreflang: alternate2.hreflang,
                            href: alternate2.href,
                        },
                    }]
                });
            }

            urlSet.push({
                url: url,
            });
        }
    }

    // ------------------------------------
    // GENRES
    // ------------------------------------
    const query = `
        SELECT
            g.id,
            gl.language_id,
            gl.hash
        FROM
            genre_language gl
        JOIN
            genre g ON g.id = gl.genre_id
        WHERE
            g.movies = 1
    `;

    dbService.query(query, [], (err, genres) => {
        if (err) {
            return callback(err);
        }

        const auxGenres = {};
        // Agrupar generos
        for (const genre of genres) {
            if (!auxGenres[genre.id]) {
                auxGenres[genre.id] = {
                    genre_id: genre.id,
                    hashes: [],
                };
            }
            auxGenres[genre.id].hashes.push({
                language_id: genre.language_id,
                hash: genre.hash,
            });
        }

        const genreRoute = website.routes.find((r) => r.name === 'genre').values;
        const defaultLanguage = website.language.available.find((l) => l.default);
        // Procesar generos
        for (const genreId in auxGenres) {
            const genre = auxGenres[genreId];
            const genreLangs = [];
            for (const language of website.language.available) {
                const langId = language.show_locale ? language.language_locale_id : language.language_id;
                const prefix = getLanguagePrefix(website, language);
                const path = genreRoute[langId] || genreRoute.default;
                const link = website.host + prefix + path;
                let bestHash;
                // Si tiene multidioma, usa el lenguaje actual, sino los generos los genera solo con el idioma base
                if (website.language.multiLanguage) {
                    bestHash = getBestGenreHash(genre.hashes, language, defaultLanguage);
                } else {
                    bestHash = getBestGenreHash(genre.hashes, defaultLanguage, defaultLanguage);
                }

                genreLangs.push({
                    href: link.replace(':hash', bestHash.hash),
                    hreflang: languageHelper.getLanguage(langId).iso,
                });
            }

            for (const alternate of genreLangs) {
                let url = [{
                    loc: alternate.href,
                }, {
                    lastmod: now,
                }, {
                    changefreq: 'hourly'
                }, {
                    priority: 0.8
                }];

                for (const alternate2 of genreLangs) {
                    url.push({
                        'xhtml:link': [{
                            _attr: {
                                rel: 'alternate',
                                hreflang: alternate2.hreflang,
                                href: alternate2.href,
                            },
                        }]
                    });
                }

                urlSet.push({
                    url: url,
                });
            }
        }

        const sitemap = [{
            urlset: urlSet
        }];

        let xmlSitemap = xml(sitemap, {
            declaration: true,
        });

        return callback(null, xmlSitemap);
    });
};
const generateMainSitemap = (website, sitemapAmount) => {
    const random = +new Date();
    const now = moment().format();
    const sitemapIndex = [];
    const sitemapSet = [{
        _attr: {
            xmlns: 'http://www.sitemaps.org/schemas/sitemap/0.9',
        }
    }];

    const defaultRoute = website.routes.find((r) => r.name === 'sitemap').paginationValues.default;
    for (let i = 1; i <= sitemapAmount; i++) {
        let url = website.host + defaultRoute.replace(':page', i) + '?v=' + random;
        sitemapSet.push({
            sitemap: [{
                loc: url,
            }, {
                lastmod: now,
            }]
        });
    }

    sitemapIndex.push({
        sitemapindex: sitemapSet
    });

    let xmlSitemap = xml(sitemapIndex, {
        declaration: true,
    });

    return xmlSitemap;
};

const generateWebsiteSitemap = (website, movies, callback) => {
    async.waterfall([
        (cb) => {
            logger.info(`[${website.name}] Generating common sitemap`, { transactionId });
            commonSitemap(website, (err, sitemap) => {
                if (err) {
                    logger.error(`[${website.name}] Error generating common sitemap`, { transactionId });
                    return cb(err);
                }

                return cb(null, [sitemap]);
            });
        },
        (sitemaps, cb) => {
            logger.info(`[${website.name}] Generating movies sitemap`, { transactionId });
            moviesSitemap(website, movies, (err, moviesSitemaps) => {
                if (err) {
                    logger.error(`[${website.name}] Generating movies sitemap`, { transactionId });
                    return cb(err);
                }

                sitemaps = sitemaps.concat(moviesSitemaps);

                return cb(null, sitemaps);
            });
        },
        (sitemaps, cb) => {
            logger.info(`[${website.name}] Generating main sitemap`, { transactionId });
            const mainSitemap = generateMainSitemap(website, sitemaps.length);

            return cb(null, sitemaps, mainSitemap);
        },
        (sitemaps, mainSitemap, cb) => {
            const key = `${website.id}_SITEMAP`;
            logger.info(`[${website.name}] Storing main sitemap`, { transactionId, key });
            redisService.setData(key, mainSitemap, null, (err) => {
                if (err) {
                    logger.error(`[${website.name}] Error storing main sitemap`, { transactionId, err });
                    return cb(err);
                }

                return cb(null, sitemaps);
            });
        },
        (sitemaps, cb) => {
            logger.info(`[${website.name}] Storing movies sitemaps`, { transactionId });
            async.forEachOf(sitemaps, (sitemap, index, cb) => {
                const sitemapNumber = index + 1;

                const key = `${website.id}_SITEMAP_${sitemapNumber}`;
                logger.info(`[${website.name}] Storing movie sitemap "${sitemapNumber}"`, { transactionId, key });
                redisService.setData(key, sitemap, null, (err) => {
                    if (err) {
                        logger.error(`[${website.name}] Error storing movie sitemap "${sitemapNumber}"`, { transactionId, err });
                        return cb(err);
                    }

                    return cb(null);
                });
            }, (err) => {
                return cb(err);
            });
        },
    ], (err) => {
        return callback(err);
    });
};

const generateMovieLink = (baseLink, movie, hash) => {
    let url = baseLink;
    url = url.replace(':release_year', movie.release_year).replace(':hash', hash.hash);
    return encodeURI(url);
};

const getLanguagePrefix = (website, currentLanguage) => {
    let prefix = '';

    if (website.language.multiLanguage) {
        let languageId = currentLanguage.show_locale ? currentLanguage.language_locale_id : currentLanguage.language_id;
        const languageIso = languageHelper.getLanguage(languageId).iso;
        prefix = '/' + languageIso;
    }

    return prefix;
};

const getBestGenreHash = (hashes, preferredLanguage, defaultLanguage) => {
    let bestHash;
    // Primero intento agarrar el idioma preferido
    bestHash = hashes.find((h) => h.language_id === preferredLanguage.language_id);

    // Si no existe, intento agarrar el idioma por defecto de la pagina
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_id);
    }

    // Si no existe, agarro ingles
    if (!bestHash) {
        const fallbackLangId = languageHelper.getId('en');
        bestHash = hashes.find((hash) => hash.language_id === fallbackLangId);
    }

    // Si no existe, agarro el primero
    if (!bestHash) {
        bestHash = hashes[0];
    }

    return bestHash;
};

const getBestMovieHash = (website, hashes, preferredLanguage, defaultLanguage) => {
    let bestHash;
    // Primero intento agarrar el locale por defecto de la pagina y website correcto
    bestHash = hashes.find((h) => h.language_locale_id === preferredLanguage.language_locale_id && h.website_id === website.id);

    // Si no existe, intento agarrar el locale por defecto de la pagina
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_locale_id === preferredLanguage.language_locale_id && h.website_id === null);
    }

    // Si no existe, intento agarrar el idioma por defecto de la pagina y website correcto
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_id === preferredLanguage.language_id && h.website_id === website.id);
    }

    // Si no existe, intento agarrar el idioma por defecto de la pagina
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_id === preferredLanguage.language_id && h.website_id === null);
    }

    // Si no existe y tiene lenguaje por defecto, intento agarrarlo utilizando el website
    if (defaultLanguage && !bestHash) {
        bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_locale_id && h.website_id === website.id);

        if (!bestHash) {
            bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_id && h.website_id === website.id);
        }
    }

    // Si no existe y tiene lenguaje por defecto, intento agarrarlo
    if (defaultLanguage && !bestHash) {
        bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_locale_id && h.website_id === null);

        if (!bestHash) {
            bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_id && h.website_id === null);
        }
    }

    // Si no existe, agarro ingles + website
    if (!bestHash) {
        const fallbackLangId = languageHelper.getId('en');
        bestHash = hashes.find((h) => h.language_id === fallbackLangId && h.website_id === website.id);
    }

    // Si no existe, agarro ingles
    if (!bestHash) {
        const fallbackLangId = languageHelper.getId('en');
        bestHash = hashes.find((h) => h.language_id === fallbackLangId && h.website_id === null);
    }

    // Si no existe, agarro el primero
    if (!bestHash) {
        bestHash = hashes[0];
    }

    return bestHash;
};

const getMoviePriority = (currentItem, totalItems) => {
    // Para el primer 10% de items asigna prioridad 1.0
    // Para 10-20% asigna prioridad 0.9
    // Asi sucesivamente.

    const percent = Math.floor((currentItem / totalItems) * 10) / 10;
    const priority = Math.round((1 - percent) * 10) / 10;
    return priority;
};

async.waterfall([
    (cb) => {
        // Procesar websites
        const websites = getWebsitesConfig();
        return cb(null, websites);
    },
    // Get languages
    (websites, cb) => {
        languageHelper.retrieve((err) => {
            if (err) {
                return cb(err);
            }

            return cb(null, websites);
        });
    },
    // Get movie list
    (websites, cb) => {
        let allMovies = [];
        const retrievePage = (page) => {
            logger.info(`Processing movies page "${page}"`, { transactionId });
            processPage(page, (err, movies) => {
                if (err) {
                    return cb(err);
                }

                if (!movies.length) {
                    return cb(null, websites, allMovies);
                }

                allMovies = allMovies.concat(movies);

                return retrievePage(page + 1);
            });
        };

        retrievePage(1);
    },
    (websites, movies, cb) => {
        async.mapSeries(websites, (website, cb) => {
            generateWebsiteSitemap(website, movies, (err) => {
                logger.info(`[${website.name}] Sitemap generated`, { transactionId });
                return cb(err);
            });
        }, (err) => {
            return cb(err);
        });
    },
], (err) => {
    if (err) {
        logger.error('Error executing cronjob', { transactionId, err });
        dbService.endPool();
        redisService.close();
    } else {
        logger.info('Job executed successfully', { transactionId });
        healthCheck(transactionId, config.HC_SITEMAP_GENERATOR).then(() => {
            dbService.endPool();
            redisService.close();
        });
    }

});