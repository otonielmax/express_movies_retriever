import { logger } from '../../../logger';
import languageManager from '../../../managers/Language';
import { calculateGalleryPriority } from '../helpers/calculate_gallery_priority';
import { ISitemapUrl } from '../interfaces/SitemapUrl';
import { sitemapUrlsToXML } from './sitemap_urls_to_xml';

const galleries = [{
    routeName: 'home',
    galleryCol: 'gallery_popular_position',
    factor: 1
}, {
    routeName: 'movies_upcoming',
    galleryCol: 'gallery_upcoming_position',
    factor: 0.75
}, {
    routeName: 'movies_top_rated',
    galleryCol: 'gallery_top_rated_position',
    factor: 0.5
}, {
    routeName: 'movies_now_playing',
    galleryCol: 'gallery_now_playing_position',
    factor: 0.75
}];

/**
 * Genera el/los sitemaps de peliculas para un sitio.
 * Ordena las peliculas por prioridad teniendo en cuenta su ubicacion en cada una de las galerias (si es que el sitio las tiene activas)
 * La prioridad es la prioridad mas alta calculada de las galerias, pero la pelicula solo se muestra 1 vez en el sitemap.
 * La fecha de ultima actualizacion es muy importante y se calcula teniendo en cuenta los siguientes valores en orden de prioridad:
 * Fecha Articulo - Fecha Spineado - Fecha actualizacion lenguajes - Fecha actualizacion pelicula
 */
export const generateMoviesSitemap = async (transactionId: string, website, movies): Promise<string[] | null> => {
    // Controlo si el sitio tiene peliculas, sino no es necesario el sitemap.
    const movieRoute = website.routes.find((r) => r.name === 'movie');
    if (!movieRoute) {
        logger.info(`Skipping movies sitemap for website "${website.name}"`, { transactionId });
        return null;
    }

    logger.info(`Generating movies sitemap for website "${website.name}"`, { transactionId });

    const defaultLanguage = website.language.available.find((l) => l.default);

    const websiteMovies = movies.map((movie) => {
        return {
            ...movie,
            priority: calculateGalleryPriority({
                website,
                items: movies,
                item: movie,
                galleries,
                maxRange: 0.99,
                minRange: 0.5
            })
        };
    });

    // Filtrar peliculas sin prioridad (ya que no aparecen en listados habilitados en el sitio) y ordenar descendente
    const sitemapMovies = websiteMovies
        .filter((m) => m.priority !== null)
        .sort((a, b) => (a.priority.priority > b.priority.priority) ? -1 : 1);

    const sitemapItems: ISitemapUrl[] = [];

    // Recorrer todas las peliculas del sitemap
    for (const movie of sitemapMovies) {
        const movieArticle = movie.articles.find((a) => a.website_id === website.id);
        const movieSpin = movie.spins.find((s) => s.website_id === website.id);
        // Orden de preferencia: Articulo - Spin - Act Lenguaje - Act peli
        const updateDate =
            (movieArticle ? movieArticle.updated_at : null) ||
            (movieSpin ? movieSpin.created_at : null) ||
            movie.language_updated_at ||
            movie.updated_at;

        const movieLang: ISitemapUrl = {
            priority: movie.priority.priority,
            changeFreq: 'monthly',
            updateDate,
            alternates: []
        };

        // Parsear pelicula en todos los lenguajes disponibles en el sitio
        for (const language of website.language.available) {
            // Obtener datos del lenguaje
            const langId = language.show_locale ? language.language_locale_id : language.language_id;
            const localeId = language.language_locale_id;
            const langObj = await languageManager.getById(langId);
            // Calcular el mejor hash
            const bestHash = await getBestMovieHash(website, movie.hashes, language, defaultLanguage);
            if (bestHash) {
                // Verificar si es necesario añadir prefijo si es sitio multilenguaje
                const prefix = await languageManager.getPathPrefix(website, language);
                // Generar link de la pelicula
                const path = movieRoute.values[localeId] || movieRoute.values.default;
                const link = website.host + prefix + generateMovieLink(path, movie, bestHash);
    
                if (langObj) {
                    movieLang.alternates.push({
                        href: link,
                        hreflang: langObj.iso,
                        default: language.default || false,
                        language_locale_id: localeId
                    });
                }
            }
        }

        sitemapItems.push(movieLang);
    }

    return sitemapUrlsToXML(sitemapItems);
};

/**
 * Esta funcion va a cambiar y vamos a reusar la de series una vez que se migre el sistema de versiones de peliculas
 */
const getBestMovieHash = async (website, hashes, preferredLanguage, defaultLanguage) => {
    let bestHash;
    // Primero intento agarrar el locale por defecto de la pagina y website correcto
    bestHash = hashes.find((h) => h.language_locale_id === preferredLanguage.language_locale_id && h.website_id === website.id);

    // Si no existe, intento agarrar el locale por defecto de la pagina
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_locale_id === preferredLanguage.language_locale_id && h.website_id === null);
    }

    // Si no existe, intento agarrar el idioma por defecto de la pagina y website correcto
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_id === preferredLanguage.language_id && h.website_id === website.id);
    }

    // Si no existe, intento agarrar el idioma por defecto de la pagina
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_id === preferredLanguage.language_id && h.website_id === null);
    }

    // Si no existe y tiene lenguaje por defecto, intento agarrarlo utilizando el website
    if (defaultLanguage && !bestHash) {
        bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_locale_id && h.website_id === website.id);

        if (!bestHash) {
            bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_id && h.website_id === website.id);
        }
    }

    // Si no existe y tiene lenguaje por defecto, intento agarrarlo
    if (defaultLanguage && !bestHash) {
        bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_locale_id && h.website_id === null);

        if (!bestHash) {
            bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_id && h.website_id === null);
        }
    }

    const fallbackLang = await languageManager.get('en');
    if (fallbackLang) {
        // Si no existe, agarro ingles + website
        if (!bestHash) {
            bestHash = hashes.find((h) => h.language_id === fallbackLang.language_id && h.website_id === website.id);
        }
    
        // Si no existe, agarro ingles
        if (!bestHash) {
            bestHash = hashes.find((h) => h.language_id === fallbackLang.language_id && h.website_id === null);
        }
    }

    // Si no existe, agarro el primero
    if (!bestHash) {
        bestHash = hashes[0];
    }

    return bestHash;
};

const generateMovieLink = (baseLink, movie, hash) => {
    return baseLink.replace(':release_year', movie.release_year).replace(':hash', hash.hash);
};