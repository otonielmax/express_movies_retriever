import * as moment from 'moment';
import * as xml from 'xml';

import { ISitemapUrl } from '../interfaces/SitemapUrl';

const MAX_ITEMS = 5000;

const getAlternate = (href, hreflang) => {
    return {
        'xhtml:link': [{
            _attr: {
                rel: 'alternate',
                hreflang,
                href
            },
        }]
    };
};

/**
 * Transforma SitemapUrls a XML. Los XML generados son Sitemaps multilenguaje basado en las buenas practicas de Google.
 * 
 * @see https://support.google.com/webmasters/answer/189077?hl=es-419
 */
export const sitemapUrlsToXML = (sitemapUrls: ISitemapUrl[]): string[] => {
    const sitemaps: any[] = [];

    // Recorrer todos los items del sitemap y parsearlos
    for (const sitemapUrl of sitemapUrls) {
        // Dentro de cada item recorrer las alternativas de idioma
        // Cada variante de lenguaje debe estar presente en el sitemap como un <url> y a su vez
        // tener dentro un listado de variantes para cada idioma
        for (const alternate of sitemapUrl.alternates) {
            // Por cada idioma disponible crear un item nuevo
            const xmlItem: any = [{
                loc: alternate.href,
            }, {
                lastmod: moment(sitemapUrl.updateDate).format(),
            }, {
                changefreq: sitemapUrl.changeFreq
            }, {
                priority: sitemapUrl.priority
            }];

            for (const alternate2 of sitemapUrl.alternates) {
                // Agregar ruta alternativa de cada idioma, y el default si es necesario
                xmlItem.push(getAlternate(alternate2.href, alternate2.hreflang));
                if (alternate2.default) {
                    xmlItem.push(getAlternate(alternate2.href, 'x-default'));
                }
            }

            // Si ya se supero la cantidad maxima de elementos o si es el primer elemento, añado un sitemap nuevo
            if (!sitemaps.length || sitemaps[sitemaps.length - 1].length >= MAX_ITEMS) {
                sitemaps.push([]);
            }
    
            // Agrego el elemento al ultimo sitemap de la lista
            sitemaps[sitemaps.length - 1].push({
                url: xmlItem,
            });
        }
    }

    // Agregar cabeceras
    for (const sitemap of sitemaps) {
        sitemap.unshift({
            _attr: {
                'xmlns': 'http://www.sitemaps.org/schemas/sitemap/0.9',
                'xmlns:xhtml': 'http://www.w3.org/1999/xhtml',
            }
        });
    }

    // Pasar a XML a todos los sitemaps
    const result: string[] = [];
    for (const sitemap of sitemaps) {
        result.push(xml({
            urlset: sitemap,
        }, {
            declaration: true,
        }));
    }

    return result;
};