import * as moment from 'moment';
import * as xml from 'xml';

/**
 * Genera un "Sitemap index" segun las buenas práctiacs de Google
 * Se genera un indice que posee en enlace a todos los sitemaps de un sitio
 * 
 * @see https://support.google.com/webmasters/answer/75712?hl=es
 */
export const generateSitemapIndex = (website, sitemapAmount: number): string => {
    const random = +new Date();
    const now = moment().format();

    const sitemapIndex: any = [];
    const sitemapSet: any = [{
        _attr: {
            xmlns: 'http://www.sitemaps.org/schemas/sitemap/0.9',
        }
    }];

    // Obtener la ruta de sitemaps
    const route = website.routes.find((r) => r.name === 'sitemap').paginationValues.default;

    // Se genera un objeto <sitemap> por cada uno de los sitemaps que tiene el sitio
    for (let i = 1; i <= sitemapAmount; i++) {
        const url = website.host + route.replace(':page', i) + '?v=' + random;
        sitemapSet.push({
            sitemap: [{
                loc: url,
            }, {
                lastmod: now,
            }]
        });
    }

    sitemapIndex.push({
        sitemapindex: sitemapSet
    });

    return xml(sitemapIndex, {
        declaration: true,
    });
};