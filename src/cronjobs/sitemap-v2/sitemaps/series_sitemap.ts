import { logger } from '../../../logger';
import languageManager from '../../../managers/Language';
import { calculateGalleryPriority } from '../helpers/calculate_gallery_priority';
import { calculateSitemapPriority } from '../helpers/calculate_sitemap_priority';
import { ISitemapUrl } from '../interfaces/SitemapUrl';
import { sitemapUrlsToXML } from './sitemap_urls_to_xml';

const galleries = [{
    routeName: 'series_popular',
    galleryCol: 'gallery_popular_position',
    factor: 1
}, {
    routeName: 'series_top_rated',
    galleryCol: 'gallery_top_rated_position',
    factor: 0.5
}, {
    routeName: 'series_on_air',
    galleryCol: 'gallery_on_air_position',
    factor: 0.5
}, {
    routeName: 'series_airing_today',
    galleryCol: 'gallery_airing_today_position',
    factor: 0.5
}];

/**
 * Genera el/los sitemaps de series, temporadas y episodios para un sitio.
 * Series:
 *      Ordena las series por prioridad teniendo en cuenta su ubicacion en cada una de las galerias (si es que el sitio las tiene activas)
 *      La prioridad es la prioridad mas alta calculada de las galerias, pero el item solo se muestra 1 vez en el sitemap.
 *      La fecha de ultima actualizacion es muy importante y se calcula teniendo en cuenta los siguientes valores en orden de prioridad:
 *      Fecha Spineado - Fecha actualizacion lenguajes - Fecha actualizacion pelicula
 * Temporadas:
 *      Solo se generan enlaces de temporada cuando la ruta esta activa en el sitio. Es posible que haya sitios que solo posean pantalla
 *      de serie y episodio, estando el listado de episodios en la misma pantalla de serie.
 *      Ordena las temporadas por prioridad considerando la mas nueva como la mas importante (dentro de una serie), y luego aplica un factor
 *      basado en la popularidad de la serie para priorizar temporadas de series mas importantes.
 *      La fecha de ult actualizacion se calcula teniendo en cuenta los siguientes valores en orden de prioridad:
 *      Fecha actualizacion lenguajes - Fecha actualizacion pelicula
 * Episodios:
 *      Ordena los episodios por prioridad considerando el mas nuevo como el mas importante, y luego aplica un factor basado en la
 *      popularidad de la serie y de la temporada, para priorizar episodios de temporadas nuevas de series populares.
 *      La fecha de ult actualizacion se calcula teniendo en cuenta los siguientes valores en orden de prioridad:
 *      Fecha Spineado - Fecha actualizacion lenguajes - Fecha actualizacion pelicula
 */
export const generateSeriesSitemap = async (transactionId, website, series): Promise<string[] | null> => {
    // Controlo si el sitio tiene series, sino no es necesario el sitemap.
    const serieRoute = website.routes.find((r) => r.name === 'serie');
    if (!serieRoute) {
        logger.info(`Skipping series sitemap for website "${website.name}"`, { transactionId });
        return null;
    }
    /** Determina si existe la ruta en la configuración para las temporadas en el sitio. */
    const hasSeasonRoute = !!website.routes.find((route) => (route.name === 'season'));

    logger.info(`Generating series sitemap for website "${website.name}"`, { transactionId });

    const defaultLanguage = website.language.available.find((l) => l.default);

    const websiteSeries = series.map((serie) => {
        return {
            ...serie,
            priority: calculateGalleryPriority({
                website,
                items: series,
                item: serie,
                galleries,
                maxRange: 0.99,
                minRange: 0.5
            })
        };
    });

    // Filtrar series sin prioridad (ya que no aparecen en listados habilitados en el sitio) y ordenar descendente
    const sitemapSeries = websiteSeries
        .filter((s) => s.priority !== null)
        .sort((a, b) => (a.priority.priority > b.priority.priority) ? -1 : 1);

    if (!sitemapSeries.length) {
        logger.info(`Skipping series sitemap for website "${website.name}"`, { transactionId });
        return null;
    }

    const serieLangs: ISitemapUrl[] = [];
    const seasonLangs: ISitemapUrl[] = [];
    const episodeLangs: ISitemapUrl[] = [];

    // Recorrer todas las eries
    for (const serie of sitemapSeries) {
        const serieSpin = serie.spins.find((s) => s.website_id === website.id);
        // Orden de preferencia: Spin - Act Lenguaje - Act peli
        const serieUpdateDate = 
            (serieSpin ? serieSpin.created_at : null) ||
            serie.language_updated_at ||
            serie.updated_at;

        // La prioridad de la serie se calcula en base a la ubicacion en los listados
        const serieLang: ISitemapUrl = {
            priority: serie.priority.priority,
            updateDate: serieUpdateDate,
            changeFreq: 'monthly',
            alternates: []
        };

        // Ordenar todos las episodios y temporadas de mas nueva a mas vieja
        const sortedSeasons = serie.seasons.sort((a, b) => (a.season_number > b.season_number) ? -1 : 1);
        let sortedEpisodes = [];
        for (const season of sortedSeasons) {
            sortedEpisodes = sortedEpisodes.concat(season.episodes.sort((a, b) => (a.episode_number > b.episode_number) ? -1 : 1));
        }

        // Recorrer todas las temporadas ordenadas
        for (const seasonIndex in sortedSeasons) {
            const season = sortedSeasons[seasonIndex];
            // Orden de preferencia: Act Lenguaje - Act peli (en este nivel no hay spin)
            const seasonUpdateDate = 
                season.language_updated_at ||
                season.updated_at;
            
            // La prioridad de la season se decide en base a la ubicacion de la mismas dentro de la serie (de mas nueva a mas vieja)
            // Y se utiliza como factor la prioridad de la serie para dar mas prioridad a las temporadas de series importantes
            const seasonPriority = calculateSitemapPriority({
                totalItems: sortedSeasons.length,
                currentItem: Number(seasonIndex) + 1,
                factor: serie.priority.positionPercentage,
                minRange: 0.3,
                maxRange: 0.5
            });

            const seasonLang: ISitemapUrl = {
                priority: seasonPriority.priority,
                updateDate: seasonUpdateDate,
                changeFreq: 'monthly',
                alternates: []
            };

            // Recorrer todos los episodios
            for (const episode of season.episodes) {
                const episodeSpin = episode.spins.find((s) => s.website_id === website.id);
                // Orden de preferencia: Spin - Act Lenguaje - Act peli
                const episodeUpdateDate =
                    (episodeSpin ? episodeSpin.created_at : null) ||
                    episode.language_updated_at ||
                    episode.updated_at;

                // La prioridad del episodio se define a la ubicacion dentro de todos los episodios de la serie (de todas las temp)
                // Y se utiliza como factor la prioridad de la serie para dar mas prioridad a los episodios de series importantes
                // No es necesario utilizar le factor de temporada ya que la posicion se calcula entre todas las temporadas.
                const episodeIndex = Number(sortedEpisodes.findIndex((e: any) => e.episode_id === episode.episode_id));
                const episodePriority = calculateSitemapPriority({
                    totalItems: sortedEpisodes.length,
                    currentItem: Number(episodeIndex) + 1,
                    factor: serie.priority.positionPercentage,
                    minRange: 0,
                    maxRange: 0.3
                });

                const episodeLang: ISitemapUrl = {
                    priority: episodePriority.priority,
                    updateDate: episodeUpdateDate,
                    changeFreq: 'monthly',
                    alternates: []
                };

                // Recorrer todos los lenguajes activo
                for (const language of website.language.available) {
                    // Info lenguaje
                    const langId = language.show_locale ? language.language_locale_id : language.language_id;
                    const langObj = await languageManager.getById(langId);
                    // Info hash
                    const bestHash = await getBestHash(serie.hashes, language, defaultLanguage);

                    if (langObj && bestHash) {
                        // Procesar info de la serie
                        const serieAlternate = serieLang.alternates.find((a) => language.language_locale_id === a.language_locale_id);
                        if (!serieAlternate) {
                            const serieLink = await generateLink(website, language, 'serie', {
                                ...serie,
                                hash: bestHash.hash
                            });
                            if (serieLink) {
                                serieLang.alternates.push({
                                    language_locale_id: language.language_locale_id,
                                    href: serieLink,
                                    hreflang: langObj.iso,
                                    default: language.default || false
                                });
                            }
                        }

                        // Procesar info de la temporada
                        /* Si no existe la ruta de temporada en la configuración del sitio
                        * web, procedemos a saltar la generación de links de temporadas. */
                        if (hasSeasonRoute) {
                            const seasonAlternate = seasonLang.alternates.find((a) => language.language_locale_id === a.language_locale_id);
                            if (!seasonAlternate) {
                                const seasonLink = await generateLink(website, language, 'season', {
                                    ...season,
                                    hash: bestHash.hash
                                });
                                if (seasonLink) {
                                    seasonLang.alternates.push({
                                        language_locale_id: language.language_locale_id,
                                        href: seasonLink,
                                        hreflang: langObj.iso,
                                        default: language.default || false
                                    });
                                }
                            }
                        }

                        // Procesar info del episodio
                        const episodeAlternate = episodeLang.alternates.find((a) => language.language_locale_id === a.language_locale_id);
                        if (!episodeAlternate) {
                            const episodeLink = await generateLink(website, language, 'episode', {
                                ...episode,
                                hash: bestHash.hash,
                                season_number: season.season_number
                            });

                            if (episodeLink) {
                                episodeLang.alternates.push({
                                    language_locale_id: language.language_locale_id,
                                    href: episodeLink,
                                    hreflang: langObj.iso,
                                    default: language.default || false
                                });
                            }
                        }
                    }
                }

                if (episodeLang.alternates.length) {
                    episodeLangs.push(episodeLang);
                }
            }

            if (seasonLang.alternates.length) {
                seasonLangs.push(seasonLang);
            }
        }

        serieLangs.push(serieLang);
    }

    // Juntar todos los items
    const allSitemapItems: ISitemapUrl[] = ([] as ISitemapUrl[]).concat(...[serieLangs, seasonLangs, episodeLangs]);

    // Generar y retornar los XML preparados
    return sitemapUrlsToXML(allSitemapItems);
};

const getBestHash = async (hashes, preferredLanguage, defaultLanguage) => {
    let bestHash;

    // Si no existe, intento agarrar el locale por defecto de la pagina
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_locale_id === preferredLanguage.language_locale_id);
    }

    // Si no existe, intento agarrar el idioma por defecto de la pagina
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_id === preferredLanguage.language_id);
    }

    // Si no existe y tiene lenguaje por defecto, intento agarrarlo
    if (defaultLanguage && !bestHash) {
        bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_locale_id);

        if (!bestHash) {
            bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_id);
        }
    }

    const fallbackLang = await languageManager.get('en');
    if (fallbackLang) {
        // Si no existe, agarro ingles
        if (!bestHash) {
            bestHash = hashes.find((h) => h.language_id === fallbackLang.language_id);
        }
    }

    // Si no existe, agarro el primero
    if (!bestHash) {
        bestHash = hashes[0];
    }

    return bestHash;
};

const generateLink = async (website, language, routeName, item) => {
    const route = website.routes.find((r) => r.name === routeName);
    if (!route) {
        return null;
    }
    
    const langId = language.language_locale_id;
    const prefix = await languageManager.getPathPrefix(website, language);
    const currentVersion = item.versions.find((v) => v.website_id === website.id);
    const routeValues = currentVersion
        ? route.versionValues
        : route.values;

    let path = routeValues[langId] || routeValues.default;

    // Reemplazar version si es que tiene
    if (currentVersion) {
        path = path.replace(':version', currentVersion.version);
    }

    // Reemplazar toda clave del objeto
    for (const key in item) {
        if (item[key] !== 'undefined' && item[key] !== null) {
            path = path.replace(`:${key}`, item[key]);
        }
    }

    return website.host + prefix + path;
};