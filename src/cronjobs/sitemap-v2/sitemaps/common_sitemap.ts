import { logger } from '../../../logger';
import languageManager from '../../../managers/Language';
import { IGenre } from '../interfaces/Genre';
import { ISitemapUrl } from '../interfaces/SitemapUrl';
import { sitemapUrlsToXML } from './sitemap_urls_to_xml';

const commonRoutes = [
    'home',
    'movies_upcoming',
    'movies_top_rated',
    'movies_now_playing',
    'series_popular',
    'series_top_rated',
    'series_on_air',
    'series_airing_today'
];

/**
 * Genera el sitemap de items generales, que incluye rutas de listados y rutas de generos
 */
export const generateCommonSitemap = async (transactionId: string, website, genres: IGenre[]): Promise<string[] | null> => {
    const sitemapItems: ISitemapUrl[] = [];
    const now = new Date();

    // De todas las rutas disponibles, filtrar solo las rutas que estan configuradas en este sitio
    const availableRoutes = commonRoutes.filter((routeName) => {
        return !!website.routes.find((r) => r.name === routeName);
    });

    if (!availableRoutes.length) {
        logger.info(`Skipping common sitemap for website "${website.name}"`, { transactionId });
        return null;
    }
    
    const defaultLanguage = website.language.available.find((l) => l.default);
    
    logger.info(`Generating common sitemap for website "${website.name}"`, { transactionId });

    // Agregar rutas genericas
    for (const routeName of availableRoutes) {
        const route = website.routes.find((r) => r.name === routeName).values;
        const routeLangs: ISitemapUrl = {
            priority: 1,
            updateDate: now,
            changeFreq: 'daily',
            alternates: []
        };

        // Preparo todos los idiomas
        for (const language of website.language.available) {
            const langId = language.show_locale ? language.language_locale_id : language.language_id;
            const langLocaleId = language.language_locale_id;
            const langObj = await languageManager.getById(langId);

            if (langObj) {
                const prefix = await languageManager.getPathPrefix(website, language);
                const path = route[langLocaleId] || route.default;
                const link = website.host + prefix + path;
                routeLangs.alternates.push({
                    href: link,
                    hreflang: langObj.iso,
                    default: language.default || false,
                    language_locale_id: langLocaleId
                });
            }
        }

        sitemapItems.push(routeLangs);
    }

    // Agregar rutas de generos
    const genreRoute = website.routes.find((r) => r.name === 'genre');
    if (genreRoute) {
        const genreRouteValues = genreRoute.values;
        for (const genre of genres) {
            const genreLangs: ISitemapUrl = {
                priority: 1,
                updateDate: now,
                changeFreq: 'daily',
                alternates: []
            };

            for (const language of website.language.available) {
                const langId = language.show_locale ? language.language_locale_id : language.language_id;
                const langLocaleId = language.language_locale_id;
                const langObj = await languageManager.getById(langId);
                const bestHash = await getBestGenreHash(genre.hashes, language, defaultLanguage);

                if (langObj && bestHash) {
                    const prefix = await languageManager.getPathPrefix(website, language);
                    const path = genreRouteValues[langLocaleId] || genreRouteValues.default;
                    const link = website.host + prefix + path;
                    genreLangs.alternates.push({
                        href: link.replace(':hash', bestHash.hash),
                        hreflang: langObj.iso,
                        default: language.default || false,
                        language_locale_id: langLocaleId
                    });
                }
            }

            sitemapItems.push(genreLangs);
        }
    }

    // Agregar rutas de paginas amigas
    const altHomesRoute = website.routes.find((r) => r.name === 'home_alt');
    if (altHomesRoute && altHomesRoute.pathParameters && altHomesRoute.pathParameters.alternative_site) {
        for (const altSite of altHomesRoute.pathParameters.alternative_site) {
            const altSiteLangs: ISitemapUrl = {
                priority: 1,
                updateDate: now,
                changeFreq: 'daily',
                alternates: []
            };

            for (const language of website.language.available) {
                const langId = language.show_locale ? language.language_locale_id : language.language_id;
                const langLocaleId = language.language_locale_id;
                const langObj = await languageManager.getById(langId);

                if (langObj) {
                    const prefix = await languageManager.getPathPrefix(website, language);
                    const path = altHomesRoute.values[langLocaleId] || altHomesRoute.values.default;
                    const link = website.host + prefix + path;
                    altSiteLangs.alternates.push({
                        href: link.replace(/:alternative_site/g, altSite),
                        hreflang: langObj.iso,
                        default: language.default || false,
                        language_locale_id: langLocaleId
                    });
                }
            }

            sitemapItems.push(altSiteLangs);
        }
    }

    return sitemapUrlsToXML(sitemapItems);
};

const getBestGenreHash = async (hashes, preferredLanguage, defaultLanguage) => {
    let bestHash;
    // Primero intento agarrar el idioma preferido
    bestHash = hashes.find((h) => h.language_id === preferredLanguage.language_id);

    // Si no existe, intento agarrar el idioma por defecto de la pagina
    if (!bestHash) {
        bestHash = hashes.find((h) => h.language_id === defaultLanguage.language_id);
    }

    // Si no existe, agarro ingles
    if (!bestHash) {
        const fallbackLang = await languageManager.get('en');
        if (fallbackLang) {
            bestHash = hashes.find((hash) => hash.language_id === fallbackLang.language_id);
        }
    }

    // Si no existe, agarro el primero
    if (!bestHash) {
        bestHash = hashes[0];
    }

    return bestHash;
};