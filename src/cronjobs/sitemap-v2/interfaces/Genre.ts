export interface IGenre {
    genre_id: number;
    hashes: Array<{
        language_id: number;
        hash: string;
    }>;
}