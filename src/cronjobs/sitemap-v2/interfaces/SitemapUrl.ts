export interface ISitemapUrl {
    priority: number;
    updateDate: Date;
    changeFreq: string;
    alternates: Array<{
        language_locale_id: number;
        href: string;
        hreflang: string;
        default: boolean;
    }>;
}