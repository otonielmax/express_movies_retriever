import * as AWS from 'aws-sdk';
import * as config from 'config';

import { logger } from '../../../logger';

AWS.config.update({
    accessKeyId: config.DO_ACCESS_KEY_ID,
    secretAccessKey: config.DO_SECRET_ACCESS_KEY,
});

const s3 = new AWS.S3({
    endpoint: 'nyc3.digitaloceanspaces.com'
});

/**
 * Almacena todos los sitemaps en S3
 * Utilizara la clave:
 *  sitemaps/{website_id}/sitemap.xml -> para el indice de sitemaps
 *  sitemaps/{website_id}/sitemap_{number}.xml -> para cada uno de los sitemaps generados
 */
export const storeSitemaps = async (transactionId: string, website, sitemaps: string[]): Promise<void> => {
    for (const i in sitemaps) {
        logger.info(`Storing sitemap number "${i}" of website "${website.name}"`, { transactionId });
        const sitemap = sitemaps[i];
        const key = Number(i) === 0
            ? `sitemaps/${website.id}/sitemap.xml`
            : `sitemaps/${website.id}/sitemap_${i}.xml`;

        await store(key, sitemap);
    }
};

const store = (key: string, value: string): Promise<any> => {
    return s3.putObject({
        Bucket: config.DO_BUCKET,
        Key: key,
        Body: Buffer.from(value, 'utf8')
    }).promise();
};