import dbPool from '../../../database/pool';
import { logger } from '../../../logger';

/**
 * Obtiene el listado de peliculas que pertenecen al menos a una galeria
 * 
 * @param pageNumber - Numero de pagina
 */
const getSerieIds = async (pageNumber: number): Promise<number[]> => {
    const limit = 200;
    const offset = limit * (pageNumber - 1);

    const query = `
        SELECT
            id
        FROM
            serie
        WHERE
            gallery_popular_position IS NOT NULL OR
            gallery_top_rated_position IS NOT NULL OR
            gallery_airing_today_position IS NOT NULL OR
            gallery_on_air_position IS NOT NULL
        LIMIT
            ?
        OFFSET
            ?
    `;

    const [ rows ] = await dbPool.query(query, [limit, offset]);

    return rows.map((r) => r.id);
};

const getEpisodes = async (seasonIds: number[]) => {
    const getInfoQuery = `
        SELECT
            e.id episode_id,
            e.season_id,
            e.episode_number,
            e.updated_at,
            e.language_updated_at
        FROM
            episode e
        WHERE
            e.season_id IN (?)
    `;

    const [ episodesInfo ] = await dbPool.query(getInfoQuery, [seasonIds]);
    if (!episodesInfo.length) {
        return;
    }
    const episodeIds: number[] = episodesInfo.map((e) => e.episode_id);
    const episodes = episodesInfo.map((s) => {
        return {
            ...s,
            versions: [],
            spins: []
        };
    });

    // Buscar todas las versiones de los capitulos
    const getVersionsQuery = `
        SELECT
            episode_id,
            website_id,
            version
        FROM
            serie_version
        WHERE
            episode_id IN (?)
    `;
    const [ versions ] = await dbPool.query(getVersionsQuery, [episodeIds]);

    for (const version of versions) {
        const episode = episodes.find((e) => e.episode_id === version.episode_id);
        if (episode) {
            episode.versions.push(version);
        }
    }

    // Buscar los spin de los episodios
    const spinQuery = `
        SELECT
            sl.episode_id,
            slw.website_id,
            slw.created_at
        FROM
            serie_language sl
        JOIN
            serie_language_website slw ON sl.id=slw.serie_language_id
        WHERE
            sl.episode_id IN (?)
    `;
    const [ spins ] = await dbPool.query(spinQuery, [episodeIds]);

    for (const spin of spins) {
        const episode = episodes.find((e) => e.episode_id === spin.episode_id);
        if (episode) {
            episode.spins.push(spin);
        }
    }

    return episodes;
};

const getSeasons = async (serieIds: number[]) => {
    const getInfoQuery = `
        SELECT
            s.id season_id,
            s.serie_id,
            s.season_number,
            s.updated_at,
            s.language_updated_at
        FROM
            season s
        WHERE
            s.serie_id IN (?)
    `;

    const [ seasonsInfo ] = await dbPool.query(getInfoQuery, [serieIds]);
    if (!seasonsInfo.length) {
        return;
    }
    const seasonIds: number[] = seasonsInfo.map((s) => s.season_id);
    const seasons = seasonsInfo.map((s) => {
        return {
            ...s,
            episodes: [],
            versions: []
        };
    });

    // Buscar todas las versiones de las temporadas
    const getVersionsQuery = `
        SELECT
            season_id,
            website_id,
            version
        FROM
            serie_version
        WHERE
            season_id IN (?)
    `;
    const [ versions ] = await dbPool.query(getVersionsQuery, [seasonIds]);

    for (const version of versions) {
        const season = seasons.find((s) => s.season_id === version.season_id);
        if (season) {
            season.versions.push(version);
        }
    }

    // Buscar todos los episodios e insertarlos dentro de las seasons
    const episodes = await getEpisodes(seasonIds);
    if (episodes) {
        for (const episode of episodes) {
            const season = seasons.find((s) => s.season_id === episode.season_id);
            if (season) {
                season.episodes.push(episode);
            }
        }
    }

    return seasons;
};

const getSerieInfo = async (serieIds: number[]) => {
    const getInfoQuery = `
        SELECT
            s.id serie_id,
            s.gallery_popular_position,
            s.gallery_top_rated_position,
            s.gallery_airing_today_position,
            s.gallery_on_air_position,
            s.updated_at,
            s.language_updated_at
        FROM
            serie s
        WHERE
            s.id IN (?)
    `;
    const [ rows ] = await dbPool.query(getInfoQuery, [serieIds]);

    // Agrupar las series
    const series: any[] = rows.map((serie) => {
        return {
            ...serie,
            seasons: [],
            hashes: [],
            spins: [],
            versions: []
        };
    });
        
    // Buscar todos los hashes de las series
    const hashesQuery = `
        SELECT
            serie_id,
            language_id,
            language_locale_id,
            hash
        FROM
            serie_hash
        WHERE
            serie_id IN (?)
    `;
    const [ hashes ] = await dbPool.query(hashesQuery, [serieIds]);

    for (const hash of hashes) {
        const serie = series.find((s) => s.serie_id === hash.serie_id);
        if (serie) {
            serie.hashes.push(hash);
        }
    }

    // Buscar todas las versiones de las series
    const getVersionsQuery = `
        SELECT
            serie_id,
            website_id,
            version
        FROM
            serie_version
        WHERE
            serie_id IN (?)
    `;
    const [ versions ] = await dbPool.query(getVersionsQuery, [serieIds]);

    for (const version of versions) {
        const serie = series.find((s) => s.serie_id === version.serie_id);
        if (serie) {
            serie.versions.push(version);
        }
    }

    // Buscar los spin de las series
    const spinQuery = `
        SELECT
            sl.serie_id,
            slw.website_id,
            slw.created_at
        FROM
            serie_language sl
        JOIN
            serie_language_website slw ON sl.id=slw.serie_language_id
        WHERE
            sl.serie_id IN (?)
    `;
    const [ spins ] = await dbPool.query(spinQuery, [serieIds]);

    for (const spin of spins) {
        const serie = series.find((m) => m.serie_id === spin.serie_id);
        if (serie) {
            serie.spins.push(spin);
        }
    }

    // Buscar todas las seasons e insertarlas dentro de las series
    const seasons = await getSeasons(serieIds);
    if (seasons) {
        for (const season of seasons) {
            const serie = series.find((s) => s.serie_id === season.serie_id);
            if (serie) {
                serie.seasons.push(season);
            }
        }
    }

    return series;
};

/**
 * Obtiene el listado de todas las series de la base de datos, con la información necesaria para generar
 * el sitemap de cada sitio en concreto.
 */
export const getAllSeries = async (transactionId: string) => {
    let allSeries: any[] = [];
    
    const getPage = async (pageNumber: number) => {
        logger.info(`Obtaining series page "${pageNumber}"`, { transactionId });

        const serieIds = await getSerieIds(pageNumber);

        if (!serieIds || !serieIds.length) {
            return allSeries;
        }

        allSeries = allSeries.concat(await getSerieInfo(serieIds));

        return getPage(pageNumber + 1);
    };

    return getPage(1);
};