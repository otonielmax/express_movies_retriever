import dbPool from '../../../database/pool';
import { logger } from '../../../logger';

/**
 * Obtiene el listado de peliculas que pertenecen al menos a un listado
 * 
 * @param pageNumber - Numero de pagina
 */
const getMovieIds = async (pageNumber: number): Promise<number[]> => {
    const limit = 250;
    const offset = limit * (pageNumber - 1);

    const query = `
        SELECT
            id
        FROM
            movie
        WHERE
            gallery_popular_position IS NOT NULL OR
            gallery_top_rated_position IS NOT NULL OR
            gallery_now_playing_position IS NOT NULL OR
            gallery_upcoming_position IS NOT NULL
        LIMIT
            ?
        OFFSET
            ?
    `;

    const [ rows ] = await dbPool.query(query, [limit, offset]);

    return rows.map((r) => r.id);
};

const getMovieInfo = async (movieIds: number[]) => {
    const movieInfoQuery = `
        SELECT
            m.id movie_id,
            m.gallery_popular_position,
            m.gallery_top_rated_position,
            m.gallery_now_playing_position,
            m.gallery_upcoming_position,
            m.updated_at,
            m.language_updated_at,
            YEAR(m.release_date) release_year
        FROM
            movie m
        WHERE
            m.id IN (?)
    `;

    const [ rows ] = await dbPool.query(movieInfoQuery, [movieIds]);

    // Agrupar las peliculas
    const movies: any = rows.map((movie) => {
        return {
            ...movie,
            hashes: [],
            articles: [],
            spins: []
        };
    });

    // Buscar todos los hashes de las peliculas
    const hashesQuery = `
        SELECT
            movie_id,
            website_id,
            language_id,
            language_locale_id,
            hash
        FROM
            movie_hash
        WHERE
            active = 1 AND
            movie_id IN (?)
    `;
    const [ hashes ] = await dbPool.query(hashesQuery, [movieIds]);

    for (const hash of hashes) {
        const movie = movies.find((m) => m.movie_id === hash.movie_id);
        if (movie) {
            movie.hashes.push(hash);
        }
    }

    // Buscar los articulos de las peliculas
    const articlesQuery = `
        SELECT
            movie_id,
            website_id,
            updated_at
        FROM
            article
        WHERE
            movie_id IN (?)
    `;
    const [ articles ] = await dbPool.query(articlesQuery, [movieIds]);

    for (const article of articles) {
        const movie = movies.find((m) => m.movie_id === article.movie_id);
        if (movie) {
            movie.articles.push(article);
        }
    }

    // Buscar los spin de las peliculas
    const spinQuery = `
        SELECT
            ml.movie_id,
            mlw.website_id,
            mlw.created_at
        FROM
            movie_language ml
        JOIN
            movie_language_website mlw ON ml.id=mlw.movie_language_id
        WHERE
            ml.movie_id IN (?)
    `;
    const [ spins ] = await dbPool.query(spinQuery, [movieIds]);

    for (const spin of spins) {
        const movie = movies.find((m) => m.movie_id === spin.movie_id);
        if (movie) {
            movie.spins.push(spin);
        }
    }

    return movies;
};

/**
 * Obtiene el listado de todas las películas de la base de datos, con la información necesaria para generar
 * el sitemap de cada sitio en concreto.
 */
export const getAllMovies = async (transactionId: string) => {
    let allMovies: any[] = [];
    
    const getPage = async (pageNumber: number) => {
        logger.info(`Obtaining movies page "${pageNumber}"`, { transactionId });

        const movieIds = await getMovieIds(pageNumber);

        if (!movieIds || !movieIds.length) {
            return allMovies;
        }

        allMovies = allMovies.concat(await getMovieInfo(movieIds));

        return getPage(pageNumber + 1);
    };

    return getPage(1);
};