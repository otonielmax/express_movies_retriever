import dbPool from '../../../database/pool';
import { logger } from '../../../logger';
import { IGenre } from '../interfaces/Genre';

/**
 * Obtiene el listado de todos los generos de peliculas de la base de datos, con la información necesaria para generar
 * el sitemap de cada sitio en concreto.
 */
export const getAllGenres = async (transactionId: string) => {
    logger.info('Retrieving all genres info', { transactionId });

    const query = `
        SELECT
            g.id genre_id,
            gl.language_id,
            gl.hash
        FROM
            genre_language gl
        JOIN
            genre g ON g.id = gl.genre_id
        WHERE
            g.movies = 1
    `;

    const [ rows ] = await dbPool.query(query, []);

    const genres: IGenre[] = [];
    for (const row of rows) {
        let genre: IGenre | undefined = genres.find((g: any) => g.genre_id === row.genre_id);
        if (!genre) {
            genre = {
                genre_id: row.genre_id,
                hashes: []
            };
            genres.push(genre);
        }
        genre.hashes.push({
            language_id: row.language_id,
            hash: row.hash
        });
    }

    return genres;
};