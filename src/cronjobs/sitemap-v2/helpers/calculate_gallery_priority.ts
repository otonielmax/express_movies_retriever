import { calculateSitemapPriority, ICalculatePriorityResult } from './calculate_sitemap_priority';

export interface IGallery {
    routeName: string;
    galleryCol: string;
    factor: number;
}

export interface ICalculateGalleryPriorityArgs {
    website: any;
    galleries: IGallery[];
    items: any[];
    item: any;
    minRange: number;
    maxRange: number;
}

/**
 * Obtiene la posicion mas alta del item en alguna de las galerias activas en el sitio.
 * En base a la posición en la galería calcula la prioridad en el sitemap dentro de un rango definido.
 * De todas las prioridades calculadas, se queda con la más alta.
 * 
 */
export const calculateGalleryPriority = (args: ICalculateGalleryPriorityArgs): ICalculatePriorityResult | null => {
    const { website, galleries, items, item, maxRange, minRange } = args;
    const priorities: ICalculatePriorityResult[] = [];

    // Recorrer las galerias
    for (const gallery of galleries) {
        const galleryRoute = website.routes.find((r) => r.name === gallery.routeName);
        const itemPosition = item[gallery.galleryCol];
        // Si el sitio tiene configurada esta galeria y el item aparece en ella, calculo la prioridad
        if (galleryRoute && itemPosition) {
            const maxPosition = Math.max(...items.map((m) => m[gallery.galleryCol]));
            const priority = calculateSitemapPriority({
                totalItems: maxPosition,
                currentItem: itemPosition,
                factor: gallery.factor,
                minRange,
                maxRange
            });
            priorities.push(priority);
        }
    }

    if (!priorities.length) {
        return null;
    }

    // De todas las prioridades calculadas, quedarse con la mas alta
    let maxPriority: ICalculatePriorityResult|null = null;
    for (const priority of priorities) {
        if (!maxPriority || maxPriority.priority < priority.priority) {
            maxPriority = priority;
        }
    }

    return maxPriority;
};