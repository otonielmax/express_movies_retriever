export interface ICalculatePriorityArgs {
    totalItems: number;
    currentItem: number;
    factor: number;
    minRange: number;
    maxRange: number;
}

export interface ICalculatePriorityResult {
    priority: number;
    positionPercentage: number;
}

/**
 * Calcula la prioridad de un item en base a su posicion dentro de un muestreo, un rango de prioridades y un factor de importancia.
 * @param args 
 */
export const calculateSitemapPriority = (args: ICalculatePriorityArgs): ICalculatePriorityResult => {
    const { totalItems, currentItem, factor, minRange, maxRange } = args;

    // Calcular cuanto peso tiene cada item.
    const itemWeight = 1 / totalItems;

    // En base a la posicion del item y el peso de cada item, se decide que % se encuentra en la muestra.
    // Se hace 1-perc ya que queremos el inverso. Si es 0.25 (se encuentra en top 25%), la prioridad es 1-0.25=0.75
    // Se utiliza (currentItem - 1) para que el primer item tome la posicion mas alta
    const positionPercentage = 1 - ((currentItem - 1) * itemWeight);

    // Aplicar factor para reducir el nivel de prioridad si es necesario.
    const positionPercentageFactor = positionPercentage * factor;

    // Una vez obtenido el porcentaje de importancia, lo aplicamos al rango
    const percentageInRange = positionPercentageFactor * (maxRange - minRange) + minRange;

    // Redondear a dos decimales
    const priority = Math.round(percentageInRange * 100) / 100;

    return {
        priority,
        positionPercentage: positionPercentageFactor
    };
};