import * as config from 'config';

import dbPool from '../../database/pool';
import * as getWebsitesConfig from '../../helpers/getWebsitesConfig';
import * as healthCheck from '../../helpers/healthCheck';
import { logger } from '../../logger';
import { getAllGenres } from './helpers/get_genres';
import { getAllMovies } from './helpers/get_movies';
import { getAllSeries } from './helpers/get_series';
import { storeSitemaps } from './helpers/store_sitemaps';
import { generateCommonSitemap } from './sitemaps/common_sitemap';
import { generateMoviesSitemap } from './sitemaps/movies_sitemap';
import { generateSeriesSitemap } from './sitemaps/series_sitemap';
import { generateSitemapIndex } from './sitemaps/sitemap_index';

const transactionId = 'cronjob-sitemap-v2';

const run = async () => {
    try {
        logger.info('Starting cronjob', { transactionId });

        const movies = await getAllMovies(transactionId);
        const series = await getAllSeries(transactionId);
        const genres = await getAllGenres(transactionId);
        const websites = getWebsitesConfig();
    
        for (const website of websites) {
            logger.info(`Processing website "${website.name}"`, { transactionId });

            const commonSitemap = await generateCommonSitemap(transactionId, website, genres);
            const moviesSitemap = await generateMoviesSitemap(transactionId, website, movies);
            const seriesSitemap = await generateSeriesSitemap(transactionId, website, series);
    
            // Juntar todos los sitemaps y limpiar nulls
            const allSitemaps: string[] = ([] as any).concat(...[commonSitemap, moviesSitemap, seriesSitemap]).filter((s) => !!s);
            
            // Generar indice de sitemaps y agregarlo al listado
            const sitemapIndex = generateSitemapIndex(website, allSitemaps.length);
            allSitemaps.unshift(sitemapIndex);
    
            // Almacenar sitemaps en redis
            await storeSitemaps(transactionId, website, allSitemaps);

            logger.info(`Website "${website.name}" processed successfully`, { transactionId });
        }

        await healthCheck(transactionId, config.HC_SITEMAP_GENERATOR);

        logger.info('Job executed successfully', { transactionId });
    } catch (err) {
        logger.error('Error executing cronjob', { transactionId, err });
    }

    await dbPool.endPool();
};

run();