module.exports = {
    env: 'master',
    cluster: {
        workers: 2,
    },
    mysql: [{
        name: 'MASTER',
        host: 'testing.smartadtags.com',
        user: 'devuser',
        password: 'desarrolloT01',
        database: 'movies',
        connectionLimit: 10,
    }, {
        name: 'SLAVE-1',
        host: 'testing.smartadtags.com',
        user: 'devuser',
        password: 'desarrolloT01',
        database: 'movies',
        connectionLimit: 10,
    }],
    system: {
        cache: false,
        port: 5000
    },
    redis: {
        disabled: true,
        master: {
            host: '127.0.0.1',
            port: 6380,
            password: undefined,
            db: 7
        },
        slave: {
            host: '127.0.0.1',
            port: 6380,
            password: undefined,
            db: 7
        }
    },
    logger: {
        logLevel: 'info',
        service: 'movies-webserver',
        environment: 'dev',
    },
    stressTestMode: false,
    ignoredSearchKeywords: [
        /ver /g,
        /pelicula[s]?/g,
        /película[s]?/g,
        /online/g,
        /español/g,
        /latino/g,
        /\(|\)/g,
        /gratis/g,
        /streaming/g,
        /gratuito/g,
        /\d/g,
    ],
    paths: {
        static: '../../movies-websites/src/common/static',
        partials: '../../movies-websites/src/common/templates',
        translations: '../../movies-websites/src/common/translations'
    },
    movies: {
        minPopularity: 10,
    },
    session: {
        key: 'asdas',
        expiration: 60 * 60 * 24 * 7,
    }
};