'use strict';
const config = require('config');

const getProxyConfig = () => {
    const user = config.PROXY_USER + '-sessid-' + generateRandomSessid() + '-cc-' + pickCountry();
    return `http://${user}:${config.PROXY_PASSWORD}@${config.PROXY_HOST}:${config.PROXY_PORT}`;
};

const pickCountry = () => {
    const countries = ['ES', 'FR', 'NL', 'DE', 'US', 'CA', 'PT'];
    return countries[Math.floor(Math.random() * countries.length)];
};

const generateRandomSessid = () => {
    return Math.random().toString(36).substring(2, 6) + Math.random().toString(36).substring(2, 6);
};

module.exports = {
    getProxyConfig
};