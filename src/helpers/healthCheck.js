const logger = require('@smartadtags/logger');
const request = require('request-promise');

module.exports = async (transactionId, hcId, failure = false) => {
    if (!hcId) {
        logger.info('Skipping healthcheck because is is not configured', { transactionId, hcId, failure });
        return;
    }

    try {
        const url = `${hcId}${failure ? '/fail' : ''}`;
        logger.info(`Sending healthcheck to "${url}"`, { transactionId, hcId, failure });
        await request(url);
        logger.info(`Healthcheck to "${url}" has been sent`, { transactionId });
    } catch (err) {
        logger.error('Error sending healthcheck', { transactionId, err });
    }
};