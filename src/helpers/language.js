'use strict';
const dbService = require('../services/db');

const Languages = {};
const LanguagesIds = {};

exports.retrieve = (callback) => {
    const query = `
        SELECT
            id,
            name,
            iso,
            locale
        FROM
            language
    `;

    dbService.query(query, [], (err, languages) => {
        if (err) {
            return callback(err);
        }

        for (let lang of languages) {
            Languages[lang.iso] = lang;
            LanguagesIds[lang.id] = lang;
        }

        return callback();
    });
};

exports.getId = (iso) => {
    return Languages[iso] ? Languages[iso].id : null;
};

exports.getLanguage = (id) => {
    return LanguagesIds[id];
};

exports.getAll = (searchString) => {
    const result = [];

    for (const key in Languages) {
        if (key.indexOf(searchString) === 0) {
            result.push(Languages[key]);
        }
    }

    return result;
};