/**
 * @fileOverview Helper that contains different functions for using the database.
 * @module services/db
 * @requires mysql2
 * @requires ../config/config
 * @requires logger
 */

'use strict';

const logger = require('@smartadtags/logger');
const config = require('config');
const mysql = require('mysql2');

const pool = mysql.createPool({
    host: config.MYSQL_HOST,
    user: config.MYSQL_USER,
    port: config.MYSQL_PORT ? Number(config.MYSQL_PORT) : undefined,
    password: config.MYSQL_PASSWORD,
    database: config.MYSQL_DATABASE,
    connectionLimit: Number(config.MYSQL_CONNECTION_LIMIT)
});

/**
 * Get a MySQL connection from the pool
 * 
 * @name getConnection
 * @function
 * @param {Function} callback 
 */
exports.getConnection = (callback) => {
    pool.getConnection((err, conn) => {
        if (err) {
            return callback(err);
        }

        return callback(null, new dbObject(conn));
    });
};

exports.endPool = async (callback) => {
    pool.end(callback);
};

/**
 * Executes a query without a previous connection to MySQL.
 * 
 * @name query
 * @function
 * @param {String} query - The MySQL query.
 * @param {Array} values - Values to replace on the query.
 * @param {Function} callback
 * @param {Integer} [timeout] - A timeout can be set if the default timeout is too large.
 */
exports.query = (query, values, callback, timeout) => {
    if (!query) {
        return callback({
            message: 'Missing query'
        });
    }

    if (!values) {
        return callback({
            message: 'Missing values'
        });
    }

    exports.getConnection((err, dbConnection) => {
        if (err) {
            return callback({
                message: 'Error creating the connection',
                err: err
            });
        }

        dbConnection.query(query, values, (err, data) => {
            dbConnection.release();
            if (err || data === undefined || data === null) {
                return callback({
                    message: 'Error executing the query',
                    err: err
                });
            }

            return callback(null, data);
        }, timeout);
    });
};

/**
 * Class with different methods for database connection.
 * 
 * @class
 */
class dbObject {
    /**
     * dbObject contructor.
     * 
     * @constructor
     */
    constructor(connection) {
        this.connection = connection;
    }
    /**
     * Executes a query with a connection to MySQL.
     * 
     * @function
     * @param {String} query - The MySQL query.
     * @param {Array} values - Values to replace on the query.
     * @param {Function} callback - 
     * @param {Integer} [timeout] - A timeout can be set if the default timeout is too large.
     */
    query(query, params, callback, timeout = 45000) {
        this.connection.query({
            sql: query,
            timeout: timeout,
            values: params
        }, (err, results, fields) => {
            return callback(err, results, fields);
        });
    }
    /**
     * Release the MySQL connection.
     * 
     * @function
     */
    release() {
        let error = null;

        /* This is executed inside of a try-catch because if the connection was released, throws an error. */
        try {
            this.connection.release();
        } catch (e) {
            logger.error('There was an attempt to release an connection already released.');
            error = e;
        }

        return error ? {
            message: 'Can\'t release the connection',
            err: error
        } : null;
    }
    /**
     * Starts a transaction on the connection. The transaction needs to be commited or make a rollback in the end.
     * 
     * @function
     * @param {Function} callback 
     */
    beginTransaction(callback) {
        this.connection.beginTransaction((err) => {
            return callback(err);
        });
    }
    /**
     * Makes a rollback of the current transaction.
     * 
     * @function
     * @param {Function} callback 
     */
    rollback(callback) {
        this.connection.rollback(() => {
            return callback();
        });
    }
    /**
     * Commits the current transaction.
     * 
     * @function
     * @param {Function} callback 
     */
    commit(callback) {
        let that = this;
        this.connection.commit(function (err) {
            if (err) {
                that.rollback(function () {
                    return callback(err);
                });
            }
            return callback();
        });
    }
}