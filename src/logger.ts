import * as staLogger from '@smartadtags/logger';
import * as config from 'config';

staLogger.config({
    environment: config.LOGGER_ENV,
    service: 'movies-retriever',
    logLevel: config.LOGGER_LOG_LEVEL,
    format: config.LOGGER_FORMAT
});

export const logger = staLogger;
