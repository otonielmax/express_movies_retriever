'use strict';

import * as mysqlHelper from '@smartadtags/mysql-helper';
import * as config from 'config';

export default mysqlHelper.startPool({
    host: config.MYSQL_HOST,
    port: config.MYSQL_PORT ? Number(config.MYSQL_PORT) : undefined,
    user: config.MYSQL_USER,
    password: config.MYSQL_PASSWORD,
    database: config.MYSQL_DATABASE,
    connectionLimit: Number(config.MYSQL_CONNECTION_LIMIT)
});