import { ImageMediaStatus } from '../enums/ImageMediaStatus';
import { ImageMediaType } from '../enums/ImageMediaType';

export interface IImageMedia {
    id?: number;
    path: string;
    file_name: string;
    image_media_type_id: ImageMediaType;
    image_media_status_id: ImageMediaStatus;
}